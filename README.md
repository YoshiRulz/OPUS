# OPUS

## Building

Build using Gradle:
```
$> git clone https://github.com/YoshiRulz/OPUS.git && cd OPUS
$> git submodule update --init   # include the license file
$> ./gradlew build
```

## Docs

Documentation in the form of KDoc is also built using Gradle: `./gradlew dokka`.
