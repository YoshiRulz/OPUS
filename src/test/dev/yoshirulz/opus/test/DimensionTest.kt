package dev.yoshirulz.opus.test

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.QuantityDimension
import dev.yoshirulz.opus.units.*
import dev.yoshirulz.opus.units.AbsoluteTemperatureUnitNameClass.AbsoluteTemperatureUnitName
import dev.yoshirulz.opus.units.AccelerationUnitNameClass.AccelerationUnitName
import dev.yoshirulz.opus.units.ActionUnitNameClass.ActionUnitName
import dev.yoshirulz.opus.units.AmountOfSubstanceUnitNameClass.AmountOfSubstanceUnitName
import dev.yoshirulz.opus.units.AngularAccelerationUnitNameClass.AngularAccelerationUnitName
import dev.yoshirulz.opus.units.AngularJerkUnitNameClass.AngularJerkUnitName
import dev.yoshirulz.opus.units.AngularMomentumUnitNameClass.AngularMomentumUnitName
import dev.yoshirulz.opus.units.AngularSpeedUnitNameClass.AngularSpeedUnitName
import dev.yoshirulz.opus.units.AreaDensityUnitNameClass.AreaDensityUnitName
import dev.yoshirulz.opus.units.AstronomicalUnitClass.AstronomicalUnit
import dev.yoshirulz.opus.units.AtomicUnitOfActionClass.AtomicUnitOfAction
import dev.yoshirulz.opus.units.AtomicUnitOfChargeClass.AtomicUnitOfCharge
import dev.yoshirulz.opus.units.AtomicUnitOfEnergyClass.AtomicUnitOfEnergy
import dev.yoshirulz.opus.units.AtomicUnitOfLengthClass.AtomicUnitOfLength
import dev.yoshirulz.opus.units.AtomicUnitOfMassClass.AtomicUnitOfMass
import dev.yoshirulz.opus.units.AtomicUnitOfTimeClass.AtomicUnitOfTime
import dev.yoshirulz.opus.units.BarClass.Bar
import dev.yoshirulz.opus.units.BarnClass.Barn
import dev.yoshirulz.opus.units.BecquerelClass.Becquerel
import dev.yoshirulz.opus.units.CandelaClass.Candela
import dev.yoshirulz.opus.units.CatalyticActivityUnitNameClass.CatalyticActivityUnitName
import dev.yoshirulz.opus.units.ChargeDensityUnitNameClass.ChargeDensityUnitName
import dev.yoshirulz.opus.units.ChargeMassRatioUnitNameClass.ChargeMassRatioUnitName
import dev.yoshirulz.opus.units.CharjonClass.Charjon
import dev.yoshirulz.opus.units.DaltonClass.Dalton
import dev.yoshirulz.opus.units.DayClass.Day
import dev.yoshirulz.opus.units.DegreeCelsiusClass.DegreeCelsius
import dev.yoshirulz.opus.units.DegreeOfArcClass.Degree
import dev.yoshirulz.opus.units.DensityUnitNameClass.DensityUnitName
import dev.yoshirulz.opus.units.DisplacementFieldStrengthUnitNameClass.DisplacementFieldStrengthUnitName
import dev.yoshirulz.opus.units.DynamicViscosityUnitNameClass.DynamicViscosityUnitName
import dev.yoshirulz.opus.units.ElectricCurrentUnitNameClass.ElectricCurrentUnitName
import dev.yoshirulz.opus.units.ElectricFieldStrengthUnitNameClass.ElectricFieldStrengthUnitName
import dev.yoshirulz.opus.units.ElectricalCapacitanceUnitNameClass.ElectricalCapacitanceUnitName
import dev.yoshirulz.opus.units.ElectricalConductanceUnitNameClass.ElectricalConductanceUnitName
import dev.yoshirulz.opus.units.ElectricalInductanceUnitNameClass.ElectricalInductanceUnitName
import dev.yoshirulz.opus.units.ElectricalResistanceUnitNameClass.ElectricalResistanceUnitName
import dev.yoshirulz.opus.units.ElectronVoltClass.ElectronVolt
import dev.yoshirulz.opus.units.ElectrostaticPotentialUnitNameClass.ElectrostaticPotentialUnitName
import dev.yoshirulz.opus.units.EnergyDensityUnitNameClass.EnergyDensityUnitName
import dev.yoshirulz.opus.units.EnergyUnitNameClass.EnergyUnitName
import dev.yoshirulz.opus.units.EntropyUnitNameClass.EntropyUnitName
import dev.yoshirulz.opus.units.ForceUnitNameClass.ForceUnitName
import dev.yoshirulz.opus.units.FrequencyUnitNameClass.FrequencyUnitName
import dev.yoshirulz.opus.units.GrayClass.Gray
import dev.yoshirulz.opus.units.HeatCapacityUnitNameClass.HeatCapacityUnitName
import dev.yoshirulz.opus.units.HectareClass.Hectare
import dev.yoshirulz.opus.units.HourClass.Hour
import dev.yoshirulz.opus.units.JerkUnitNameClass.JerkUnitName
import dev.yoshirulz.opus.units.KinematicViscosityUnitNameClass.KinematicViscosityUnitName
import dev.yoshirulz.opus.units.KnotClass.Knot
import dev.yoshirulz.opus.units.LitreClass.Litre
import dev.yoshirulz.opus.units.LongitudinClass.Longitudin
import dev.yoshirulz.opus.units.LongitudinCubedClass.LongitudinCubed
import dev.yoshirulz.opus.units.LongitudinSquaredClass.LongitudinSquared
import dev.yoshirulz.opus.units.LumenClass.Lumen
import dev.yoshirulz.opus.units.LuxClass.Lux
import dev.yoshirulz.opus.units.MagneticFieldHStrengthUnitNameClass.MagneticFieldHStrengthUnitName
import dev.yoshirulz.opus.units.MagneticFluxDensityUnitNameClass.MagneticFluxDensityUnitName
import dev.yoshirulz.opus.units.MagneticFluxUnitNameClass.MagneticFluxUnitName
import dev.yoshirulz.opus.units.MagneticMomentUnitNameClass.MagneticMomentUnitName
import dev.yoshirulz.opus.units.MassUnitNameClass.MassUnitName
import dev.yoshirulz.opus.units.MillimetreMercuryClass.MillimetreMercury
import dev.yoshirulz.opus.units.MinuteClass.Minute
import dev.yoshirulz.opus.units.MolarClass.Molar
import dev.yoshirulz.opus.units.MolarConcentrationUnitNameClass.MolarConcentrationUnitName
import dev.yoshirulz.opus.units.MolarEnergyUnitNameClass.MolarEnergyUnitName
import dev.yoshirulz.opus.units.MolarHeatCapacityUnitNameClass.MolarHeatCapacityUnitName
import dev.yoshirulz.opus.units.MolarMassUnitNameClass.MolarMassUnitName
import dev.yoshirulz.opus.units.MolarVolumeUnitNameClass.MolarVolumeUnitName
import dev.yoshirulz.opus.units.MomentumUnitNameClass.MomentumUnitName
import dev.yoshirulz.opus.units.NaturalUnitOfActionClass.NaturalUnitOfAction
import dev.yoshirulz.opus.units.NaturalUnitOfMassClass.NaturalUnitOfMass
import dev.yoshirulz.opus.units.NaturalUnitOfSpeedClass.NaturalUnitOfSpeed
import dev.yoshirulz.opus.units.NaturalUnitOfTimeClass.NaturalUnitOfTime
import dev.yoshirulz.opus.units.NauticalMileClass.NauticalMile
import dev.yoshirulz.opus.units.NeoSIAmpereClass.Ampere
import dev.yoshirulz.opus.units.NeoSIAmperePerMetreClass.AmperePerMetre
import dev.yoshirulz.opus.units.NeoSICoulombClass.Coulomb
import dev.yoshirulz.opus.units.NeoSICoulombPerKilogramClass.CoulombPerKilogram
import dev.yoshirulz.opus.units.NeoSICoulombPerMetreCubedClass.CoulombPerMetreCubed
import dev.yoshirulz.opus.units.NeoSICoulombPerMetreSquaredClass.CoulombPerMetreSquared
import dev.yoshirulz.opus.units.NeoSIFaradClass.Farad
import dev.yoshirulz.opus.units.NeoSIFaradPerMetreClass.FaradPerMetre
import dev.yoshirulz.opus.units.NeoSIHenryClass.Henry
import dev.yoshirulz.opus.units.NeoSIHenryPerMetreClass.HenryPerMetre
import dev.yoshirulz.opus.units.NeoSIHertzClass.Hertz
import dev.yoshirulz.opus.units.NeoSIJouleClass.Joule
import dev.yoshirulz.opus.units.NeoSIJoulePerKelvinHeatCapacityClass.JoulePerKelvin
import dev.yoshirulz.opus.units.NeoSIJoulePerKilogramClass.JoulePerKilogram
import dev.yoshirulz.opus.units.NeoSIJoulePerKilogramKelvinClass.JoulePerKilogramKelvin
import dev.yoshirulz.opus.units.NeoSIJoulePerMoleClass.JoulePerMole
import dev.yoshirulz.opus.units.NeoSIJoulePerMoleKelvinClass.JoulePerMoleKelvin
import dev.yoshirulz.opus.units.NeoSIJoulePerRadianClass.JoulePerRadian
import dev.yoshirulz.opus.units.NeoSIJoulePerTeslaClass.JoulePerTesla
import dev.yoshirulz.opus.units.NeoSIJouleSecondActionClass.JouleSecond
import dev.yoshirulz.opus.units.NeoSIKatalClass.Katal
import dev.yoshirulz.opus.units.NeoSIKelvinClass.Kelvin
import dev.yoshirulz.opus.units.NeoSIKilogramClass.Kilogram
import dev.yoshirulz.opus.units.NeoSIKilogramPerMetreCubedClass.KilogramPerMetreCubed
import dev.yoshirulz.opus.units.NeoSIKilogramPerMetreSquaredClass.KilogramPerMetreSquared
import dev.yoshirulz.opus.units.NeoSIKilogramPerMoleClass.KilogramPerMole
import dev.yoshirulz.opus.units.NeoSIMetreClass.Metre
import dev.yoshirulz.opus.units.NeoSIMetreCubedClass.MetreCubed
import dev.yoshirulz.opus.units.NeoSIMetreCubedPerKilogramClass.MetreCubedPerKilogram
import dev.yoshirulz.opus.units.NeoSIMetreCubedPerMoleClass.MetreCubedPerMole
import dev.yoshirulz.opus.units.NeoSIMetreCubedPerSecondClass.MetreCubedPerSecond
import dev.yoshirulz.opus.units.NeoSIMetrePerSecondClass.MetrePerSecond
import dev.yoshirulz.opus.units.NeoSIMetrePerSecondCubedClass.MetrePerSecondCubed
import dev.yoshirulz.opus.units.NeoSIMetrePerSecondSquaredClass.MetrePerSecondSquared
import dev.yoshirulz.opus.units.NeoSIMetreSquaredClass.MetreSquared
import dev.yoshirulz.opus.units.NeoSIMetreSquaredPerSecondClass.MetreSquaredPerSecond
import dev.yoshirulz.opus.units.NeoSIMoleClass.Mole
import dev.yoshirulz.opus.units.NeoSIMolePerMetreCubedClass.MolePerMetreCubed
import dev.yoshirulz.opus.units.NeoSINewtonClass.Newton
import dev.yoshirulz.opus.units.NeoSINewtonMetrePerSecondClass.NewtonMetrePerSecond
import dev.yoshirulz.opus.units.NeoSINewtonPerCoulombClass.NewtonPerCoulomb
import dev.yoshirulz.opus.units.NeoSINewtonPerMetreClass.NewtonPerMetre
import dev.yoshirulz.opus.units.NeoSINewtonPerSecondClass.NewtonPerSecond
import dev.yoshirulz.opus.units.NeoSINewtonSecondClass.NewtonSecond
import dev.yoshirulz.opus.units.NeoSIOhmClass.Ohm
import dev.yoshirulz.opus.units.NeoSIPascalClass.Pascal
import dev.yoshirulz.opus.units.NeoSIPascalSecondClass.PascalSecond
import dev.yoshirulz.opus.units.NeoSIRadianClass.Radian
import dev.yoshirulz.opus.units.NeoSIRadianPerSecondClass.RadianPerSecond
import dev.yoshirulz.opus.units.NeoSIRadianPerSecondCubedClass.RadianPerSecondCubed
import dev.yoshirulz.opus.units.NeoSIRadianPerSecondSquaredClass.RadianPerSecondSquared
import dev.yoshirulz.opus.units.NeoSIReciprocalMetreClass.ReciprocalMetre
import dev.yoshirulz.opus.units.NeoSISecondClass.Second
import dev.yoshirulz.opus.units.NeoSISiemensClass.Siemens
import dev.yoshirulz.opus.units.NeoSISteradianClass.Steradian
import dev.yoshirulz.opus.units.NeoSITeslaClass.Tesla
import dev.yoshirulz.opus.units.NeoSIVoltClass.Volt
import dev.yoshirulz.opus.units.NeoSIWattClass.Watt
import dev.yoshirulz.opus.units.NeoSIWattPerMetreKelvinClass.WattPerMetreKelvin
import dev.yoshirulz.opus.units.NeoSIWeberClass.Weber
import dev.yoshirulz.opus.units.OPUSHydrogenPotentialUnitNameClass.HydrogenPotentialUnitName
import dev.yoshirulz.opus.units.OPUSHydroxidePotentialUnitNameClass.HydroxidePotentialUnitName
import dev.yoshirulz.opus.units.PermeabilityUnitNameClass.PermeabilityUnitName
import dev.yoshirulz.opus.units.PermittivityUnitNameClass.PermittivityUnitName
import dev.yoshirulz.opus.units.PowerUnitNameClass.PowerUnitName
import dev.yoshirulz.opus.units.PressureUnitNameClass.PressureUnitName
import dev.yoshirulz.opus.units.RevolutionClass.Revolution
import dev.yoshirulz.opus.units.RotatumUnitNameClass.RotatumUnitName
import dev.yoshirulz.opus.units.SievertClass.Sievert
import dev.yoshirulz.opus.units.SpecificEnergyUnitNameClass.SpecificEnergyUnitName
import dev.yoshirulz.opus.units.SpecificHeatCapacityUnitNameClass.SpecificHeatCapacityUnitName
import dev.yoshirulz.opus.units.SpecificVolumeUnitNameClass.SpecificVolumeUnitName
import dev.yoshirulz.opus.units.SpeedUnitNameClass.SpeedUnitName
import dev.yoshirulz.opus.units.SuperrevolutionClass.Superrevolution
import dev.yoshirulz.opus.units.SurfaceChargeDensityUnitNameClass.SurfaceChargeDensityUnitName
import dev.yoshirulz.opus.units.SurfaceTensionUnitNameClass.SurfaceTensionUnitName
import dev.yoshirulz.opus.units.SymbolCountUnitNameClass.SymbolCountUnitName
import dev.yoshirulz.opus.units.TempunClass.Tempun
import dev.yoshirulz.opus.units.ThermalConductivityUnitNameClass.ThermalConductivityUnitName
import dev.yoshirulz.opus.units.TonneClass.Tonne
import dev.yoshirulz.opus.units.TorqueUnitNameClass.TorqueUnitName
import dev.yoshirulz.opus.units.VolumetricFlowRateUnitNameClass.VolumetricFlowRateUnitName
import dev.yoshirulz.opus.units.WavenumberUnitNameClass.WavenumberUnitName
import dev.yoshirulz.opus.units.YankUnitNameClass.YankUnitName
import dev.yoshirulz.opus.units.ÅngströmClass.Ångström
import org.junit.Assert.assertEquals
import org.junit.Test

/** @see License */
class DimensionTest {
	companion object {
		private val SI_ANGULAR_DIM_CONVERSION_FACTOR = SIUnitAmalgam("", intArrayOf(2, 0, 0, 0, 0, 0, 0)) // L^2/R
		private val OPUS_QDIM_MAP = mapOf<OPUSUnit<*>, NeoSIUnit<*>>(
			OPUSUnitlessClass.Unitless to NeoSIUnitlessClass.Unitless,
			Tempun to Second,
			Longitudin to Metre,
			MassUnitName to Kilogram,
			Charjon to Coulomb,
			Revolution to Radian,
			AbsoluteTemperatureUnitName to Kelvin,
			SymbolCountUnitName to NeoSIUnitlessClass.Unitless,
			AmountOfSubstanceUnitName to Mole,
			OPUSUnitOfAccountClass.UnitOfAccount to NeoSIUnitOfAccountClass.UnitOfAccount,

			AccelerationUnitName to MetrePerSecondSquared,
			ActionUnitName to JouleSecond,
			AngularAccelerationUnitName to RadianPerSecondSquared,
			AngularJerkUnitName to RadianPerSecondCubed,
			AngularSpeedUnitName to RadianPerSecond,
			AreaDensityUnitName to KilogramPerMetreSquared,
			CatalyticActivityUnitName to Katal,
			ChargeDensityUnitName to CoulombPerMetreCubed,
			ChargeMassRatioUnitName to CoulombPerKilogram,
			DensityUnitName to KilogramPerMetreCubed,
			DynamicViscosityUnitName to PascalSecond,
			ElectricalCapacitanceUnitName to Farad,
			ElectricalConductanceUnitName to Siemens,
			ElectricalInductanceUnitName to Henry,
			ElectricalResistanceUnitName to Ohm,
			ElectricCurrentUnitName to Ampere,
			ElectricFieldStrengthUnitName to NewtonPerCoulomb,
			ElectrostaticPotentialUnitName to Volt,
			EnergyUnitName to Joule,
			EntropyUnitName to NeoSIJoulePerKelvinEntropyClass.JoulePerKelvin,
			ForceUnitName to Newton,
			FrequencyUnitName to Hertz,
			HeatCapacityUnitName to JoulePerKelvin,
			JerkUnitName to MetrePerSecondCubed,
			KinematicViscosityUnitName to MetreSquaredPerSecond,
			LongitudinCubed to MetreCubed,
			LongitudinSquared to MetreSquared,
			MagneticFieldHStrengthUnitName to AmperePerMetre,
			MagneticFluxUnitName to Weber,
			MagneticFluxDensityUnitName to Tesla,
			MagneticMomentUnitName to JoulePerTesla,
			MolarConcentrationUnitName to MolePerMetreCubed,
			MolarEnergyUnitName to JoulePerMole,
			MolarHeatCapacityUnitName to JoulePerMoleKelvin,
			MolarMassUnitName to KilogramPerMole,
			MolarVolumeUnitName to MetreCubedPerMole,
			MomentumUnitName to NewtonSecond,
			PermeabilityUnitName to HenryPerMetre,
			PermittivityUnitName to FaradPerMetre,
			PowerUnitName to Watt,
			PressureUnitName to Pascal,
			RotatumUnitName to NewtonMetrePerSecond,
			SpecificEnergyUnitName to JoulePerKilogram,
			SpecificHeatCapacityUnitName to JoulePerKilogramKelvin,
			SpecificVolumeUnitName to MetreCubedPerKilogram,
			SpeedUnitName to MetrePerSecond,
			Superrevolution to Steradian,
			SurfaceChargeDensityUnitName to CoulombPerMetreSquared,
			SurfaceTensionUnitName to NewtonPerMetre,
			ThermalConductivityUnitName to WattPerMetreKelvin,
			TorqueUnitName to JoulePerRadian,
			VolumetricFlowRateUnitName to MetreCubedPerSecond,
			WavenumberUnitName to ReciprocalMetre,
			YankUnitName to NewtonPerSecond
		).mapKeys { it.key.qDimension }.mapValues { it.value.qDimension }
		private val NEO_SI_QDIM_MAP = mapOf<NeoSIUnit<*>, SIUnit<*>>(
			NeoSIUnitlessClass.Unitless to SIUnitlessClass.Unitless,
			Metre to SIMetreClass.Metre,
			Kilogram to SIKilogramClass.Kilogram,
			Second to SISecondClass.Second,
			Ampere to SIAmpereClass.Ampere,
			Radian to SIUnitlessClass.Unitless,
			Kelvin to SIKelvinClass.Kelvin,
			Mole to SIMoleClass.Mole,
			NeoSIUnitOfAccountClass.UnitOfAccount to SIGenericDollarClass.GenericDollar,

			Ampere to SIAmpereClass.Ampere,
			AmperePerMetre to SIAmperePerMetreClass.AmperePerMetre,
			Coulomb to SICoulombClass.Coulomb,
			CoulombPerKilogram to SICoulombPerKilogramClass.CoulombPerKilogram,
			CoulombPerMetreCubed to SICoulombPerMetreCubedClass.CoulombPerMetreCubed,
			CoulombPerMetreSquared to SICoulombPerMetreSquaredClass.CoulombPerMetreSquared,
			Farad to SIFaradClass.Farad,
			FaradPerMetre to SIFaradPerMetreClass.FaradPerMetre,
			Henry to SIHenryClass.Henry,
			HenryPerMetre to SIHenryPerMetreClass.HenryPerMetre,
			Hertz to SIHertzClass.Hertz,
			Joule to SIJouleClass.Joule,
			NeoSIJoulePerKelvinEntropyClass.JoulePerKelvin to SIJoulePerKelvinEntropyClass.JoulePerKelvin,
			JoulePerKelvin to SIJoulePerKelvinHeatCapacityClass.JoulePerKelvin,
			JoulePerKilogram to SIJoulePerKilogramClass.JoulePerKilogram,
			JoulePerKilogramKelvin to SIJoulePerKilogramKelvinClass.JoulePerKilogramKelvin,
			JoulePerMole to SIJoulePerMoleClass.JoulePerMole,
			JoulePerMoleKelvin to SIJoulePerMoleKelvinClass.JoulePerMoleKelvin,
			JoulePerTesla to SIJoulePerTeslaClass.JoulePerTesla,
			JouleSecond to SIJouleSecondActionClass.JouleSecond,
			Katal to SIKatalClass.Katal,
			KilogramPerMetreCubed to SIKilogramPerMetreCubedClass.KilogramPerMetreCubed,
			KilogramPerMetreSquared to SIKilogramPerMetreSquaredClass.KilogramPerMetreSquared,
			KilogramPerMole to SIKilogramPerMoleClass.KilogramPerMole,
			MetreCubedPerKilogram to SIMetreCubedPerKilogramClass.MetreCubedPerKilogram,
			MetreCubedPerMole to SIMetreCubedPerMoleClass.MetreCubedPerMole,
			MetreCubedPerSecond to SIMetreCubedPerSecondClass.MetreCubedPerSecond,
			MetreCubed to SIMetreCubedClass.MetreCubed,
			MetrePerSecond to SIMetrePerSecondClass.MetrePerSecond,
			MetrePerSecondCubed to SIMetrePerSecondCubedClass.MetrePerSecondCubed,
			MetrePerSecondSquared to SIMetrePerSecondSquaredClass.MetrePerSecondSquared,
			MetreSquared to SIMetreSquaredClass.MetreSquared,
			MetreSquaredPerSecond to SIMetreSquaredPerSecondClass.MetreSquaredPerSecond,
			MolePerMetreCubed to SIMolePerMetreCubedClass.MolePerMetreCubed,
			Newton to SINewtonClass.Newton,
			NewtonPerCoulomb to SINewtonPerCoulombClass.NewtonPerCoulomb,
			NewtonPerMetre to SINewtonPerMetreClass.NewtonPerMetre,
			NewtonPerSecond to SINewtonPerSecondClass.NewtonPerSecond,
			NewtonSecond to SINewtonSecondClass.NewtonSecond,
			Ohm to SIOhmClass.Ohm,
			Pascal to SIPascalClass.Pascal,
			PascalSecond to SIPascalSecondClass.PascalSecond,
			RadianPerSecond to SIRadianPerSecondClass.RadianPerSecond,
			RadianPerSecondCubed to SIRadianPerSecondCubedClass.RadianPerSecondCubed,
			RadianPerSecondSquared to SIRadianPerSecondSquaredClass.RadianPerSecondSquared,
			ReciprocalMetre to SIReciprocalMetreClass.ReciprocalMetre,
			Siemens to SISiemensClass.Siemens,
			Steradian to SISteradianClass.Steradian,
			Tesla to SITeslaClass.Tesla,
			Volt to SIVoltClass.Volt,
			Watt to SIWattClass.Watt,
			WattPerMetreKelvin to SIWattPerMetreKelvinClass.WattPerMetreKelvin,
			Weber to SIWeberClass.Weber
		).mapKeys { it.key.qDimension }.mapValues { it.value.qDimension }

		private fun dimensionsOf(d: QuantityDimension<*>) = d.group.toString()
		private fun dimensionsOf(u: NeoSIUnit<*>) = dimensionsOf(u.qDimension)
		private fun dimensionsOf(u: OPUSUnit<*>) = dimensionsOf(u.qDimension)
		private fun dimensionsOf(u: SIUnit<*>) = dimensionsOf(u.qDimension)
		private fun dimensionsOfProduct(vararg u: OPUSUnit<*>) = dimensionsOf(OPUSUnitAmalgam(u))
		private fun dimensionsOfProduct(vararg u: SIUnit<*>) = dimensionsOf(SIUnitAmalgam(u))
	}

	@Test
	fun testOPUS() {
		assertEquals("f = ν·v", dimensionsOfProduct(WavenumberUnitName, SpeedUnitName), dimensionsOf(FrequencyUnitName))
		assertEquals("F = m·a", dimensionsOfProduct(MassUnitName, AccelerationUnitName), dimensionsOf(ForceUnitName))
		assertEquals("F = J/t", dimensionsOfProduct(MomentumUnitName, Tempun.reciprocal), dimensionsOf(ForceUnitName))
		assertEquals("P = F/A", dimensionsOfProduct(ForceUnitName, LongitudinSquared.reciprocal), dimensionsOf(PressureUnitName))
		assertEquals("E = F·d", dimensionsOfProduct(ForceUnitName, Longitudin), dimensionsOf(EnergyUnitName))
		assertEquals("P = E/t", dimensionsOfProduct(EnergyUnitName, Tempun.reciprocal), dimensionsOf(PowerUnitName))
		assertEquals("I = q/t", dimensionsOfProduct(Charjon, Tempun.reciprocal), dimensionsOf(ElectricCurrentUnitName))
		assertEquals("V = E/q", dimensionsOfProduct(EnergyUnitName, Charjon.reciprocal), dimensionsOf(ElectrostaticPotentialUnitName))
		assertEquals("C = q/V", dimensionsOfProduct(Charjon, ElectrostaticPotentialUnitName.reciprocal), dimensionsOf(ElectricalCapacitanceUnitName))
		assertEquals("R = V/I", dimensionsOfProduct(ElectrostaticPotentialUnitName, ElectricCurrentUnitName.reciprocal), dimensionsOf(ElectricalResistanceUnitName))
		assertEquals("G = I/V", dimensionsOfProduct(ElectricCurrentUnitName, ElectrostaticPotentialUnitName.reciprocal), dimensionsOf(ElectricalConductanceUnitName))
		assertEquals("Φ_B = B·S", dimensionsOfProduct(MagneticFluxDensityUnitName, LongitudinSquared), dimensionsOf(MagneticFluxUnitName))
		assertEquals("B = F/(q·v)", dimensionsOfProduct(ForceUnitName, Charjon.reciprocal, SpeedUnitName.reciprocal), dimensionsOf(MagneticFluxDensityUnitName))
		assertEquals("M = Φ/I", dimensionsOfProduct(MagneticFluxUnitName, ElectricCurrentUnitName.reciprocal), dimensionsOf(ElectricalInductanceUnitName))
		assertEquals(dimensionsOfProduct(AmountOfSubstanceUnitName, Tempun.reciprocal), dimensionsOf(CatalyticActivityUnitName))

		assertEquals(dimensionsOfProduct(Longitudin, Longitudin), dimensionsOf(LongitudinSquared))
		assertEquals(dimensionsOfProduct(Longitudin, Longitudin, Longitudin), dimensionsOf(LongitudinCubed))
		assertEquals("v = s/t", dimensionsOfProduct(Longitudin, Tempun.reciprocal), dimensionsOf(SpeedUnitName))
		assertEquals("Q = V/t", dimensionsOfProduct(LongitudinCubed, Tempun.reciprocal), dimensionsOf(VolumetricFlowRateUnitName))
		assertEquals("a = v/t", dimensionsOfProduct(SpeedUnitName, Tempun.reciprocal), dimensionsOf(AccelerationUnitName))
		assertEquals("j = a/t", dimensionsOfProduct(AccelerationUnitName, Tempun.reciprocal), dimensionsOf(JerkUnitName))
		assertEquals("ω = θ/t", dimensionsOfProduct(Revolution, Tempun.reciprocal), dimensionsOf(AngularSpeedUnitName))
		assertEquals("α = ω/t", dimensionsOfProduct(AngularSpeedUnitName, Tempun.reciprocal), dimensionsOf(AngularAccelerationUnitName))
		assertEquals("ζ = α/t", dimensionsOfProduct(AngularAccelerationUnitName, Tempun.reciprocal), dimensionsOf(AngularJerkUnitName))
		assertEquals("p = m·v", dimensionsOfProduct(MassUnitName, SpeedUnitName), dimensionsOf(MomentumUnitName))
		assertEquals(dimensionsOfProduct(MassUnitName, AngularSpeedUnitName), dimensionsOf(AngularMomentumUnitName))
		assertEquals(dimensionsOfProduct(AngularMomentumUnitName, Tempun.reciprocal), dimensionsOf(TorqueUnitName))
		assertEquals("Y = m·j", dimensionsOfProduct(MassUnitName, JerkUnitName), dimensionsOf(YankUnitName))
		assertEquals("P = ∂τ/∂t", dimensionsOfProduct(TorqueUnitName, Tempun.reciprocal), dimensionsOf(RotatumUnitName))
		assertEquals("ν = 1/λ", dimensionsOf(Longitudin.reciprocal), dimensionsOf(WavenumberUnitName))
		assertEquals("σ = ρ·s", dimensionsOfProduct(DensityUnitName, Longitudin), dimensionsOf(AreaDensityUnitName))
		assertEquals("ρ = m/V", dimensionsOfProduct(MassUnitName, LongitudinCubed.reciprocal), dimensionsOf(DensityUnitName))
		assertEquals("v = V/m", dimensionsOfProduct(LongitudinCubed, MassUnitName.reciprocal), dimensionsOf(SpecificVolumeUnitName))
		assertEquals("c = n/V", dimensionsOfProduct(AmountOfSubstanceUnitName, LongitudinCubed.reciprocal), dimensionsOf(MolarConcentrationUnitName))
		assertEquals("V_m = M/ρ", dimensionsOfProduct(MolarMassUnitName, DensityUnitName.reciprocal), dimensionsOf(MolarVolumeUnitName))
		assertEquals("S = L·t", dimensionsOfProduct(EnergyUnitName, Tempun), dimensionsOf(ActionUnitName))
		assertEquals("C = Q/ΔT", dimensionsOfProduct(EnergyUnitName, AbsoluteTemperatureUnitName.reciprocal), dimensionsOf(HeatCapacityUnitName))
		assertEquals("C_mol = C/n", dimensionsOfProduct(HeatCapacityUnitName, AmountOfSubstanceUnitName.reciprocal), dimensionsOf(MolarHeatCapacityUnitName))
		assertEquals("c = ∂C/∂m", dimensionsOfProduct(HeatCapacityUnitName, MassUnitName.reciprocal), dimensionsOf(SpecificHeatCapacityUnitName))
		assertEquals("ε = E/N", dimensionsOfProduct(EnergyUnitName, AmountOfSubstanceUnitName.reciprocal), dimensionsOf(MolarEnergyUnitName))
		//J/kg
		//J/m3
		//N/m
		//W/m2
		//assertEquals("???", dimensionsOfProduct(), dimensionsOf(ThermalConductivityUnitName));
		assertEquals("ν = μ/ρ", dimensionsOfProduct(DynamicViscosityUnitName, DensityUnitName.reciprocal), dimensionsOf(KinematicViscosityUnitName))
		assertEquals("μ = τ·(∂y/∂u)", dimensionsOfProduct(PressureUnitName, Longitudin, SpeedUnitName.reciprocal), dimensionsOf(DynamicViscosityUnitName))
		assertEquals("σ = q/A", dimensionsOfProduct(Charjon, LongitudinSquared.reciprocal), dimensionsOf(SurfaceChargeDensityUnitName))
		assertEquals("ρ = q/V", dimensionsOfProduct(Charjon, LongitudinCubed.reciprocal), dimensionsOf(ChargeDensityUnitName))
		//A/m2
		//S/m
		//S m2/mol
		assertEquals("ε = D/E", dimensionsOfProduct(DisplacementFieldStrengthUnitName, ElectricFieldStrengthUnitName.reciprocal), dimensionsOf(PermittivityUnitName))
		assertEquals("µ = B/H", dimensionsOfProduct(MagneticFluxDensityUnitName, MagneticFieldHStrengthUnitName.reciprocal), dimensionsOf(PermeabilityUnitName))
		assertEquals("E = F/q", dimensionsOfProduct(ForceUnitName, Charjon.reciprocal), dimensionsOf(ElectricFieldStrengthUnitName))
		assertEquals("M = m/V", dimensionsOfProduct(MagneticMomentUnitName, LongitudinCubed.reciprocal), dimensionsOf(MagneticFieldHStrengthUnitName))
		assertEquals("m = I·S", dimensionsOfProduct(ElectricCurrentUnitName, LongitudinSquared), dimensionsOf(MagneticMomentUnitName))
		//cd/m2
		//lm s
		//lx s
		assertEquals(dimensionsOfProduct(Charjon, MassUnitName.reciprocal), dimensionsOf(ChargeMassRatioUnitName))
		//Gy/s
		//Ω m
		//kg/m
		//C/m
		//mol/kg
		assertEquals("M = m/n", dimensionsOfProduct(MassUnitName, AmountOfSubstanceUnitName.reciprocal), dimensionsOf(MolarMassUnitName))
		//m/m3
		//kg/s
		//J/T
		//W/m3
		//K/W
		//K-1
		//K/m
		//m2/V s
		//J/m2 s
		//Pa-1
		//H-1
		//Wb/m
		//Wb m
		//T m
		//J/m2
		//m3/mol s
		//N m s/kg
		//Hz/s
		//lm/W
		//A rad
		//m/H
		//W/sr
		//W/sr m
		//W/sr m2
		//W/sr m3
		//W/m

		assertEquals(dimensionsOf(OPUSUnitlessClass.Unitless), dimensionsOf(HydrogenPotentialUnitName))
		assertEquals(dimensionsOf(OPUSUnitlessClass.Unitless), dimensionsOf(HydroxidePotentialUnitName))
		assertEquals(dimensionsOf(Revolution), dimensionsOf(OPUSRadianClass.Radian))
		assertEquals(dimensionsOf(Superrevolution), dimensionsOf(OPUSSteradianClass.Steradian))
		assertEquals(dimensionsOf(OPUSUnitOfAccountClass.UnitOfAccount), dimensionsOf(OPUSGenericDollarClass.GenericDollar))
		assertEquals(dimensionsOf(PressureUnitName), dimensionsOf(EnergyDensityUnitName))
		assertEquals(dimensionsOf(SurfaceChargeDensityUnitName), dimensionsOf(DisplacementFieldStrengthUnitName))
		assertEquals(dimensionsOf(HeatCapacityUnitName), dimensionsOf(EntropyUnitName))
	}

	@Test
	fun testNeoSI() {
		assertEquals(dimensionsOf(NeoSIUnitOfAccountClass.UnitOfAccount), dimensionsOf(NeoSIGenericDollarClass.GenericDollar))
		assertEquals(dimensionsOf(MetrePerSecond), dimensionsOf(NeoSILightspeedClass.Lightspeed))
		assertEquals(dimensionsOf(Metre), dimensionsOf(NeoSILightYearClass.Lightyear))
	}

	@Test
	fun testSI() {
		assertEquals(dimensionsOf(SIKelvinClass.Kelvin), dimensionsOf(DegreeCelsius))
		assertEquals(dimensionsOfProduct(Candela, SISteradianClass.Steradian), dimensionsOf(Lumen))
		assertEquals(dimensionsOfProduct(Lumen, SIMetreSquaredClass.MetreSquared.reciprocal), dimensionsOf(Lux))
		assertEquals(dimensionsOf(SIHertzClass.Hertz), dimensionsOf(Becquerel))
		assertEquals(dimensionsOfProduct(SIJouleClass.Joule, SIKilogramClass.Kilogram.reciprocal), dimensionsOf(Gray))
		assertEquals(dimensionsOfProduct(SIJouleClass.Joule, SIKilogramClass.Kilogram.reciprocal), dimensionsOf(Sievert))
		assertEquals(dimensionsOfProduct(SIKilogramClass.Kilogram, SIRadianPerSecondClass.RadianPerSecond, SI_ANGULAR_DIM_CONVERSION_FACTOR),
			dimensionsOf(SIJouleSecondAngularMomentumClass.JouleSecond))

		assertEquals(dimensionsOf(SISecondClass.Second), dimensionsOf(Minute))
		assertEquals(dimensionsOf(SISecondClass.Second), dimensionsOf(Hour))
		assertEquals(dimensionsOf(SISecondClass.Second), dimensionsOf(Day))
		assertEquals(dimensionsOf(SIRadianClass.Radian), dimensionsOf(Degree))
		assertEquals(dimensionsOf(SIRadianClass.Radian), dimensionsOf(MinuteOfArcClass.Minute))
		assertEquals(dimensionsOf(SIRadianClass.Radian), dimensionsOf(SecondOfArcClass.Second))
		assertEquals(dimensionsOf(SIMetreSquaredClass.MetreSquared), dimensionsOf(Hectare))
		assertEquals(dimensionsOf(SIMetreCubedClass.MetreCubed), dimensionsOf(Litre))
		assertEquals(dimensionsOf(SIKilogramClass.Kilogram), dimensionsOf(Tonne))
		assertEquals(dimensionsOf(SIJouleClass.Joule), dimensionsOf(ElectronVolt))
		assertEquals(dimensionsOf(SIKilogramClass.Kilogram), dimensionsOf(Dalton))
		assertEquals(dimensionsOf(SIMetreClass.Metre), dimensionsOf(AstronomicalUnit))
		assertEquals(dimensionsOf(SIMetrePerSecondClass.MetrePerSecond), dimensionsOf(NaturalUnitOfSpeed))
		assertEquals(dimensionsOf(SIJouleSecondActionClass.JouleSecond), dimensionsOf(NaturalUnitOfAction))
		assertEquals(dimensionsOf(SIKilogramClass.Kilogram), dimensionsOf(NaturalUnitOfMass))
		assertEquals(dimensionsOf(SISecondClass.Second), dimensionsOf(NaturalUnitOfTime))
		assertEquals(dimensionsOf(SICoulombClass.Coulomb), dimensionsOf(AtomicUnitOfCharge))
		assertEquals(dimensionsOf(SIKilogramClass.Kilogram), dimensionsOf(AtomicUnitOfMass))
		assertEquals(dimensionsOf(SIJouleSecondActionClass.JouleSecond), dimensionsOf(AtomicUnitOfAction))
		assertEquals(dimensionsOf(SIMetreClass.Metre), dimensionsOf(AtomicUnitOfLength))
		assertEquals(dimensionsOf(SIJouleClass.Joule), dimensionsOf(AtomicUnitOfEnergy))
		assertEquals(dimensionsOf(SISecondClass.Second), dimensionsOf(AtomicUnitOfTime))

		assertEquals(dimensionsOf(SIMolePerMetreCubedClass.MolePerMetreCubed), dimensionsOf(Molar))
		assertEquals(dimensionsOf(SIMetrePerSecondClass.MetrePerSecond), dimensionsOf(SILightspeedClass.Lightspeed))
		assertEquals(dimensionsOf(SIMetreClass.Metre), dimensionsOf(SILightYearClass.Lightyear))
		assertEquals(dimensionsOf(SIMetreClass.Metre), dimensionsOf(ParsecClass.Parsec))
		assertEquals(dimensionsOf(SIUnitlessClass.Unitless), dimensionsOf(SIGenericDollarClass.GenericDollar))
		assertEquals(dimensionsOf(SIPascalClass.Pascal), dimensionsOf(SIJoulePerMetreCubedClass.JoulePerMetreCubed))

		assertEquals(dimensionsOf(SIPascalClass.Pascal), dimensionsOf(Bar))
		assertEquals(dimensionsOf(SIPascalClass.Pascal), dimensionsOf(MillimetreMercury))
		assertEquals(dimensionsOf(SIMetreClass.Metre), dimensionsOf(Ångström))
		assertEquals(dimensionsOf(SIMetreClass.Metre), dimensionsOf(NauticalMile))
		assertEquals(dimensionsOf(SIMetreSquaredClass.MetreSquared), dimensionsOf(Barn))
		assertEquals(dimensionsOf(SIMetrePerSecondClass.MetrePerSecond), dimensionsOf(Knot))
	}

	@Test
	fun testEquivalency() {
		OPUS_QDIM_MAP.forEach { opus, neoSI ->
			assertEquals(neoSI.javaClass.simpleName, dimensionsOf(neoSI), dimensionsOf(opus.getNeoISQEquivalent()))
		}
		NEO_SI_QDIM_MAP.forEach { neoSI, si ->
			assertEquals(si.javaClass.simpleName, dimensionsOf(si), dimensionsOf(neoSI.getISQEquivalent()))
		}
	}
}
