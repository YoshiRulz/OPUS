package dev.yoshirulz.opus.test

import dev.yoshirulz.opus.LibraryGlobals
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.QuantityDimension
import dev.yoshirulz.opus.quantity.ApproximableQuantity
import dev.yoshirulz.opus.quantity.DecimalCalculable
import dev.yoshirulz.opus.quantity.Quantity
import dev.yoshirulz.opus.quantity.SIApproximableConstant
import dev.yoshirulz.opus.units.*
import dev.yoshirulz.opus.units.AbsoluteTemperatureUnitNameClass.AbsoluteTemperatureUnitNames
import dev.yoshirulz.opus.units.AccelerationUnitNameClass.AccelerationUnitNames
import dev.yoshirulz.opus.units.ActionUnitNameClass.ActionUnitNames
import dev.yoshirulz.opus.units.AmountOfSubstanceUnitNameClass.AmountOfSubstanceUnitNames
import dev.yoshirulz.opus.units.AngularAccelerationUnitNameClass.AngularAccelerationUnitNames
import dev.yoshirulz.opus.units.AngularJerkUnitNameClass.AngularJerkUnitNames
import dev.yoshirulz.opus.units.AngularMomentumUnitNameClass.AngularMomentumUnitNames
import dev.yoshirulz.opus.units.AngularSpeedUnitNameClass.AngularSpeedUnitNames
import dev.yoshirulz.opus.units.AreaDensityUnitNameClass.AreaDensityUnitNames
import dev.yoshirulz.opus.units.AstronomicalUnitClass.AstronomicalUnits
import dev.yoshirulz.opus.units.AtomicUnitOfActionClass.AtomicUnitsOfAction
import dev.yoshirulz.opus.units.AtomicUnitOfChargeClass.AtomicUnitsOfCharge
import dev.yoshirulz.opus.units.AtomicUnitOfEnergyClass.AtomicUnitsOfEnergy
import dev.yoshirulz.opus.units.AtomicUnitOfEnergyClass.Hartrees
import dev.yoshirulz.opus.units.AtomicUnitOfLengthClass.AtomicUnitsOfLength
import dev.yoshirulz.opus.units.AtomicUnitOfLengthClass.Bohrs
import dev.yoshirulz.opus.units.AtomicUnitOfMassClass.AtomicUnitsOfMass
import dev.yoshirulz.opus.units.AtomicUnitOfTimeClass.AtomicUnitsOfTime
import dev.yoshirulz.opus.units.BarClass.Bars
import dev.yoshirulz.opus.units.BarnClass.Barns
import dev.yoshirulz.opus.units.BecquerelClass.Becquerels
import dev.yoshirulz.opus.units.BelClass.Bels
import dev.yoshirulz.opus.units.CandelaClass.Candelas
import dev.yoshirulz.opus.units.CatalyticActivityUnitNameClass.CatalyticActivityUnitNames
import dev.yoshirulz.opus.units.ChargeDensityUnitNameClass.ChargeDensityUnitNames
import dev.yoshirulz.opus.units.ChargeMassRatioUnitNameClass.ChargeMassRatioUnitNames
import dev.yoshirulz.opus.units.CharjonClass.Charjons
import dev.yoshirulz.opus.units.DaltonClass.Daltons
import dev.yoshirulz.opus.units.DayClass.Days
import dev.yoshirulz.opus.units.DegreeCelsiusClass.DegreesCelsius
import dev.yoshirulz.opus.units.DegreeOfArcClass.Degrees
import dev.yoshirulz.opus.units.DensityUnitNameClass.DensityUnitNames
import dev.yoshirulz.opus.units.DisplacementFieldStrengthUnitNameClass.DisplacementFieldStrengthUnitNames
import dev.yoshirulz.opus.units.DynamicViscosityUnitNameClass.DynamicViscosityUnitNames
import dev.yoshirulz.opus.units.ElectricCurrentUnitNameClass.ElectricCurrentUnitNames
import dev.yoshirulz.opus.units.ElectricFieldStrengthUnitNameClass.ElectricFieldStrengthUnitNames
import dev.yoshirulz.opus.units.ElectricalCapacitanceUnitNameClass.ElectricalCapacitanceUnitNames
import dev.yoshirulz.opus.units.ElectricalConductanceUnitNameClass.ElectricalConductanceUnitNames
import dev.yoshirulz.opus.units.ElectricalInductanceUnitNameClass.ElectricalInductanceUnitNames
import dev.yoshirulz.opus.units.ElectricalResistanceUnitNameClass.ElectricalResistanceUnitNames
import dev.yoshirulz.opus.units.ElectronVoltClass.ElectronVolts
import dev.yoshirulz.opus.units.ElectrostaticPotentialUnitNameClass.ElectrostaticPotentialUnitNames
import dev.yoshirulz.opus.units.EnergyUnitNameClass.EnergyUnitNames
import dev.yoshirulz.opus.units.EntropyUnitNameClass.EntropyUnitNames
import dev.yoshirulz.opus.units.ForceUnitNameClass.ForceUnitNames
import dev.yoshirulz.opus.units.FrequencyUnitNameClass.FrequencyUnitNames
import dev.yoshirulz.opus.units.GrayClass.Grays
import dev.yoshirulz.opus.units.HectareClass.Hectares
import dev.yoshirulz.opus.units.HourClass.Hours
import dev.yoshirulz.opus.units.JerkUnitNameClass.JerkUnitNames
import dev.yoshirulz.opus.units.KinematicViscosityUnitNameClass.KinematicViscosityUnitNames
import dev.yoshirulz.opus.units.KnotClass.Knots
import dev.yoshirulz.opus.units.LitreClass.Litres
import dev.yoshirulz.opus.units.LongitudinClass.Longitudins
import dev.yoshirulz.opus.units.LongitudinCubedClass.LongitudinsCubed
import dev.yoshirulz.opus.units.LongitudinSquaredClass.LongitudinsSquared
import dev.yoshirulz.opus.units.LumenClass.Lumens
import dev.yoshirulz.opus.units.MagneticFieldHStrengthUnitNameClass.MagneticFieldHStrengthUnitNames
import dev.yoshirulz.opus.units.MagneticFluxDensityUnitNameClass.MagneticFluxDensityUnitNames
import dev.yoshirulz.opus.units.MagneticFluxUnitNameClass.MagneticFluxUnitNames
import dev.yoshirulz.opus.units.MagneticMomentUnitNameClass.MagneticMomentUnitNames
import dev.yoshirulz.opus.units.MassUnitNameClass.MassUnitNames
import dev.yoshirulz.opus.units.MillimetreMercuryClass.MillimetresMercury
import dev.yoshirulz.opus.units.MinuteClass.Minutes
import dev.yoshirulz.opus.units.MolarConcentrationUnitNameClass.MolarConcentrationUnitNames
import dev.yoshirulz.opus.units.MolarEnergyUnitNameClass.MolarEnergyUnitNames
import dev.yoshirulz.opus.units.MolarHeatCapacityUnitNameClass.MolarHeatCapacityUnitNames
import dev.yoshirulz.opus.units.MolarMassUnitNameClass.MolarMassUnitNames
import dev.yoshirulz.opus.units.MolarVolumeUnitNameClass.MolarVolumeUnitNames
import dev.yoshirulz.opus.units.MomentumUnitNameClass.MomentumUnitNames
import dev.yoshirulz.opus.units.NaturalUnitOfActionClass.NaturalUnitsOfAction
import dev.yoshirulz.opus.units.NaturalUnitOfMassClass.NaturalUnitsOfMass
import dev.yoshirulz.opus.units.NaturalUnitOfSpeedClass.NaturalUnitsOfSpeed
import dev.yoshirulz.opus.units.NaturalUnitOfTimeClass.NaturalUnitsOfTime
import dev.yoshirulz.opus.units.NauticalMileClass.NauticalMiles
import dev.yoshirulz.opus.units.NeoSIAmpereClass.Amperes
import dev.yoshirulz.opus.units.NeoSIAmperePerMetreClass.AmperesPerMetre
import dev.yoshirulz.opus.units.NeoSICoulombClass.Coulombs
import dev.yoshirulz.opus.units.NeoSICoulombPerKilogramClass.CoulombsPerKilogram
import dev.yoshirulz.opus.units.NeoSICoulombPerMetreCubedClass.CoulombsPerMetreCubed
import dev.yoshirulz.opus.units.NeoSICoulombPerMetreSquaredClass.CoulombsPerMetreSquared
import dev.yoshirulz.opus.units.NeoSIFaradClass.Farads
import dev.yoshirulz.opus.units.NeoSIFaradPerMetreClass.FaradsPerMetre
import dev.yoshirulz.opus.units.NeoSIHenryClass.Henries
import dev.yoshirulz.opus.units.NeoSIHenryPerMetreClass.HenriesPerMetre
import dev.yoshirulz.opus.units.NeoSIJouleClass.Joules
import dev.yoshirulz.opus.units.NeoSIJoulePerKelvinHeatCapacityClass.JoulesPerKelvin
import dev.yoshirulz.opus.units.NeoSIJoulePerKilogramClass.JoulesPerKilogram
import dev.yoshirulz.opus.units.NeoSIJoulePerKilogramKelvinClass.JoulesPerKilogramKelvin
import dev.yoshirulz.opus.units.NeoSIJoulePerMetreCubedClass.JoulesPerMetreCubed
import dev.yoshirulz.opus.units.NeoSIJoulePerMoleClass.JoulesPerMole
import dev.yoshirulz.opus.units.NeoSIJoulePerMoleKelvinClass.JoulesPerMoleKelvin
import dev.yoshirulz.opus.units.NeoSIJoulePerRadianClass.JoulesPerRadian
import dev.yoshirulz.opus.units.NeoSIJoulePerTeslaClass.JoulesPerTesla
import dev.yoshirulz.opus.units.NeoSIJouleSecondActionClass.JouleSeconds
import dev.yoshirulz.opus.units.NeoSIKatalClass.Katals
import dev.yoshirulz.opus.units.NeoSIKelvinClass.Kelvins
import dev.yoshirulz.opus.units.NeoSIKilogramClass.Kilograms
import dev.yoshirulz.opus.units.NeoSIKilogramPerMetreCubedClass.KilogramsPerMetreCubed
import dev.yoshirulz.opus.units.NeoSIKilogramPerMetreSquaredClass.KilogramsPerMetreSquared
import dev.yoshirulz.opus.units.NeoSIKilogramPerMoleClass.KilogramsPerMole
import dev.yoshirulz.opus.units.NeoSIMetreClass.Metres
import dev.yoshirulz.opus.units.NeoSIMetreCubedPerKilogramClass.MetresCubedPerKilogram
import dev.yoshirulz.opus.units.NeoSIMetreCubedPerMoleClass.MetresCubedPerMole
import dev.yoshirulz.opus.units.NeoSIMetreCubedPerSecondClass.MetresCubedPerSecond
import dev.yoshirulz.opus.units.NeoSIMetrePerSecondClass.MetresPerSecond
import dev.yoshirulz.opus.units.NeoSIMetrePerSecondCubedClass.MetresPerSecondCubed
import dev.yoshirulz.opus.units.NeoSIMetrePerSecondSquaredClass.MetresPerSecondSquared
import dev.yoshirulz.opus.units.NeoSIMetreSquaredClass.MetresSquared
import dev.yoshirulz.opus.units.NeoSIMetreSquaredPerSecondClass.MetresSquaredPerSecond
import dev.yoshirulz.opus.units.NeoSIMoleClass.Moles
import dev.yoshirulz.opus.units.NeoSIMolePerMetreCubedClass.MolesPerMetreCubed
import dev.yoshirulz.opus.units.NeoSINewtonClass.Newtons
import dev.yoshirulz.opus.units.NeoSINewtonMetrePerSecondClass.NewtonMetresPerSecond
import dev.yoshirulz.opus.units.NeoSINewtonPerCoulombClass.NewtonsPerCoulomb
import dev.yoshirulz.opus.units.NeoSINewtonPerMetreClass.NewtonsPerMetre
import dev.yoshirulz.opus.units.NeoSINewtonPerSecondClass.NewtonsPerSecond
import dev.yoshirulz.opus.units.NeoSINewtonSecondClass.NewtonSeconds
import dev.yoshirulz.opus.units.NeoSIOhmClass.Ohms
import dev.yoshirulz.opus.units.NeoSIPascalClass.Pascals
import dev.yoshirulz.opus.units.NeoSIPascalSecondClass.PascalSeconds
import dev.yoshirulz.opus.units.NeoSIRadianClass.Radians
import dev.yoshirulz.opus.units.NeoSIRadianPerSecondClass.RadiansPerSecond
import dev.yoshirulz.opus.units.NeoSIRadianPerSecondCubedClass.RadiansPerSecondCubed
import dev.yoshirulz.opus.units.NeoSIRadianPerSecondSquaredClass.RadiansPerSecondSquared
import dev.yoshirulz.opus.units.NeoSIReciprocalMetreClass.ReciprocalMetres
import dev.yoshirulz.opus.units.NeoSISecondClass.Seconds
import dev.yoshirulz.opus.units.NeoSISteradianClass.Steradians
import dev.yoshirulz.opus.units.NeoSITeslaClass.Teslas
import dev.yoshirulz.opus.units.NeoSIVoltClass.Volts
import dev.yoshirulz.opus.units.NeoSIWattClass.Watts
import dev.yoshirulz.opus.units.NeoSIWattPerMetreKelvinClass.WattsPerMetreKelvin
import dev.yoshirulz.opus.units.NeoSIWeberClass.Webers
import dev.yoshirulz.opus.units.NeperClass.Nepers
import dev.yoshirulz.opus.units.PermeabilityUnitNameClass.PermeabilityUnitNames
import dev.yoshirulz.opus.units.PermittivityUnitNameClass.PermittivityUnitNames
import dev.yoshirulz.opus.units.PowerUnitNameClass.PowerUnitNames
import dev.yoshirulz.opus.units.PressureUnitNameClass.PressureUnitNames
import dev.yoshirulz.opus.units.RevolutionClass.Revolutions
import dev.yoshirulz.opus.units.RotatumUnitNameClass.RotatumUnitNames
import dev.yoshirulz.opus.units.SievertClass.Sieverts
import dev.yoshirulz.opus.units.SpecificEnergyUnitNameClass.SpecificEnergyUnitNames
import dev.yoshirulz.opus.units.SpecificHeatCapacityUnitNameClass.SpecificHeatCapacityUnitNames
import dev.yoshirulz.opus.units.SpecificVolumeUnitNameClass.SpecificVolumeUnitNames
import dev.yoshirulz.opus.units.SpeedUnitNameClass.SpeedUnitNames
import dev.yoshirulz.opus.units.SuperrevolutionClass.Superrevolutions
import dev.yoshirulz.opus.units.SurfaceChargeDensityUnitNameClass.SurfaceChargeDensityUnitNames
import dev.yoshirulz.opus.units.SurfaceTensionUnitNameClass.SurfaceTensionUnitNames
import dev.yoshirulz.opus.units.SymbolCountUnitNameClass.SymbolCountUnitNames
import dev.yoshirulz.opus.units.TempunClass.Tempuns
import dev.yoshirulz.opus.units.ThermalConductivityUnitNameClass.ThermalConductivityUnitNames
import dev.yoshirulz.opus.units.TonneClass.Tonnes
import dev.yoshirulz.opus.units.TorqueUnitNameClass.TorqueUnitNames
import dev.yoshirulz.opus.units.VolumetricFlowRateUnitNameClass.VolumetricFlowRateUnitNames
import dev.yoshirulz.opus.units.WavenumberUnitNameClass.WavenumberUnitNames
import dev.yoshirulz.opus.units.YankUnitNameClass.YankUnitNames
import dev.yoshirulz.opus.units.ÅngströmClass.Ångströms
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

/** @see License */
class UnitQuantityTest {
	companion object {
		private const val DELTA = 0.0
		private fun r() = Math.random()
		private val NEOSI_PLURALS = arrayOf<NeoSIUnit<*>>(Metres, Kilograms, Seconds, Amperes, Radians, Kelvins, Moles, NeoSIUnitOfAccountClass.UnitsOfAccount,
			Radians, Steradians, Newtons, Pascals, Joules, Watts, Coulombs, Volts, Farads, Ohms, Webers, Teslas, Henries, Katals,
			NeoSIGenericDollarClass.GenericDollars, NeoSILightYearClass.Lightyears,
			AmperesPerMetre, CoulombsPerKilogram, CoulombsPerMetreCubed, CoulombsPerMetreSquared, FaradsPerMetre, HenriesPerMetre, NeoSIJoulePerKelvinEntropyClass.JoulesPerKelvin, JoulesPerKelvin, JoulesPerKilogram, JoulesPerKilogramKelvin, JoulesPerMetreCubed, JoulesPerMole, JoulesPerMoleKelvin, JoulesPerRadian, JoulesPerTesla, JouleSeconds, KilogramsPerMetreCubed, KilogramsPerMetreSquared, KilogramsPerMole, NeoSIMetreCubedClass.MetresCubed, MetresCubedPerKilogram, MetresCubedPerMole, MetresCubedPerSecond, MetresPerSecond, MetresPerSecondCubed, MetresPerSecondSquared, MetresSquared, MetresSquaredPerSecond, MolesPerMetreCubed, NewtonMetresPerSecond, NewtonsPerCoulomb, NewtonsPerMetre, NewtonsPerSecond, NewtonSeconds, PascalSeconds, RadiansPerSecond, RadiansPerSecondCubed, RadiansPerSecondSquared, ReciprocalMetres, WattsPerMetreKelvin)
		private val OPUS_PLURALS = arrayOf<OPUSUnit<*>>(Tempuns, Longitudins, MassUnitNames, Charjons, Revolutions, AbsoluteTemperatureUnitNames, AmountOfSubstanceUnitNames, SymbolCountUnitNames, OPUSUnitOfAccountClass.UnitsOfAccount,
			OPUSGenericDollarClass.GenericDollars, LongitudinsCubed, LongitudinsSquared, OPUSRadianClass.Radians, OPUSSteradianClass.Steradians, Superrevolutions,
			AccelerationUnitNames, ActionUnitNames, AngularAccelerationUnitNames, AngularJerkUnitNames, AngularMomentumUnitNames, AngularSpeedUnitNames, AreaDensityUnitNames, CatalyticActivityUnitNames, ChargeDensityUnitNames, ChargeMassRatioUnitNames, DensityUnitNames, DisplacementFieldStrengthUnitNames, DynamicViscosityUnitNames, ElectricalCapacitanceUnitNames, ElectricalConductanceUnitNames, ElectricalInductanceUnitNames, ElectricalResistanceUnitNames, ElectricCurrentUnitNames, ElectrostaticPotentialUnitNames, ElectricFieldStrengthUnitNames, EnergyUnitNames, EntropyUnitNames, ForceUnitNames, FrequencyUnitNames, JerkUnitNames, KinematicViscosityUnitNames, MagneticFieldHStrengthUnitNames, MagneticFluxUnitNames, MagneticFluxDensityUnitNames, MagneticMomentUnitNames, MolarConcentrationUnitNames, MolarEnergyUnitNames, MolarHeatCapacityUnitNames, MolarMassUnitNames, MolarVolumeUnitNames, MomentumUnitNames, PermeabilityUnitNames, PermittivityUnitNames, PowerUnitNames, PressureUnitNames, RotatumUnitNames, SpecificEnergyUnitNames, SpecificHeatCapacityUnitNames, SpecificVolumeUnitNames, SpeedUnitNames, SurfaceChargeDensityUnitNames, SurfaceTensionUnitNames, ThermalConductivityUnitNames, TorqueUnitNames, VolumetricFlowRateUnitNames, WavenumberUnitNames, YankUnitNames)
		private val SI_PLURALS = arrayOf<SIUnit<*>>(SIMetreClass.Metres, SIKilogramClass.Kilograms, SecondOfArcClass.Seconds, SIAmpereClass.Amperes, SIKelvinClass.Kelvins, SIMoleClass.Moles, Candelas,
			SIRadianClass.Radians, SISteradianClass.Steradians, SINewtonClass.Newtons, SIPascalClass.Pascals, SIJouleClass.Joules, SIWattClass.Watts, SICoulombClass.Coulombs, SIVoltClass.Volts, SIFaradClass.Farads, SIOhmClass.Ohms, SIWeberClass.Webers, SITeslaClass.Teslas, SIHenryClass.Henries, DegreesCelsius, Lumens, Becquerels, Grays, Sieverts, SIKatalClass.Katals,
			SIGenericDollarClass.GenericDollars, SILightYearClass.Lightyears, ParsecClass.Parsecs,
			SIAmperePerMetreClass.AmperesPerMetre, SICoulombPerKilogramClass.CoulombsPerKilogram, SICoulombPerMetreCubedClass.CoulombsPerMetreCubed, SICoulombPerMetreSquaredClass.CoulombsPerMetreSquared, SIFaradPerMetreClass.FaradsPerMetre, SIHenryPerMetreClass.HenriesPerMetre, SIJoulePerKelvinEntropyClass.JoulesPerKelvin, SIJoulePerKelvinHeatCapacityClass.JoulesPerKelvin, SIJoulePerKilogramClass.JoulesPerKilogram, SIJoulePerKilogramKelvinClass.JoulesPerKilogramKelvin, SIJoulePerMetreCubedClass.JoulesPerMetreCubed, SIJoulePerMoleClass.JoulesPerMole, SIJoulePerMoleKelvinClass.JoulesPerMoleKelvin, SIJoulePerRadianClass.JoulesPerRadian, SIJoulePerTeslaClass.JoulesPerTesla, SIJouleSecondActionClass.JouleSeconds, SIJouleSecondAngularMomentumClass.JouleSeconds, SIKilogramPerMetreCubedClass.KilogramsPerMetreCubed, SIKilogramPerMetreSquaredClass.KilogramsPerMetreSquared, SIKilogramPerMoleClass.KilogramsPerMole, SIMetreCubedClass.MetresCubed, SIMetreCubedPerKilogramClass.MetresCubedPerKilogram, SIMetreCubedPerMoleClass.MetresCubedPerMole, SIMetreCubedPerSecondClass.MetresCubedPerSecond, SIMetrePerSecondClass.MetresPerSecond, SIMetrePerSecondCubedClass.MetresPerSecondCubed, SIMetrePerSecondSquaredClass.MetresPerSecondSquared, SIMetreSquaredClass.MetresSquared, SIMetreSquaredPerSecondClass.MetresSquaredPerSecond, SIMolePerMetreCubedClass.MolesPerMetreCubed, SINewtonMetrePerSecondClass.NewtonMetresPerSecond, SINewtonPerCoulombClass.NewtonsPerCoulomb, SINewtonPerMetreClass.NewtonsPerMetre, SINewtonPerSecondClass.NewtonsPerSecond, SINewtonSecondClass.NewtonSeconds, SIPascalSecondClass.PascalSeconds, SIRadianPerSecondClass.RadiansPerSecond, SIRadianPerSecondCubedClass.RadiansPerSecondCubed, SIRadianPerSecondSquaredClass.RadiansPerSecondSquared, SIReciprocalMetreClass.ReciprocalMetres, SIWattPerMetreKelvinClass.WattsPerMetreKelvin,
			Ångströms, AstronomicalUnits, AtomicUnitsOfAction, AtomicUnitsOfCharge, AtomicUnitsOfEnergy, AtomicUnitsOfLength, AtomicUnitsOfMass, AtomicUnitsOfTime, Barns, Bars, Bels, Bohrs, Daltons, Days, Degrees, ElectronVolts, Hartrees, Hectares, Hours, Knots, Litres, MillimetresMercury, Minutes, MinuteOfArcClass.Minutes, NaturalUnitsOfAction, NaturalUnitsOfMass, NaturalUnitsOfSpeed, NaturalUnitsOfTime, NauticalMiles, Nepers, SecondOfArcClass.Seconds, Tonnes)
	}
	private fun <D: QuantityDimension<S>, S: UnitSystem> conditionalPrint(u: UnitInterface<D, S>): Boolean {
		if (LibraryGlobals.isDevMode)
			System.out.println("createMapEntry(\"${u.qDimension.group}\", ${u.qDimension.javaClass.simpleName}.QDIM),")
		return true
	}
	@Test
	fun printNeoSIMapToNamedSwitches() { NEOSI_PLURALS.forEach { u -> assertTrue(conditionalPrint(u)) } }
	@Test
	fun printOPUSMapToNamedSwitches() { OPUS_PLURALS.forEach { u -> assertTrue(conditionalPrint(u)) } }
	@Test
	fun printSIMapToNamedSwitches() { SI_PLURALS.forEach { u -> assertTrue(conditionalPrint(u)) } }

	@Test
	fun testNamedUnits() {
		for (u in NEOSI_PLURALS) r().apply { assertEquals(this,
			Quantity(this, u).exactValue.calcTo32bFloat(), DELTA) }
		for (u in OPUS_PLURALS) r().apply { assertEquals(this.toFloat(),
			Quantity(this, u).exactValue.calcTo16bFloat(), DELTA.toFloat()) }
		for (u in SI_PLURALS) StrictMath.exp(r()).toLong().apply { assertEquals(this,
			Quantity(this, u).exactValue.calcTo32bFloat().toLong()) }
	}

	//TODO remove after implementing pi approximator
	@Test
	fun satisfyInspector() {
		r().apply { assertEquals(this, ApproximableQuantity(DecimalCalculable(this), Metres).doubleApprox, DELTA) }
		assertEquals("5.2728584E-35", SIApproximableConstant.FERMION_SPIN.floatApprox.toString())
	}
}
