package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.TemperatureDimPowers.AbsoluteTemperatureElement
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.AmountOfSubstanceElement
import dev.yoshirulz.opus.qdims.AbelianAngle.AngleDimPowers.AngleElement
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ElectricCurrentDimPowers.ElectricCurrentElement
import dev.yoshirulz.opus.qdims.AbelianFinancialValue.FinancialValueDimPowers.FinancialValueElement
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.LengthElement
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.OmitLength
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.MassElement
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.OmitMass
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.OmitTime
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.TimeElement

/** @see License */
enum class NeoISQDimensionOne: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, OmitTime)
	override fun toString() = "1"
}
/** @see License */
enum class NeoISQLength: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, OmitMass, OmitTime)
	override fun timeDerivative() = NeoISQSpeed.QDIM
	override fun toString() = "L"
}
/** @see License */
enum class NeoISQMass: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, MassElement, OmitTime)
	override fun toString() = "M"
}
/** @see License */
enum class NeoISQTime: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, TimeElement)
	override fun timeDerivative() = NeoISQDimensionOne.QDIM
	override fun toString() = "T"
}
/** @see License */
enum class NeoISQElectricCurrent: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, OmitTime, ElectricCurrentElement)
	override fun toString() = "I"
}
/** @see License */
enum class NeoISQAngle: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, OmitTime, angle = AngleElement)
	override fun timeDerivative() = NeoISQAngularSpeed.QDIM
	override fun toString() = "R"
}
/** @see License */
enum class NeoISQAbsoluteTemperature: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, OmitTime, temperature = AbsoluteTemperatureElement)
	override fun toString() = "Θ"
}
/** @see License */
enum class NeoISQAmountOfSubstance: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, OmitTime, amount = AmountOfSubstanceElement)
	override fun toString() = "N"
}
/** @see License */
enum class NeoISQFinancialValue: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, OmitTime, financialValue = FinancialValueElement)
	override fun toString() = "V"
}
