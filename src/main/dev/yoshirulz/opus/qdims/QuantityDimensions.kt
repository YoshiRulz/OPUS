@file:Suppress("REDUNDANT_ELSE_IN_WHEN")

package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.LibraryGlobals.DimensionSymbolName
import dev.yoshirulz.opus.LibraryGlobals.DimensionSymbolName.*
import dev.yoshirulz.opus.LibraryGlobals.i18nGetCanonicalDimensionSymbol
import dev.yoshirulz.opus.LibraryGlobals.i18nGetDimensionSymbol
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.TemperatureDimPowers.OmitAbsoluteTemperature
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.OmitAmountOfSubstance
import dev.yoshirulz.opus.qdims.AbelianAngle.AngleDimPowers.OmitAngle
import dev.yoshirulz.opus.qdims.AbelianElectricCharge.ElectricChargeDimPowers.OmitElectricCharge
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ElectricCurrentDimPowers.OmitElectricCurrent
import dev.yoshirulz.opus.qdims.AbelianFinancialValue.FinancialValueDimPowers.OmitFinancialValue
import dev.yoshirulz.opus.qdims.AbelianLuminousIntensity.LuminousIntensityDimPowers.OmitLuminousIntensity
import dev.yoshirulz.opus.qdims.AbelianSymbolCount.SymbolCountDimPowers.OmitSymbolCount
import dev.yoshirulz.opus.units.UnitSystem
import dev.yoshirulz.opus.units.UnitSystem.*
import java.util.*

fun abelianElementStrFormat(i: Int, s: String) = if (i == 0) "" else if (i == 1) s else "$s^$i"
fun abelianGroupStrFormat(g: Set<AbelianElement>, canonical: Boolean = false): String {
	val sjPos = StringJoiner("·")
	val sjNeg = StringJoiner("·")
	for (e in g) {
		val s = if (canonical) i18nGetCanonicalDimensionSymbol(e.symbol) else i18nGetDimensionSymbol(e.symbol)
		if (e.exponent > 0) sjPos.add(if (e.exponent > 1) s + e.exponent else s)
		else if (e.exponent < 0) sjNeg.add(s + e.exponent)
	}
	val pos = sjPos.toString()
	val neg = sjNeg.toString()
	val sj = StringJoiner("·")
	if (!pos.isEmpty()) sj.add(pos)
	if (!neg.isEmpty()) sj.add(neg)
	val s = sj.toString()
	return if (s.isEmpty()) "1" else s
}
fun abelianGroupCanonicalFormat(g: Set<AbelianElement>) = abelianGroupStrFormat(g, true)

/** @see License */
interface AbelianElement {
	val exponent: Int
	val symbol: DimensionSymbolName
}

/** @see License */
interface AbelianAbsoluteTemperature: AbelianElement {
	companion object {
		const val SYMBOL = "Θ"
	}
	val reciprocal: AbelianAbsoluteTemperature
	override val symbol get() = Temperature
	enum class TemperatureDimPowers(override val exponent: Int): AbelianAbsoluteTemperature {
		OmitAbsoluteTemperature(0),
		AbsoluteTemperatureElement(1),
		ReciprocalAbsoluteTemperature(-1);
		override val reciprocal: AbelianAbsoluteTemperature get() = when (this) {
			AbsoluteTemperatureElement -> ReciprocalAbsoluteTemperature
			OmitAbsoluteTemperature -> OmitAbsoluteTemperature
			ReciprocalAbsoluteTemperature -> AbsoluteTemperatureElement
			else -> ArbitraryTemperatureDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryTemperatureDim(override val exponent: Int): AbelianAbsoluteTemperature {
		override val reciprocal: AbelianAbsoluteTemperature get() = ArbitraryTemperatureDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianAmountOfSubstance: AbelianElement {
	companion object {
		const val SYMBOL = "N"
	}
	val reciprocal: AbelianAmountOfSubstance
	override val symbol get() = Amount
	enum class AmountOfSubstanceDimPowers(override val exponent: Int): AbelianAmountOfSubstance {
		OmitAmountOfSubstance(0),
		AmountOfSubstanceElement(1),
		ReciprocalAmountOfSubstance(-1);
		override val reciprocal: AbelianAmountOfSubstance get() = when (this) {
			AmountOfSubstanceElement -> ReciprocalAmountOfSubstance
			OmitAmountOfSubstance -> OmitAmountOfSubstance
			ReciprocalAmountOfSubstance -> AmountOfSubstanceElement
			else -> ArbitraryAmountDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryAmountDim(override val exponent: Int): AbelianAmountOfSubstance {
		override val reciprocal: AbelianAmountOfSubstance get() = ArbitraryAmountDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianAngle: AbelianElement {
	companion object {
		const val SYMBOL = "R"
	}
	val reciprocal: AbelianAngle
	override val symbol get() = Angle
	enum class AngleDimPowers(override val exponent: Int): AbelianAngle {
		OmitAngle(0),
		AngleElement(1), AngleSquared(2),
		ReciprocalAngle(-1);
		override val reciprocal: AbelianAngle get() = when (this) {
			AngleElement -> ReciprocalAngle
			OmitAngle -> OmitAngle
			ReciprocalAngle -> AngleElement
			else -> ArbitraryAngleDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryAngleDim(override val exponent: Int): AbelianAngle {
		override val reciprocal: AbelianAngle get() = ArbitraryAngleDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianElectricCharge: AbelianElement {
	companion object {
		const val SYMBOL = "Q"
	}
	val reciprocal: AbelianElectricCharge
	override val symbol get() = Charge
	enum class ElectricChargeDimPowers(override val exponent: Int): AbelianElectricCharge {
		OmitElectricCharge(0),
		ElectricChargeElement(1), ElectricChargeSquared(2),
		ReciprocalElectricCharge(-1), ReciprocalElectricChargeSquared(-2);
		override val reciprocal: AbelianElectricCharge get() = when (this) {
			ElectricChargeSquared -> ReciprocalElectricChargeSquared
			ElectricChargeElement -> ReciprocalElectricCharge
			OmitElectricCharge -> OmitElectricCharge
			ReciprocalElectricCharge -> ElectricChargeElement
			ReciprocalElectricChargeSquared -> ElectricChargeSquared
			else -> ArbitraryChargeDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryChargeDim(override val exponent: Int): AbelianElectricCharge {
		override val reciprocal: AbelianElectricCharge get() = ArbitraryChargeDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianElectricCurrent: AbelianElement {
	companion object {
		const val SYMBOL = "I"
	}
	val reciprocal: AbelianElectricCurrent
	override val symbol get() = Current
	enum class ElectricCurrentDimPowers(override val exponent: Int): AbelianElectricCurrent {
		OmitElectricCurrent(0),
		ElectricCurrentElement(1), ElectricCurrentSquared(2),
		ReciprocalElectricCurrent(-1), ReciprocalElectricCurrentSquared(-2);
		override val reciprocal: AbelianElectricCurrent get() = when (this) {
			ElectricCurrentSquared -> ReciprocalElectricCurrentSquared
			ElectricCurrentElement -> ReciprocalElectricCurrent
			OmitElectricCurrent -> OmitElectricCurrent
			ReciprocalElectricCurrent -> ElectricCurrentElement
			ReciprocalElectricCurrentSquared -> ElectricCurrentSquared
			else -> ArbitraryCurrentDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryCurrentDim(override val exponent: Int): AbelianElectricCurrent {
		override val reciprocal: AbelianElectricCurrent get() = ArbitraryCurrentDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianFinancialValue: AbelianElement {
	companion object {
		const val SYMBOL = "V"
	}
	val reciprocal: AbelianFinancialValue
	override val symbol get() = FinancialValue
	enum class FinancialValueDimPowers(override val exponent: Int): AbelianFinancialValue {
		OmitFinancialValue(0),
		FinancialValueElement(1);
		override val reciprocal: AbelianFinancialValue get() = when (this) {
			OmitFinancialValue -> OmitFinancialValue
			else -> ArbitraryFinancialValueDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryFinancialValueDim(override val exponent: Int): AbelianFinancialValue {
		override val reciprocal: AbelianFinancialValue get() = ArbitraryFinancialValueDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianLength: AbelianElement {
	companion object {
		const val SYMBOL = "L"
	}
	val reciprocal: AbelianLength
	override val symbol get() = Length
	enum class LengthDimPowers(override val exponent: Int): AbelianLength {
		OmitLength(0),
		LengthElement(1), LengthSquared(2), LengthCubed(3),
		ReciprocalLength(-1), ReciprocalLengthSquared(-2), ReciprocalLengthCubed(-3);
		override val reciprocal: AbelianLength get() = when (this) {
			LengthCubed -> ReciprocalLengthCubed
			LengthSquared -> ReciprocalLengthSquared
			LengthElement -> ReciprocalLength
			OmitLength -> OmitLength
			ReciprocalLength -> LengthElement
			ReciprocalLengthSquared -> LengthSquared
			ReciprocalLengthCubed -> LengthCubed
			else -> ArbitraryLengthDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryLengthDim(override val exponent: Int): AbelianLength {
		override val reciprocal: AbelianLength get() = ArbitraryLengthDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianLuminousIntensity: AbelianElement {
	companion object {
		const val SYMBOL = "J"
	}
	val reciprocal: AbelianLuminousIntensity
	override val symbol get() = LuminousIntensity
	enum class LuminousIntensityDimPowers(override val exponent: Int): AbelianLuminousIntensity {
		OmitLuminousIntensity(0),
		LuminousIntensityElement(1);
		override val reciprocal: AbelianLuminousIntensity get() = when (this) {
			OmitLuminousIntensity -> OmitLuminousIntensity
			else -> ArbitraryLuminousIntensityDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}

	/**
	 * The most arbitrary.
	 */
	class ArbitraryLuminousIntensityDim(override val exponent: Int): AbelianLuminousIntensity {
		override val reciprocal: AbelianLuminousIntensity get() = ArbitraryLuminousIntensityDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianMass: AbelianElement {
	companion object {
		const val SYMBOL = "M"
	}
	val reciprocal: AbelianMass
	override val symbol get() = Mass
	enum class MassDimPowers(override val exponent: Int): AbelianMass {
		OmitMass(0),
		MassElement(1),
		ReciprocalMass(-1);
		override val reciprocal: AbelianMass get() = when (this) {
			MassElement -> ReciprocalMass
			OmitMass -> OmitMass
			ReciprocalMass -> MassElement
			else -> ArbitraryMassDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryMassDim(override val exponent: Int): AbelianMass {
		override val reciprocal: AbelianMass get() = ArbitraryMassDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianSymbolCount: AbelianElement {
	companion object {
		const val SYMBOL = "Η"
	}
	val reciprocal: AbelianSymbolCount
	override val symbol get() = SymbolCount
	enum class SymbolCountDimPowers(override val exponent: Int): AbelianSymbolCount {
		OmitSymbolCount(0),
		SymbolCountElement(1);
		override val reciprocal: AbelianSymbolCount get() = when (this) {
			OmitSymbolCount -> OmitSymbolCount
			else -> ArbitrarySymbolCountDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitrarySymbolCountDim(override val exponent: Int): AbelianSymbolCount {
		override val reciprocal: AbelianSymbolCount get() = ArbitrarySymbolCountDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianTime: AbelianElement {
	companion object {
		const val SYMBOL = "T"
	}
	val decrementation: AbelianTime
	val reciprocal: AbelianTime
	override val symbol get() = Time
	enum class TimeDimPowers(override val exponent: Int): AbelianTime {
		OmitTime(0),
		TimeElement(1), TimeSquared(2), TimeCubed(3), TimeHypercubed(4),
		ReciprocalTime(-1), ReciprocalTimeSquared(-2), ReciprocalTimeCubed(-3);
		override val decrementation: AbelianTime get() = when (this) {
			TimeHypercubed -> TimeCubed
			TimeCubed -> TimeSquared
			TimeSquared -> TimeElement
			TimeElement -> OmitTime
			OmitTime -> ReciprocalTime
			ReciprocalTime -> ReciprocalTimeSquared
			ReciprocalTimeSquared -> ReciprocalTimeCubed
			else -> ArbitraryTimeDim(exponent - 1)
		}
		override val reciprocal: AbelianTime get() = when (this) {
			TimeCubed -> ReciprocalTimeCubed
			TimeSquared -> ReciprocalTimeSquared
			TimeElement -> ReciprocalTime
			OmitTime -> OmitTime
			ReciprocalTime -> TimeElement
			ReciprocalTimeSquared -> TimeSquared
			ReciprocalTimeCubed -> TimeCubed
			else -> ArbitraryTimeDim(-exponent)
		}
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
	class ArbitraryTimeDim(override val exponent: Int): AbelianTime {
		override val decrementation: AbelianTime get() = ArbitraryTimeDim(exponent - 1)
		override val reciprocal: AbelianTime get() = ArbitraryTimeDim(-exponent)
		override fun toString() = abelianElementStrFormat(exponent, SYMBOL)
	}
}

/** @see License */
interface AbelianGroup<S: UnitSystem> {
	val dimSet: Set<AbelianElement>
	fun getCanonicalForm() = abelianGroupCanonicalFormat(dimSet)
}

/** @see License */
interface QuantityDimension<S: UnitSystem> {
	val group: AbelianGroup<S>
	fun reciprocal(): QuantityDimension<S>
	fun timeDerivative(): QuantityDimension<S>
}

/** @see License */
interface UnnamedDimension<D: QuantityDimension<S>, S: UnitSystem>: QuantityDimension<S> {
	fun tryMapToNamed(): D
}

/** @see License */
interface ReciprocalDim<S: UnitSystem>: QuantityDimension<S>

/** @see License */
data class ISQAbelianGroup(val length: AbelianLength, val mass: AbelianMass, val time: AbelianTime,
		val current: AbelianElectricCurrent = OmitElectricCurrent,
		val temperature: AbelianAbsoluteTemperature = OmitAbsoluteTemperature,
		val amount: AbelianAmountOfSubstance = OmitAmountOfSubstance,
		val luminousIntensity: AbelianLuminousIntensity = OmitLuminousIntensity): AbelianGroup<SI> {
	override val dimSet get() = setOf(length, mass, time, current, temperature, amount, luminousIntensity)
	override fun toString() = abelianGroupStrFormat(dimSet)
}
/** @see License */
interface ISQQuantityDimension: QuantityDimension<SI> {
	override val group: ISQAbelianGroup
	fun isEquivalentTo(d: ISQQuantityDimension) = group == d.group
	override fun reciprocal() = ISQUnnamedDimension(group).reciprocal().tryMapToNamed()
	override fun timeDerivative() = ISQUnnamedDimension(group).timeDerivative().tryMapToNamed()
}
/** @see License */
class ISQReciprocalDim<D: ISQQuantityDimension> internal constructor(private val q: ISQQuantityDimension):
		ReciprocalDim<SI>, ISQQuantityDimension {
	override val group get() = { g: ISQAbelianGroup -> ISQAbelianGroup(g.length.reciprocal, g.mass.reciprocal,
		g.time.reciprocal, g.current.reciprocal, g.temperature.reciprocal, g.amount.reciprocal,
		g.luminousIntensity.reciprocal) }.invoke(q.group)
}

/** @see License */
data class NeoISQAbelianGroup(val length: AbelianLength, val mass: AbelianMass, val time: AbelianTime,
		val current: AbelianElectricCurrent = OmitElectricCurrent, val angle: AbelianAngle = OmitAngle,
		val temperature: AbelianAbsoluteTemperature = OmitAbsoluteTemperature,
		val amount: AbelianAmountOfSubstance = OmitAmountOfSubstance,
		val financialValue: AbelianFinancialValue = OmitFinancialValue): AbelianGroup<NeoSI> {
	override val dimSet get() = setOf(length, mass, time, current, angle, temperature, amount, financialValue)
	override fun toString() = abelianGroupStrFormat(dimSet)
}
/** @see License */
interface NeoISQQuantityDimension: QuantityDimension<NeoSI> {
	override val group: NeoISQAbelianGroup
	/**
	 * Warning: Due to the BIPM declaring angles (and powers of angles) to be of dimension one, and also because the
	 * proposed Neo-ISQ includes and financial value as a base dimension over luminous intensity, this method may return
	 * a dimension that is theoretically equivalent only when these factors are set to unity.
	 */
	fun getISQEquivalent() = NeoISQUnnamedDimension(group).getISQEquivalent().tryMapToNamed()
	fun isEquivalentTo(d: NeoISQQuantityDimension) = group == d.group
	override fun reciprocal() = NeoISQUnnamedDimension(group).reciprocal().tryMapToNamed()
	override fun timeDerivative() = NeoISQUnnamedDimension(group).timeDerivative().tryMapToNamed()
}
/** @see License */
class NeoISQReciprocalDim<D: NeoISQQuantityDimension> internal constructor(private val q: NeoISQQuantityDimension):
		ReciprocalDim<NeoSI>, NeoISQQuantityDimension {
	override val group get() = { g: NeoISQAbelianGroup -> NeoISQAbelianGroup(g.length.reciprocal, g.mass.reciprocal,
		g.time.reciprocal, g.current.reciprocal, g.angle.reciprocal, g.temperature.reciprocal, g.amount.reciprocal,
		g.financialValue.reciprocal) }.invoke(q.group)
}

/** @see License */
data class OQuSAbelianGroup(val time: AbelianTime, val length: AbelianLength, val mass: AbelianMass,
		val charge: AbelianElectricCharge = OmitElectricCharge, val angle: AbelianAngle = OmitAngle,
		val temperature: AbelianAbsoluteTemperature = OmitAbsoluteTemperature,
		val amount: AbelianAmountOfSubstance = OmitAmountOfSubstance,
		val symbolCount: AbelianSymbolCount = OmitSymbolCount,
		val financialValue: AbelianFinancialValue = OmitFinancialValue): AbelianGroup<OPUS> {
	override val dimSet get() = setOf(time, length, mass, charge, angle, temperature, amount, symbolCount,
		financialValue)
	override fun toString() = abelianGroupStrFormat(dimSet)
}
/** @see License */
interface OQuSQuantityDimension: QuantityDimension<OPUS> {
	override val group: OQuSAbelianGroup
	/**
	 * Warning: Due to OQuS including symbol count as a base dimension, this method may return a dimension that is
	 * theoretically equivalent only when symbol count is set to unity.
	 */
	fun getNeoISQEquivalent() = OQuSUnnamedDimension(group).getNeoISQEquivalent().tryMapToNamed()
	fun isEquivalentTo(d: OQuSQuantityDimension) = group == d.group
	override fun reciprocal() = OQuSUnnamedDimension(group).reciprocal().tryMapToNamed()
	override fun timeDerivative() = OQuSUnnamedDimension(group).timeDerivative().tryMapToNamed()
}
/** @see License */
class OQuSReciprocalDim<D: OQuSQuantityDimension> internal constructor(private val q: OQuSQuantityDimension):
		ReciprocalDim<OPUS>, OQuSQuantityDimension {
	override val group get() = { g: OQuSAbelianGroup -> OQuSAbelianGroup(g.time.reciprocal, g.length.reciprocal,
		g.mass.reciprocal, g.charge.reciprocal, g.angle.reciprocal, g.temperature.reciprocal, g.amount.reciprocal,
		g.symbolCount.reciprocal, g.financialValue.reciprocal) }.invoke(q.group)
}
