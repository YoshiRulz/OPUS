package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ElectricCurrentDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.*

/** @see License */
enum class NeoISQChargeDensity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthCubed, OmitMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQChargeMassRatio: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, ReciprocalMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQElectricalCapacitance: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthSquared, ReciprocalMass, TimeHypercubed,
		ElectricCurrentSquared)
	override fun timeDerivative() = NeoISQElectricalConductance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQElectricalConductance: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthSquared, ReciprocalMass, TimeCubed, ElectricCurrentSquared)
	override fun reciprocal() = NeoISQElectricalResistance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQElectricalInductance: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		ReciprocalElectricCurrentSquared)
	override fun timeDerivative() = NeoISQElectricalResistance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQElectricalResistance: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeCubed,
		ReciprocalElectricCurrentSquared)
	override fun reciprocal() = NeoISQElectricalConductance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQElectricCharge: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQElectricFieldStrength: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeCubed, ReciprocalElectricCurrent)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQElectrostaticPotential: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeCubed, ReciprocalElectricCurrent)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMagneticFieldHStrength: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLength, OmitMass, OmitTime, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMagneticFlux: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		ReciprocalElectricCurrent)
	override fun timeDerivative() = NeoISQElectrostaticPotential.QDIM
	override fun toString() = group.toString()
}
/**
 * Note: magnetic flux density is magnetic flux per unit area, not volume.
 * @see License
 */
enum class NeoISQMagneticFluxDensity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, MassElement, ReciprocalTimeSquared, ReciprocalElectricCurrent)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMagneticMoment: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, OmitMass, OmitTime, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQPermeability: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeSquared,
		ReciprocalElectricCurrentSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQPermittivity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthCubed, ReciprocalMass, TimeHypercubed,
		ElectricCurrentSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQSurfaceChargeDensity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthSquared, OmitMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
