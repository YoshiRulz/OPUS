package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.TemperatureDimPowers.ReciprocalAbsoluteTemperature
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.AmountOfSubstanceElement
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.ReciprocalAmountOfSubstance
import dev.yoshirulz.opus.qdims.AbelianAngle.AngleDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ElectricCurrentDimPowers.OmitElectricCurrent
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.*

/** @see License */
enum class NeoISQAcceleration: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, OmitMass, ReciprocalTimeSquared)
	override fun timeDerivative() = NeoISQJerk.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQAction: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTime)
	override fun timeDerivative() = NeoISQEnergy.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQAngularAcceleration: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, ReciprocalTimeSquared, angle = AngleElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQAngularJerk: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, ReciprocalTimeCubed, angle = AngleElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQAngularMomentum: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, MassElement, ReciprocalTime, angle = AngleElement)
	override fun timeDerivative() = NeoISQTorque.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQAngularSpeed: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, ReciprocalTime, angle = AngleElement)
	override fun timeDerivative() = NeoISQAngularAcceleration.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQArea: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, OmitMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQAreaDensity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthSquared, MassElement, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQCatalyticActivity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, ReciprocalTime, amount = AmountOfSubstanceElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQDensity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthCubed, MassElement, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQDynamicViscosity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLength, MassElement, ReciprocalTime)
	override fun timeDerivative() = NeoISQJerk.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQEnergy: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared)
	override fun reciprocal() = NeoISQPower.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQEnergyDensity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQPressure.QDIM.group
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQEntropy: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQForce: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeSquared)
	override fun timeDerivative() = NeoISQYank.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQFrequency: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, ReciprocalTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQHeatCapacity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQJerk: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, OmitMass, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQKinematicViscosity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, OmitMass, ReciprocalTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMolarConcentration: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLengthCubed, OmitMass, OmitTime,
		amount = AmountOfSubstanceElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMolarEnergy: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMolarHeatCapacity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared, OmitElectricCurrent,
		OmitAngle, ReciprocalAbsoluteTemperature, ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMolarMass: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, MassElement, OmitTime, amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMolarVolume: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthCubed, OmitMass, OmitTime, amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQMomentum: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, MassElement, ReciprocalTime)
	override fun timeDerivative() = NeoISQForce.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQPower: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQPressure: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLength, MassElement, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQRotatum: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, MassElement, ReciprocalTimeCubed, angle = AngleElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQSolidAngle: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, OmitMass, OmitTime, angle = AngleSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQSpecificEnergy: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, OmitMass, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQSpecificHeatCapacity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthSquared, OmitMass, ReciprocalTimeSquared,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQSpecificVolume: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthCubed, ReciprocalMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQSpeed: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, OmitMass, ReciprocalTime)
	override fun timeDerivative() = NeoISQAcceleration.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQSurfaceTension: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, MassElement, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQThermalConductivity: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeCubed,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQTorque: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(OmitLength, MassElement, ReciprocalTimeSquared, angle = AngleElement)
	override fun reciprocal() = NeoISQRotatum.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQVolume: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthCubed, OmitMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQVolumetricFlowRate: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthCubed, OmitMass, ReciprocalTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQWavenumber: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(ReciprocalLength, OmitMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class NeoISQYank: NeoISQQuantityDimension { QDIM;
	override val group = NeoISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
