package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.units.UnitSystem.SI

/** @see License */
class ISQUnnamedDimension internal constructor(override val group: ISQAbelianGroup):
		UnnamedDimension<ISQQuantityDimension, SI>, ISQQuantityDimension {
	companion object {
		val QDIM_STRMAP: Map<String, ISQQuantityDimension> = mapOf(
			"1" to ISQDimensionOne.QDIM,
			"I" to ISQElectricCurrent.QDIM,
			"I·L^-1" to ISQMagneticFieldHStrength.QDIM,
			"J" to ISQLuminousIntensity.QDIM,
			"L^-1" to ISQWavenumber.QDIM,
			"L^2·I" to ISQMagneticMoment.QDIM,
			"L^2" to ISQArea.QDIM,
			"L^2·M·T^-1" to ISQAction.QDIM,
			"L^2·M·T^-2·I^-1" to ISQMagneticFlux.QDIM,
			"L^2·M·T^-2·I^-2" to ISQElectricalInductance.QDIM,
			"L^2·M·T^-2·N^-1" to ISQMolarEnergy.QDIM,
			"L^2·M·T^-2" to ISQEnergy.QDIM,
			"L^2·M·T^-2·Θ^-1" to ISQEntropy.QDIM,
			"L^2·M·T^-3·I^-1" to ISQElectrostaticPotential.QDIM,
			"L^2·M·T^-3·I^-2" to ISQElectricalResistance.QDIM,
			"L^2·M·T^-3" to ISQPower.QDIM,
			"L^2·T^-2" to ISQAbsorbedDose.QDIM,
			"L^3" to ISQVolume.QDIM,
			"L^3·T^-1" to ISQVolumetricFlowRate.QDIM,
			"L" to ISQLength.QDIM,
			"L·M·T^-1" to ISQMomentum.QDIM,
			"L·M·T^-2·I^-2" to ISQPermeability.QDIM,
			"L·M·T^-2" to ISQForce.QDIM,
			"L·M·T^-3·I^-1" to ISQElectricFieldStrength.QDIM,
			"L·M·T^-3" to ISQYank.QDIM,
			"L·T^-1" to ISQSpeed.QDIM,
			"L·T^-2" to ISQAcceleration.QDIM,
			"L·T^-3" to ISQJerk.QDIM,
			"M" to ISQMass.QDIM,
			"M·L^-1·T^-2" to ISQPressure.QDIM,
			"M·L^-2" to ISQAreaDensity.QDIM,
			"M·L^-3" to ISQDensity.QDIM,
			"M·N^-1" to ISQMolarMass.QDIM,
			"M·T^-2·I^-1" to ISQMagneticFluxDensity.QDIM,
			"N" to ISQAmountOfSubstance.QDIM,
			"N·L^-3" to ISQMolarConcentration.QDIM,
			"N·T^-1" to ISQCatalyticActivity.QDIM,
			"T^-1" to ISQFrequency.QDIM,
			"T^-2" to ISQAngularAcceleration.QDIM,
			"T^3·I^2·L^-2·M^-1" to ISQElectricalConductance.QDIM,
			"T^4·I^2·L^-2·M^-1" to ISQElectricalCapacitance.QDIM,
			"T^4·I^2·L^-3·M^-1" to ISQPermittivity.QDIM,
			"T·I" to ISQElectricCharge.QDIM,
			"T·I·L^-2" to ISQSurfaceChargeDensity.QDIM,
			"T·I·L^-3" to ISQChargeDensity.QDIM,
			"T·I·M^-1" to ISQChargeMassRatio.QDIM,
			"T" to ISQTime.QDIM,
			"Θ" to ISQAbsoluteTemperature.QDIM
		)
	}

	constructor(length: AbelianLength, mass: AbelianMass, time: AbelianTime, electricCurrent: AbelianElectricCurrent,
			absoluteTemperature: AbelianAbsoluteTemperature, amountOfSubstance: AbelianAmountOfSubstance,
			luminousIntensity: AbelianLuminousIntensity):
		this(ISQAbelianGroup(length, mass, time, electricCurrent, absoluteTemperature, amountOfSubstance,
			luminousIntensity))

	/**
	 * Warning: Due to the BIPM declaring angles (and powers of angles) to be of dimension one,
	 * this method may return correct but unexpected results.
	 */
	override fun tryMapToNamed() = QDIM_STRMAP[group.getCanonicalForm()] ?: this

	override fun reciprocal(): ISQUnnamedDimension {
		return ISQUnnamedDimension(group.length.reciprocal, group.mass.reciprocal, group.time.reciprocal,
			group.current.reciprocal, group.temperature.reciprocal, group.amount.reciprocal,
			group.luminousIntensity.reciprocal)
	}

	override fun timeDerivative(): ISQUnnamedDimension {
		return ISQUnnamedDimension(group.length, group.mass, group.time.decrementation, group.current,
			group.temperature, group.amount, group.luminousIntensity)
	}
}
