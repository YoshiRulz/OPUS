package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianLuminousIntensity.LuminousIntensityDimPowers.OmitLuminousIntensity
import dev.yoshirulz.opus.units.UnitSystem.NeoSI

/** @see License */
class NeoISQUnnamedDimension internal constructor(override val group: NeoISQAbelianGroup):
		UnnamedDimension<NeoISQQuantityDimension, NeoSI>, NeoISQQuantityDimension {
	companion object {
		val QDIM_STRMAP: Map<String, NeoISQQuantityDimension> = mapOf(
			"1" to NeoISQDimensionOne.QDIM,
			"I·L^-1" to NeoISQMagneticFieldHStrength.QDIM,
			"I" to NeoISQElectricCurrent.QDIM,
			"L^-1" to NeoISQWavenumber.QDIM,
			"L^2·I" to NeoISQMagneticMoment.QDIM,
			"L^2·M·T^-1" to NeoISQAction.QDIM,
			"L^2·M·T^-2·I^-1" to NeoISQMagneticFlux.QDIM,
			"L^2·M·T^-2·I^-2" to NeoISQElectricalInductance.QDIM,
			"L^2·M·T^-2·N^-1" to NeoISQMolarEnergy.QDIM,
			"L^2·M·T^-2" to NeoISQEnergy.QDIM,
			"L^2·M·T^-2·Θ^-1" to NeoISQEntropy.QDIM,
			"L^2·M·T^-3·I^-1" to NeoISQElectrostaticPotential.QDIM,
			"L^2·M·T^-3·I^-2" to NeoISQElectricalResistance.QDIM,
			"L^2·M·T^-3" to NeoISQPower.QDIM,
			"L^2" to NeoISQArea.QDIM,
			"L^3·T^-1" to NeoISQVolumetricFlowRate.QDIM,
			"L^3" to NeoISQVolume.QDIM,
			"L·M·T^-1" to NeoISQMomentum.QDIM,
			"L·M·T^-2·I^-2" to NeoISQPermeability.QDIM,
			"L·M·T^-2" to NeoISQForce.QDIM,
			"L·M·T^-3·I^-1" to NeoISQElectricFieldStrength.QDIM,
			"L·M·T^-3" to NeoISQYank.QDIM,
			"L·T^-1" to NeoISQSpeed.QDIM,
			"L·T^-2" to NeoISQAcceleration.QDIM,
			"L·T^-3" to NeoISQJerk.QDIM,
			"L" to NeoISQLength.QDIM,
			"M·L^-1·T^-2" to NeoISQPressure.QDIM,
			"M·L^-2" to NeoISQAreaDensity.QDIM,
			"M·L^-3" to NeoISQDensity.QDIM,
			"M·N^-1" to NeoISQMolarMass.QDIM,
			"M·R·T^-1" to NeoISQAngularMomentum.QDIM,
			"M·R·T^-2" to NeoISQTorque.QDIM,
			"M·R·T^-3" to NeoISQRotatum.QDIM,
			"M·T^-2·I^-1" to NeoISQMagneticFluxDensity.QDIM,
			"M·T^-2·L^-1" to NeoISQPressure.QDIM,
			"M" to NeoISQMass.QDIM,
			"N·L^-3" to NeoISQMolarConcentration.QDIM,
			"N·T^-1" to NeoISQCatalyticActivity.QDIM,
			"N" to NeoISQAmountOfSubstance.QDIM,
			"R^2" to NeoISQSolidAngle.QDIM,
			"R·T^-1" to NeoISQAngularSpeed.QDIM,
			"R·T^-2" to NeoISQAngularAcceleration.QDIM,
			"R·T^-3" to NeoISQAngularJerk.QDIM,
			"R" to NeoISQAngle.QDIM,
			"T^-1" to NeoISQFrequency.QDIM,
			"T^-2" to NeoISQAngularAcceleration.QDIM,
			"T^3·I^2·L^-2·M^-1" to NeoISQElectricalConductance.QDIM,
			"T^4·I^2·L^-2·M^-1" to NeoISQElectricalCapacitance.QDIM,
			"T^4·I^2·L^-3·M^-1" to NeoISQPermittivity.QDIM,
			"T·I·L^-2" to NeoISQSurfaceChargeDensity.QDIM,
			"T·I·L^-3" to NeoISQChargeDensity.QDIM,
			"T·I·M^-1" to NeoISQChargeMassRatio.QDIM,
			"T·I" to NeoISQElectricCharge.QDIM,
			"T" to NeoISQTime.QDIM,
			"V" to NeoISQFinancialValue.QDIM,
			"Θ" to NeoISQAbsoluteTemperature.QDIM
		)
	}

	constructor(length: AbelianLength, mass: AbelianMass, time: AbelianTime, current: AbelianElectricCurrent,
			angle: AbelianAngle, temperature: AbelianAbsoluteTemperature, amount: AbelianAmountOfSubstance,
			financialValue: AbelianFinancialValue):
		this(NeoISQAbelianGroup(length, mass, time, current, angle, temperature, amount, financialValue))

	override fun tryMapToNamed() = QDIM_STRMAP[group.getCanonicalForm()] ?: this

	override fun getISQEquivalent(): ISQUnnamedDimension {
		return ISQUnnamedDimension(group.length, group.mass, group.time, group.current, group.temperature, group.amount,
			OmitLuminousIntensity)
	}

	override fun reciprocal(): NeoISQUnnamedDimension {
		return NeoISQUnnamedDimension(group.length.reciprocal, group.mass.reciprocal, group.time.reciprocal,
			group.current.reciprocal, group.angle.reciprocal, group.temperature.reciprocal, group.amount.reciprocal,
			group.financialValue.reciprocal)
	}

	override fun timeDerivative(): NeoISQUnnamedDimension {
		return NeoISQUnnamedDimension(group.length, group.mass, group.time.decrementation, group.current, group.angle,
			group.temperature, group.amount, group.financialValue)
	}
}
