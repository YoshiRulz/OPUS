package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.TemperatureDimPowers.ReciprocalAbsoluteTemperature
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.AmountOfSubstanceElement
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.ReciprocalAmountOfSubstance
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ElectricCurrentDimPowers.OmitElectricCurrent
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianLuminousIntensity.LuminousIntensityDimPowers.LuminousIntensityElement
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.*

/** @see License */
enum class ISQAcceleration: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, OmitMass, ReciprocalTimeSquared)
	override fun timeDerivative() = ISQJerk.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQAction: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTime)
	override fun timeDerivative() = ISQEnergy.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQAngle: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime)
	override fun timeDerivative() = ISQAngularSpeed.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQAngularAcceleration: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQAngularJerk: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQAngularMomentum: ISQQuantityDimension { QDIM;
	override val group = ISQAction.QDIM.group
	override fun reciprocal() = ISQAction.QDIM.reciprocal()
	override fun timeDerivative() = ISQAction.QDIM.timeDerivative()
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQAngularSpeed: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, ReciprocalTime)
	override fun timeDerivative() = ISQAngularAcceleration.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQArea: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, OmitMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQAreaDensity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthSquared, MassElement, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQCatalyticActivity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, ReciprocalTime, amount = AmountOfSubstanceElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQDensity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthCubed, MassElement, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQDynamicViscosity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLength, MassElement, ReciprocalTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQEnergy: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared)
	override fun reciprocal() = ISQPower.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQEnergyDensity: ISQQuantityDimension { QDIM;
	override val group = ISQPressure.QDIM.group
	override fun reciprocal() = ISQPressure.QDIM.reciprocal()
	override fun timeDerivative() = ISQPressure.QDIM.timeDerivative()
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQEntropy: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQFinancialValue: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQForce: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeSquared)
	override fun timeDerivative() = ISQYank.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQFrequency: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, ReciprocalTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQHeatCapacity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQIlluminance: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthSquared, OmitMass, OmitTime,
		luminousIntensity = LuminousIntensityElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQJerk: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, OmitMass, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQKinematicViscosity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, OmitMass, ReciprocalTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQLuminousFlux: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime,
		luminousIntensity = LuminousIntensityElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMolarConcentration: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthCubed, OmitMass, OmitTime, amount = AmountOfSubstanceElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMolarEnergy: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMolarHeatCapacity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared, OmitElectricCurrent,
		ReciprocalAbsoluteTemperature, ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMolarMass: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, MassElement, OmitTime, amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMolarVolume: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthCubed, OmitMass, OmitTime, amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMomentum: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, MassElement, ReciprocalTime)
	override fun timeDerivative() = ISQForce.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQPower: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQPressure: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLength, MassElement, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQRotatum: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQSolidAngle: ISQQuantityDimension { QDIM;
	override val group = ISQAngle.QDIM.group
	override fun reciprocal() = ISQAngle.QDIM.reciprocal()
	override fun timeDerivative() = ISQAngle.QDIM.timeDerivative()
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQSpecificEnergy: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, OmitMass, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQSpecificHeatCapacity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, OmitMass, ReciprocalTimeSquared, OmitElectricCurrent,
		ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQSpecificVolume: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthCubed, ReciprocalMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQSpeed: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, OmitMass, ReciprocalTime)
	override fun timeDerivative() = ISQAcceleration.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQSurfaceTension: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, MassElement, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQThermalConductivity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeCubed,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQTorque: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared)
	override fun reciprocal() = ISQRotatum.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQVolume: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthCubed, OmitMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQVolumetricFlowRate: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthCubed, OmitMass, ReciprocalTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQWavenumber: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLength, OmitMass, OmitTime)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQYank: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeCubed)
	override fun toString() = group.toString()
}
