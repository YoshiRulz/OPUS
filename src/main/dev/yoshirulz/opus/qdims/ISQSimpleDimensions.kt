package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.TemperatureDimPowers.AbsoluteTemperatureElement
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.AmountOfSubstanceElement
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ElectricCurrentDimPowers.ElectricCurrentElement
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.LengthElement
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.OmitLength
import dev.yoshirulz.opus.qdims.AbelianLuminousIntensity.LuminousIntensityDimPowers.LuminousIntensityElement
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.MassElement
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.OmitMass
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.OmitTime
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.TimeElement

/** @see License */
enum class ISQDimensionOne: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime)
	override fun toString() = "1"
}
/** @see License */
enum class ISQLength: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, OmitMass, OmitTime)
	override fun timeDerivative() = ISQSpeed.QDIM
	override fun toString() = "L"
}
/** @see License */
enum class ISQMass: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, MassElement, OmitTime)
	override fun toString() = "M"
}
/** @see License */
enum class ISQTime: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, TimeElement)
	override fun timeDerivative() = ISQDimensionOne.QDIM
	override fun toString() = "T"
}
/** @see License */
enum class ISQElectricCurrent: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime, ElectricCurrentElement)
	override fun toString() = "I"
}
/** @see License */
enum class ISQAbsoluteTemperature: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime, temperature = AbsoluteTemperatureElement)
	override fun toString() = "Θ"
}
/** @see License */
enum class ISQAmountOfSubstance: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime, amount = AmountOfSubstanceElement)
	override fun toString() = "N"
}
/** @see License */
enum class ISQLuminousIntensity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, OmitTime, luminousIntensity = LuminousIntensityElement)
	override fun toString() = "J"
}
