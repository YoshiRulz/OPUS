package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ElectricCurrentDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.*

/** @see License */
enum class ISQChargeDensity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthCubed, OmitMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQChargeMassRatio: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, ReciprocalMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQElectricalCapacitance: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthSquared, ReciprocalMass, TimeHypercubed,
		ElectricCurrentSquared)
	override fun timeDerivative() = ISQElectricalConductance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQElectricalConductance: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthSquared, ReciprocalMass, TimeCubed, ElectricCurrentSquared)
	override fun reciprocal() = ISQElectricalResistance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQElectricalInductance: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared,
		ReciprocalElectricCurrentSquared)
	override fun timeDerivative() = ISQElectricalResistance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQElectricalResistance: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeCubed,
		ReciprocalElectricCurrentSquared)
	override fun reciprocal() = ISQElectricalConductance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQElectricCharge: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, OmitMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQElectricFieldStrength: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeCubed, ReciprocalElectricCurrent)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQElectrostaticPotential: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeCubed, ReciprocalElectricCurrent)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMagneticFieldHStrength: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLength, OmitMass, OmitTime, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMagneticFlux: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, MassElement, ReciprocalTimeSquared, ReciprocalElectricCurrent)
	override fun timeDerivative() = ISQElectrostaticPotential.QDIM
	override fun toString() = group.toString()
}
/**
 * Note: magnetic flux density is magnetic flux per unit area, not volume.
 * @see License
 */
enum class ISQMagneticFluxDensity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(OmitLength, MassElement, ReciprocalTimeSquared, ReciprocalElectricCurrent)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQMagneticMoment: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, OmitMass, OmitTime, ElectricCurrentElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQPermeability: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthElement, MassElement, ReciprocalTimeSquared,
		ReciprocalElectricCurrentSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQPermittivity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthCubed, ReciprocalMass, TimeHypercubed, ElectricCurrentSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQSurfaceChargeDensity: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(ReciprocalLengthSquared, OmitMass, TimeElement, ElectricCurrentElement)
	override fun toString() = group.toString()
}
