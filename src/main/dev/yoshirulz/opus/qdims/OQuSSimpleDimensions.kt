package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.TemperatureDimPowers.AbsoluteTemperatureElement
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.AmountOfSubstanceElement
import dev.yoshirulz.opus.qdims.AbelianAngle.AngleDimPowers.AngleElement
import dev.yoshirulz.opus.qdims.AbelianElectricCharge.ElectricChargeDimPowers.ElectricChargeElement
import dev.yoshirulz.opus.qdims.AbelianFinancialValue.FinancialValueDimPowers.FinancialValueElement
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.LengthElement
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.OmitLength
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.MassElement
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.OmitMass
import dev.yoshirulz.opus.qdims.AbelianSymbolCount.SymbolCountDimPowers.SymbolCountElement
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.OmitTime
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.TimeElement

/** @see License */
enum class DimensionOne: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass)
	override fun reciprocal() = this
	override fun toString() = "1"
}
/** @see License */
enum class Time: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(TimeElement, OmitLength, OmitMass)
	override fun timeDerivative() = DimensionOne.QDIM
	override fun toString() = "T"
}
/** @see License */
enum class Length: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, LengthElement, OmitMass)
	override fun timeDerivative() = Speed.QDIM
	override fun toString() = "L"
}
/** @see License */
enum class Mass: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, MassElement)
	override fun toString() = "M"
}
/** @see License */
enum class ElectricCharge: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass, ElectricChargeElement)
	override fun timeDerivative() = ElectricCurrent.QDIM
	override fun toString() = "Q"
}
/** @see License */
enum class Angle: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass, angle = AngleElement)
	override fun timeDerivative() = AngularSpeed.QDIM
	override fun toString() = "R"
}
/** @see License */
enum class AbsoluteTemperature: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass, temperature = AbsoluteTemperatureElement)
	override fun toString() = "Θ"
}
/** @see License */
enum class AmountOfSubstance: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass, amount = AmountOfSubstanceElement)
	override fun toString() = "N"
}
/** @see License */
enum class SymbolCount: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass, symbolCount = SymbolCountElement)
	override fun toString() = "Η"
}
/** @see License */
enum class FinancialValue: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass, financialValue = FinancialValueElement)
	override fun toString() = "V"
}
