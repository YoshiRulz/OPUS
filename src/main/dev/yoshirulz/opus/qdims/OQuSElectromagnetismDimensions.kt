package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianElectricCharge.ElectricChargeDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.*

/** @see License */
enum class ChargeDensity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, ReciprocalLengthCubed, OmitMass, ElectricChargeElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ChargeMassRatio: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, ReciprocalMass, ElectricChargeElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class DisplacementFieldStrength: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, ReciprocalLengthSquared, OmitMass, ElectricChargeElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ElectricalCapacitance: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(TimeSquared, ReciprocalLengthSquared, ReciprocalMass, ElectricChargeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ElectricalConductance: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(TimeElement, ReciprocalLengthSquared, ReciprocalMass, ElectricChargeSquared)
	override fun reciprocal() = ElectricalResistance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ElectricalInductance: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, LengthSquared, MassElement, ReciprocalElectricChargeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ElectricalResistance: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthSquared, MassElement, ReciprocalElectricChargeSquared)
	override fun reciprocal() = ElectricalConductance.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class ElectricCurrent: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, OmitLength, OmitMass, ElectricChargeElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ElectricFieldStrength: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthElement, MassElement, ReciprocalElectricCharge)
	override fun toString() = group.toString()
}
/** @see License */
enum class ElectrostaticPotential: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, MassElement, ReciprocalElectricCharge)
	override fun toString() = group.toString()
}
/** @see License */
enum class MagneticFieldHStrength: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, ReciprocalLength, OmitMass, ElectricChargeElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class MagneticFlux: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthSquared, MassElement, ReciprocalElectricCharge)
	override fun toString() = group.toString()
}
/**
 * Note: magnetic flux density is magnetic flux per unit area, not volume.
 * @see License
 */
enum class MagneticFluxDensity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, OmitLength, MassElement, ReciprocalElectricCharge)
	override fun toString() = group.toString()
}
/** @see License */
enum class MagneticMoment: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthSquared, OmitMass, ElectricChargeElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class Permeability: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, LengthElement, MassElement, ReciprocalElectricChargeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class Permittivity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(TimeSquared, ReciprocalLengthCubed, ReciprocalMass, ElectricChargeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class SurfaceChargeDensity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, ReciprocalLengthSquared, OmitMass, ElectricChargeElement)
	override fun toString() = group.toString()
}
