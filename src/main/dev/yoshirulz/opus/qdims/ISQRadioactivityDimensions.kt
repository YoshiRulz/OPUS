package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.LengthSquared
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.OmitMass
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.ReciprocalTimeSquared

/** @see License */
enum class ISQAbsorbedDose: ISQQuantityDimension { QDIM;
	override val group = ISQAbelianGroup(LengthSquared, OmitMass, ReciprocalTimeSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class ISQEquivalentDose: ISQQuantityDimension { QDIM;
	override val group = ISQAbsorbedDose.QDIM.group
	override fun toString() = ISQAbsorbedDose.QDIM.group.toString()
}
/** @see License */
enum class ISQRadioactivity: ISQQuantityDimension { QDIM;
	override val group = ISQFrequency.QDIM.group
	override fun toString() = ISQFrequency.QDIM.group.toString()
}
