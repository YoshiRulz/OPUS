package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ArbitraryCurrentDim
import dev.yoshirulz.opus.qdims.AbelianTime.ArbitraryTimeDim
import dev.yoshirulz.opus.units.UnitSystem.OPUS

/** @see License */
class OQuSUnnamedDimension internal constructor(override val group: OQuSAbelianGroup):
		UnnamedDimension<OQuSQuantityDimension, OPUS>, OQuSQuantityDimension {
	companion object {
		val QDIM_STRMAP: Map<String, OQuSQuantityDimension> = mapOf(
			"1" to DimensionOne.QDIM,
			"L^-1" to Wavenumber.QDIM,
			"L^2" to Area.QDIM,
			"L^2·M·Q^-2" to ElectricalInductance.QDIM,
			"L^2·M·T^-1" to Action.QDIM,
			"L^2·M·T^-1·Q^-1" to MagneticFlux.QDIM,
			"L^2·M·T^-1·Q^-2" to ElectricalResistance.QDIM,
			"L^2·M·T^-2" to Energy.QDIM,
			"L^2·M·T^-2·N^-1" to MolarEnergy.QDIM,
			"L^2·M·T^-2·Q^-1" to ElectrostaticPotential.QDIM,
			"L^2·M·T^-2·Θ^-1" to Entropy.QDIM,
			"L^2·M·T^-3" to Power.QDIM,
			"L^2·Q·T^-1" to MagneticMoment.QDIM,
			"L^3·T^-1" to VolumetricFlowRate.QDIM,
			"L^3" to Volume.QDIM,
			"L" to Length.QDIM,
			"L·M·Q^-2" to Permeability.QDIM,
			"L·M·T^-1" to Momentum.QDIM,
			"L·M·T^-2" to Force.QDIM,
			"L·M·T^-2·Q^-1" to ElectricFieldStrength.QDIM,
			"L·M·T^-3" to Yank.QDIM,
			"L·T^-1" to Speed.QDIM,
			"L·T^-2" to Acceleration.QDIM,
			"L·T^-3" to Jerk.QDIM,
			"M·L^-2" to AreaDensity.QDIM,
			"M·L^-3" to Density.QDIM,
			"M" to Mass.QDIM,
			"M·N^-1" to MolarMass.QDIM,
			"M·R·T^-1" to AngularMomentum.QDIM,
			"M·R·T^-2" to Torque.QDIM,
			"M·R·T^-3" to Rotatum.QDIM,
			"M·T^-1·Q^-1" to MagneticFluxDensity.QDIM,
			"M·T^-2·L^-1" to Pressure.QDIM,
			"N" to AmountOfSubstance.QDIM,
			"N·L^-3" to MolarConcentration.QDIM,
			"N·T^-1" to CatalyticActivity.QDIM,
			"Q" to ElectricCharge.QDIM,
			"Q·L^-2" to SurfaceChargeDensity.QDIM,
			"Q·L^-3" to ChargeDensity.QDIM,
			"Q·M^-1" to ChargeMassRatio.QDIM,
			"Q·T^-1" to ElectricCurrent.QDIM,
			"Q·T^-1·L^-1" to MagneticFieldHStrength.QDIM,
			"R^2" to SolidAngle.QDIM,
			"R" to Angle.QDIM,
			"R·T^-1" to AngularSpeed.QDIM,
			"R·T^-2" to AngularAcceleration.QDIM,
			"R·T^-3" to AngularJerk.QDIM,
			"T^-1" to Frequency.QDIM,
			"T^2·Q^2·L^-2·M^-1" to ElectricalCapacitance.QDIM,
			"T^2·Q^2·L^-3·M^-1" to Permittivity.QDIM,
			"T·Q^2·L^-2·M^-1" to ElectricalConductance.QDIM,
			"T" to Time.QDIM,
			"V" to FinancialValue.QDIM,
			"Η" to SymbolCount.QDIM,
			"Θ" to AbsoluteTemperature.QDIM
		)
	}
	constructor(time: AbelianTime, length: AbelianLength, mass: AbelianMass, electricCharge: AbelianElectricCharge,
			angle: AbelianAngle, absoluteTemperature: AbelianAbsoluteTemperature,
			amountOfSubstance: AbelianAmountOfSubstance, symbolCount: AbelianSymbolCount,
			financialValue: AbelianFinancialValue):
		this(OQuSAbelianGroup(time, length, mass, electricCharge, angle, absoluteTemperature, amountOfSubstance,
			symbolCount, financialValue))

	override fun tryMapToNamed() = QDIM_STRMAP[group.getCanonicalForm()] ?: this

	override fun getNeoISQEquivalent(): NeoISQUnnamedDimension {
		return NeoISQUnnamedDimension(group.length, group.mass,
			ArbitraryTimeDim(group.time.exponent + group.charge.exponent), ArbitraryCurrentDim(group.charge.exponent),
			group.angle, group.temperature, group.amount, group.financialValue)
	}

	override fun reciprocal(): OQuSUnnamedDimension {
		return OQuSUnnamedDimension(group.time.reciprocal, group.length.reciprocal, group.mass.reciprocal,
			group.charge.reciprocal, group.angle.reciprocal, group.temperature.reciprocal,
			group.amount.reciprocal, group.symbolCount.reciprocal, group.financialValue.reciprocal)
	}

	override fun timeDerivative(): OQuSUnnamedDimension {
		return OQuSUnnamedDimension(group.time.decrementation, group.length, group.mass, group.charge,
			group.angle, group.temperature, group.amount, group.symbolCount, group.financialValue)
	}
}
