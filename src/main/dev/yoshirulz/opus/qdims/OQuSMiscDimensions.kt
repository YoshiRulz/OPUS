package dev.yoshirulz.opus.qdims

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.TemperatureDimPowers.ReciprocalAbsoluteTemperature
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.AmountOfSubstanceElement
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.AmountOfSubstanceDimPowers.ReciprocalAmountOfSubstance
import dev.yoshirulz.opus.qdims.AbelianAngle.AngleDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianLength.LengthDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianMass.MassDimPowers.*
import dev.yoshirulz.opus.qdims.AbelianTime.TimeDimPowers.*

/** @see License */
enum class Acceleration: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthElement, OmitMass)
	override fun timeDerivative() = Jerk.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class Action: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthSquared, MassElement)
	override fun timeDerivative() = Energy.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class AngularAcceleration: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, OmitLength, OmitMass, angle = AngleElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class AngularJerk: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeCubed, OmitLength, OmitMass, angle = AngleElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class AngularMomentum: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, OmitLength, MassElement, angle = AngleElement)
	override fun timeDerivative() = Torque.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class AngularSpeed: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, OmitLength, OmitMass, angle = AngleElement)
	override fun timeDerivative() = AngularAcceleration.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class Area: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, LengthSquared, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class AreaDensity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, ReciprocalLengthSquared, MassElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class CatalyticActivity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, OmitLength, OmitMass, amount = AmountOfSubstanceElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class Density: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, ReciprocalLengthCubed, MassElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class DynamicViscosity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, ReciprocalLength, MassElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class Energy: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, MassElement)
	override fun timeDerivative() = Power.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class EnergyDensity: OQuSQuantityDimension { QDIM;
	override val group = Pressure.QDIM.group
	override fun toString() = group.toString()
}
/** @see License */
enum class Entropy: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, MassElement,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class Force: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthElement, MassElement)
	override fun timeDerivative() = Yank.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class Frequency: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, OmitLength, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class HeatCapacity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, MassElement,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class HydrogenPotential: OQuSQuantityDimension { QDIM;
	override val group = DimensionOne.QDIM.group
	override fun toString() = group.toString()
}
/** @see License */
enum class HydroxidePotential: OQuSQuantityDimension { QDIM;
	override val group = DimensionOne.QDIM.group
	override fun toString() = group.toString()
}
/** @see License */
enum class Jerk: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeCubed, LengthElement, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class KinematicViscosity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthSquared, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class MolarConcentration: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, ReciprocalLengthCubed, OmitMass, amount = AmountOfSubstanceElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class MolarEnergy: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, MassElement,
		amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class MolarHeatCapacity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, MassElement,
		temperature = ReciprocalAbsoluteTemperature, amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class MolarMass: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, MassElement, amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class MolarVolume: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, LengthCubed, OmitMass, amount = ReciprocalAmountOfSubstance)
	override fun toString() = group.toString()
}
/** @see License */
enum class Momentum: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthElement, MassElement)
	override fun timeDerivative() = Force.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class Power: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeCubed, LengthSquared, MassElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class Pressure: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, ReciprocalLength, MassElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class Rotatum: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeCubed, OmitLength, MassElement, angle = AngleElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class SolidAngle: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, OmitLength, OmitMass, angle = AngleSquared)
	override fun toString() = group.toString()
}
/** @see License */
enum class SpecificEnergy: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class SpecificHeatCapacity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, LengthSquared, OmitMass,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class SpecificVolume: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, LengthCubed, ReciprocalMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class Speed: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthElement, OmitMass)
	override fun timeDerivative() = Acceleration.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class SurfaceTension: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, OmitLength, MassElement)
	override fun toString() = group.toString()
}
/** @see License */
enum class ThermalConductivity: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeCubed, LengthElement, MassElement,
		temperature = ReciprocalAbsoluteTemperature)
	override fun toString() = group.toString()
}
/** @see License */
enum class Torque: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeSquared, OmitLength, MassElement, angle = AngleElement)
	override fun reciprocal() = Rotatum.QDIM
	override fun toString() = group.toString()
}
/** @see License */
enum class Volume: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, LengthCubed, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class VolumetricFlowRate: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTime, LengthCubed, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class Wavenumber: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(OmitTime, ReciprocalLength, OmitMass)
	override fun toString() = group.toString()
}
/** @see License */
enum class Yank: OQuSQuantityDimension { QDIM;
	override val group = OQuSAbelianGroup(ReciprocalTimeCubed, LengthElement, MassElement)
	override fun toString() = group.toString()
}
