package dev.yoshirulz.opus.quantity

import dev.yoshirulz.opus.LibraryGlobals
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.PureMaths.trimXToNDecimalPlaces
import dev.yoshirulz.opus.quantity.ApproximableProduct.Companion.approxProductOf
import java.math.BigDecimal

/** @see License */
interface Approximable {
	fun approxTo16bFloat(): Float
	fun approxTo32bFloat(): Double
	fun approxToNDecimalPlaces(n: Int): BigDecimal

	operator fun times(other: Approximable) = approxProductOf(this, other)
	operator fun div(other: Approximable): Approximable = approxProductOf(this) / other
	operator fun unaryMinus(): Approximable = ApproximableNegativeProduct(this)
}

/**
 * @see License
 * Value calculated upon approxTo*() method call.
 */
open class ApproximableProduct (vararg factors: Approximable): Approximable {
	companion object {
		fun approxProductOf(vararg factors: Approximable) = ApproximableProduct(*factors)
	}

	private val factors: Array<out Approximable> = factors.clone()

	fun overProductOf(vararg divisors: Approximable) = approxProductOf(this, ApproximableReciprocalProduct(divisors))

	override fun approxTo16bFloat() = factors.map { it.approxTo16bFloat() }.reduce { t, f -> t * f }
	override fun approxTo32bFloat() = factors.map { it.approxTo32bFloat() }.reduce { t, f -> t * f }
	override fun approxToNDecimalPlaces(n: Int) =
		trimXToNDecimalPlaces(factors.map { it.approxToNDecimalPlaces(n + 1) }.reduce { t, f -> t * f }, n)
	override operator fun div(other: Approximable) = overProductOf(other)

	override fun toString() = approxTo32bFloat().toString()
}

/** @see License */
class ApproximableNegativeProduct(private val value: Approximable): Approximable {
	override fun approxTo16bFloat() = -value.approxTo16bFloat()
	override fun approxTo32bFloat() = -value.approxTo32bFloat()
	override fun approxToNDecimalPlaces(n: Int): BigDecimal =
		value.approxToNDecimalPlaces(n).negate(LibraryGlobals.mathContext)
}

/** @see License */
class ApproximableReciprocalProduct internal constructor(divisor: Array<out Approximable>):
		ApproximableProduct(*divisor) {
	override fun approxTo16bFloat() = 1 / super.approxTo16bFloat()
	override fun approxTo32bFloat() = 1 / super.approxTo32bFloat()
}
