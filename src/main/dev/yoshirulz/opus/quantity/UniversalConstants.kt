@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package dev.yoshirulz.opus.quantity

import dev.yoshirulz.opus.LibraryGlobals
import dev.yoshirulz.opus.LibraryGlobals.ConstantSymbolName
import dev.yoshirulz.opus.LibraryGlobals.ConstantSymbolName.*
import dev.yoshirulz.opus.LibraryGlobals.i18nGetConstantSymbol
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.PureMaths.approxNDecimalPlacesOfCircleConst
import dev.yoshirulz.opus.PureMaths.approxNDecimalPlacesOfEulersNumber
import dev.yoshirulz.opus.PureMaths.trimXToNDecimalPlaces
import dev.yoshirulz.opus.qdims.ISQQuantityDimension
import dev.yoshirulz.opus.qdims.OQuSQuantityDimension
import dev.yoshirulz.opus.qdims.QuantityDimension
import dev.yoshirulz.opus.quantity.ApproximableDimOneConstMultiple.Companion.RawConsts.*
import dev.yoshirulz.opus.quantity.IntegerCalculable.Companion.ONE
import dev.yoshirulz.opus.quantity.IntegerCalculable.Companion.TWO
import dev.yoshirulz.opus.units.ActionUnitNameClass.ActionUnitName
import dev.yoshirulz.opus.units.AmountOfSubstanceUnitNameClass.AmountOfSubstanceUnitNames
import dev.yoshirulz.opus.units.CharjonClass.Charjon
import dev.yoshirulz.opus.units.ElectricalResistanceUnitNameClass.ElectricalResistanceUnitName
import dev.yoshirulz.opus.units.EntropyUnitNameClass.EntropyUnitNames
import dev.yoshirulz.opus.units.MolarHeatCapacityUnitNameClass.MolarHeatCapacityUnitNames
import dev.yoshirulz.opus.units.OPUSUnitlessClass
import dev.yoshirulz.opus.units.PermeabilityUnitNameClass.PermeabilityUnitName
import dev.yoshirulz.opus.units.PermittivityUnitNameClass.PermittivityUnitName
import dev.yoshirulz.opus.units.RevolutionClass.Revolution
import dev.yoshirulz.opus.units.SICoulombClass.Coulombs
import dev.yoshirulz.opus.units.SIFaradPerMetreClass.FaradsPerMetre
import dev.yoshirulz.opus.units.SIHenryPerMetreClass.HenriesPerMetre
import dev.yoshirulz.opus.units.SIJoulePerKelvinEntropyClass.JoulesPerKelvin
import dev.yoshirulz.opus.units.SIJoulePerKilogramClass.JoulesPerKilogram
import dev.yoshirulz.opus.units.SIJoulePerMoleKelvinClass.JoulesPerMoleKelvin
import dev.yoshirulz.opus.units.SIJouleSecondActionClass.JouleSeconds
import dev.yoshirulz.opus.units.SIKilogramClass.Kilograms
import dev.yoshirulz.opus.units.SIMetrePerSecondClass.MetresPerSecond
import dev.yoshirulz.opus.units.SIMetreSquaredClass.MetresSquared
import dev.yoshirulz.opus.units.SIMoleClass.Moles
import dev.yoshirulz.opus.units.SINewtonClass.Newtons
import dev.yoshirulz.opus.units.SIOhmClass.Ohms
import dev.yoshirulz.opus.units.SIUnitAmalgam
import dev.yoshirulz.opus.units.SIUnitlessClass
import dev.yoshirulz.opus.units.SpecificEnergyUnitNameClass.SpecificEnergyUnitName
import dev.yoshirulz.opus.units.SpeedUnitNameClass.SpeedUnitName
import dev.yoshirulz.opus.units.UnitSystem
import dev.yoshirulz.opus.units.UnitSystem.OPUS
import dev.yoshirulz.opus.units.UnitSystem.SI
import java.math.BigDecimal

val AVOGADRO_CONSTANT_VALUE_OPUS = DecimalCalculable(0.0) //TODO
val AVOGADRO_CONSTANT_VALUE_SI = DecimalCalculable(6.02214076E23)
val BOLTZMANN_CONSTANT_VALUE_OPUS = DecimalCalculable(0.0) //TODO
val BOLTZMANN_CONSTANT_VALUE_SI = DecimalCalculable(1.380649E-23)
val ELEMENTARY_CHARGE_VALUE_SI = DecimalCalculable(-1.602176634E-19)
val FINE_STRUCTURE_CONSTANT_MEASUREMENT_SI = ArbitraryPrecisionCalculable(BigDecimal.ONE
	.divide(BigDecimal("137.035999173"), LibraryGlobals.roundingMode))
val GRAVITATIONAL_CONSTANT_VALUE_SI = DecimalCalculable(6.67408E-11)
val PLANCK_CONSTANT_VALUE_SI = DecimalCalculable(6.626069934E-34)
val SPEED_OF_LIGHT_VALUE_SI = IntegerCalculable(299792458)
val UNITY = ONE

/** @see License */
class ApproximableDimOneConstMultiple internal constructor(private val coefficient: Double, private val raw: RawConsts):
		Approximable {
	companion object {
		@Suppress("EnumEntryName")
		enum class RawConsts(internal val raw: Double) {
			Tau(2.0 * Math.PI), τ(2.0 * Math.PI),
			@Deprecated("") Pi(Math.PI), @Deprecated("") π(Math.PI),
			EulersNumber(Math.E), e(Math.E);
			operator fun times(d: Double) = ApproximableDimOneConstMultiple(d, this)
			val once = times(1.0)
		}
	}
	override fun approxTo16bFloat() = (coefficient * raw.raw).toFloat()
	override fun approxTo32bFloat() = coefficient * raw.raw
	@Suppress("DEPRECATION")
	override fun approxToNDecimalPlaces(n: Int) = when (raw) {
		Tau, τ -> trimXToNDecimalPlaces(approxNDecimalPlacesOfCircleConst(n), n)
		Pi, π -> trimXToNDecimalPlaces(approxNDecimalPlacesOfCircleConst(n, approxPi = true), n)
		EulersNumber, e -> trimXToNDecimalPlaces(approxNDecimalPlacesOfEulersNumber(n), n)
	}
	override fun toString() = "$coefficient * $raw"
}

/** @see License */
interface UniversalConstants<D: QuantityDimension<S>, S: UnitSystem> {
	val symbol: ConstantSymbolName
	val isFixed: Boolean
}
/** @see License */
open class ApproximableUniversalConstant<D: QuantityDimension<S>, S: UnitSystem>(q: ApproximableQuantity<D, S>,
			override val symbol: ConstantSymbolName, override val isFixed: Boolean):
		ApproximableQuantity<D, S>(q.value, q.units), UniversalConstants<D, S> {
	override fun toString() = i18nGetConstantSymbol(symbol)
}
/** @see License */
open class CalculableUniversalConstant<D: QuantityDimension<S>, S: UnitSystem>(q: Quantity<D, S>,
			override val symbol: ConstantSymbolName, override val isFixed: Boolean):
		Quantity<D, S>(q.exactValue, q.units), UniversalConstants<D, S> {
	override fun toString() = i18nGetConstantSymbol(symbol)
}

/** @see License */
class OPUSApproximableConstant<D: OQuSQuantityDimension> private constructor(q: ApproximableQuantity<D, OPUS>,
		s: ConstantSymbolName, b: Boolean = false): ApproximableUniversalConstant<D, OPUS>(q, s, b) {
	companion object {
		val PLANCK_CONSTANT = OPUSApproximableConstant(OPUSCalculableConstant.REDUCED_PLANCK_CONSTANT * τ.once,
			PlanckConstant)
		val FINE_STRUCTURE_CONSTANT = OPUSApproximableConstant(OPUSUnitlessClass.Unitless.qOf(
				OPUSCalculableConstant.IMPEDANCE_OF_FREE_SPACE.exactValue *
				OPUSCalculableConstant.ELECTRON_CHARGE_SQUARED.exactValue) / (TWO * PLANCK_CONSTANT.value),
			FineStructureConstant)
	}
}
/** @see License */
class OPUSCalculableConstant<D: OQuSQuantityDimension> private constructor(q: Quantity<D, OPUS>, s: ConstantSymbolName,
		b: Boolean = false): CalculableUniversalConstant<D, OPUS>(q, s, b) {
	companion object {
		// Unity
		val DOWN_TYPE_QUARK_CHARGE = OPUSCalculableConstant(Charjon.qOf(UNITY), DownTypeQuarkCharge, true)
		val FERMION_SPIN = OPUSCalculableConstant(ActionUnitName.qOf(UNITY), FermionSpin, true)
		val IMPEDANCE_OF_FREE_SPACE = OPUSCalculableConstant(ElectricalResistanceUnitName.qOf(UNITY),
			ImpedanceOfFreeSpace, true)
		val REVOLUTION = OPUSCalculableConstant(Revolution.qOf(UNITY), ConstantSymbolName.Revolution, true)
		val SPEED_OF_LIGHT_IN_FREE_SPACE = OPUSCalculableConstant(SpeedUnitName.qOf(UNITY), SpeedOfLightInFreeSpace, true)
		val PERMEABILITY_OF_FREE_SPACE = OPUSCalculableConstant(PermeabilityUnitName.qOf(
				IMPEDANCE_OF_FREE_SPACE.exactValue / SPEED_OF_LIGHT_IN_FREE_SPACE.exactValue),
			PermeabilityOfFreeSpace)
		val SPEED_OF_LIGHT_IN_FREE_SPACE_SQUARED = OPUSCalculableConstant(SpecificEnergyUnitName.qOf(
				SPEED_OF_LIGHT_IN_FREE_SPACE.exactValue * SPEED_OF_LIGHT_IN_FREE_SPACE.exactValue),
			SpeedOfLightInFreeSpaceSquared)
		val PERMITTIVITY_OF_FREE_SPACE = OPUSCalculableConstant(PermittivityUnitName.qOf(ONE / (
				PERMEABILITY_OF_FREE_SPACE.exactValue * SPEED_OF_LIGHT_IN_FREE_SPACE_SQUARED.exactValue)),
			PermittivityOfFreeSpace)

		// Misc.
		val AVOGADRO_CONSTANT = OPUSCalculableConstant(AmountOfSubstanceUnitNames.qOf(AVOGADRO_CONSTANT_VALUE_OPUS),
			AvogadroConstant)
		val BOLTZMANN_CONSTANT = OPUSCalculableConstant(EntropyUnitNames.qOf(BOLTZMANN_CONSTANT_VALUE_OPUS),
			BoltzmannConstant)
		val IDEAL_GAS_CONSTANT = OPUSCalculableConstant(MolarHeatCapacityUnitNames.qOf(
			AVOGADRO_CONSTANT.exactValue * BOLTZMANN_CONSTANT.exactValue), IdealGasConstant)
		val ELECTRON_CHARGE = OPUSCalculableConstant(DOWN_TYPE_QUARK_CHARGE * 3, ElectronChargePositive)
		val ELECTRON_CHARGE_SQUARED = OPUSCalculableConstant(SpecificEnergyUnitName.qOf(
			ELECTRON_CHARGE.exactValue * ELECTRON_CHARGE.exactValue), ElectronChargeSquaredPositive)
		val REDUCED_PLANCK_CONSTANT = OPUSCalculableConstant(FERMION_SPIN * TWO, ReducedPlanckConstant)
	}
}

/** @see License */
class SIApproximableConstant<D: ISQQuantityDimension> private constructor(q: ApproximableQuantity<D, SI>,
		s: ConstantSymbolName, b: Boolean = false): ApproximableUniversalConstant<D, SI>(q, s, b) {
	companion object {
		val FINE_STRUCTURE_CONSTANT = SIApproximableConstant(SIUnitlessClass.Unitless.qOf(
			FINE_STRUCTURE_CONSTANT_MEASUREMENT_SI), FineStructureConstant)
		val PERMEABILITY_OF_FREE_SPACE = SIApproximableConstant(HenriesPerMetre.qOf(TWO * PLANCK_CONSTANT_VALUE_SI *
				FINE_STRUCTURE_CONSTANT_MEASUREMENT_SI / (SICalculableConstant.SPEED_OF_LIGHT_IN_FREE_SPACE.value *
			SICalculableConstant.ELECTRON_CHARGE_SQUARED.value)), PermeabilityOfFreeSpace)
		val PERMITTIVITY_OF_FREE_SPACE = SIApproximableConstant(FaradsPerMetre.qOf(
				ONE / (SICalculableConstant.SPEED_OF_LIGHT_IN_FREE_SPACE_SQUARED.exactValue *
			PERMEABILITY_OF_FREE_SPACE.value)), PermittivityOfFreeSpace)
		val IMPEDANCE_OF_FREE_SPACE = SIApproximableConstant(Ohms.qOf(PERMEABILITY_OF_FREE_SPACE.value *
			SICalculableConstant.SPEED_OF_LIGHT_IN_FREE_SPACE.exactValue), ImpedanceOfFreeSpace)
		val REDUCED_PLANCK_CONSTANT = SIApproximableConstant(SICalculableConstant.PLANCK_CONSTANT / τ.once,
			ReducedPlanckConstant)
		val FERMION_SPIN = SIApproximableConstant(REDUCED_PLANCK_CONSTANT / 2, FermionSpin)
	}
}
/** @see License */
class SICalculableConstant<D: ISQQuantityDimension> private constructor(q: Quantity<D, SI>, s: ConstantSymbolName,
		b: Boolean = false): CalculableUniversalConstant<D, SI>(q, s, b) {
	companion object {
		val AVOGADRO_CONSTANT = SICalculableConstant(Moles.reciprocal.qOf(AVOGADRO_CONSTANT_VALUE_SI),
			AvogadroConstant, true)
		val BOLTZMANN_CONSTANT = SICalculableConstant(JoulesPerKelvin.qOf(BOLTZMANN_CONSTANT_VALUE_SI),
			BoltzmannConstant, true)
		val ELECTRON_CHARGE = SICalculableConstant(Coulombs.qOf(ELEMENTARY_CHARGE_VALUE_SI),
			ElectronChargeNegative, true)
		val ELECTRON_CHARGE_SQUARED = SICalculableConstant(JoulesPerKilogram.qOf(ELECTRON_CHARGE.exactValue *
			ELECTRON_CHARGE.exactValue), ElectronChargeSquaredNegative)
		val DOWN_TYPE_QUARK_CHARGE = SICalculableConstant(ELECTRON_CHARGE * 2 / 3, DownTypeQuarkCharge)
		val GRAVITATIONAL_CONSTANT = SICalculableConstant(SIUnitAmalgam(arrayOf(Newtons, MetresSquared,
				Kilograms.reciprocal, Kilograms.reciprocal)).qOf(GRAVITATIONAL_CONSTANT_VALUE_SI),
			GravitationalConstant)
		val IDEAL_GAS_CONSTANT = SICalculableConstant(JoulesPerMoleKelvin.qOf(AVOGADRO_CONSTANT.exactValue *
			BOLTZMANN_CONSTANT.exactValue), IdealGasConstant)
		val SPEED_OF_LIGHT_IN_FREE_SPACE = SICalculableConstant(MetresPerSecond.qOf(SPEED_OF_LIGHT_VALUE_SI),
			SpeedOfLightInFreeSpace, true)
		val SPEED_OF_LIGHT_IN_FREE_SPACE_SQUARED = SICalculableConstant(JoulesPerKilogram.qOf(
				SPEED_OF_LIGHT_IN_FREE_SPACE.exactValue * SPEED_OF_LIGHT_IN_FREE_SPACE.exactValue),
			SpeedOfLightInFreeSpaceSquared)
		val PLANCK_CONSTANT = SICalculableConstant(JouleSeconds.qOf(PLANCK_CONSTANT_VALUE_SI),
			PlanckConstant, true)
	}
}
