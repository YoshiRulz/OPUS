package dev.yoshirulz.opus.quantity

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.QuantityDimension
import dev.yoshirulz.opus.units.UnitInterface
import dev.yoshirulz.opus.units.UnitSystem
import java.math.BigDecimal

/** @see License */
open class ApproximableQuantity<D: QuantityDimension<S>, S: UnitSystem>(val value: Approximable,
		val units: UnitInterface<D, S>) {
	companion object {
		fun <D: QuantityDimension<S>, S: UnitSystem> sum(augend: ApproximableQuantity<D, S>,
				addend: ApproximableQuantity<D, S>) =
			ApproximableQuantity(DecimalCalculable(augend.value.approxTo32bFloat() + addend.value.approxTo32bFloat()),
				augend.units)
		fun <D: QuantityDimension<S>, S: UnitSystem> diff(minuend: ApproximableQuantity<D, S>,
				subtrahend: ApproximableQuantity<D, S>) =
			ApproximableQuantity(DecimalCalculable(minuend.value.approxTo32bFloat() -
				subtrahend.value.approxTo32bFloat()), minuend.units)
	}

	val floatApprox = value.approxTo16bFloat()
	val doubleApprox = value.approxTo32bFloat()
	fun nDecimalPlacesApprox(n: Int) = value.approxToNDecimalPlaces(n)

	operator fun plus(other: ApproximableQuantity<D, S>) = sum(this, other)
	operator fun minus(other: ApproximableQuantity<D, S>) = diff(this, other)
	open operator fun times(other: Approximable) = ApproximableQuantity(value * other, units)
	open operator fun times(other: BigDecimal) = times(ArbitraryPrecisionCalculable(other))
	open operator fun times(other: Double) = times(DecimalCalculable(other))
	open operator fun times(other: Long) = times(IntegerCalculable(other))
	open operator fun div(other: Approximable) = ApproximableQuantity(value / other, units)
	open operator fun div(other: BigDecimal) = div(ArbitraryPrecisionCalculable(other))
	open operator fun div(other: Double) = div(DecimalCalculable(other))
	open operator fun div(other: Long) = div(IntegerCalculable(other))

	override fun toString() = "$doubleApprox ${units.unitSymbol}"
}

/** @see License */
open class Quantity<D: QuantityDimension<S>, S: UnitSystem>(val exactValue: Calculable, units: UnitInterface<D, S>):
		ApproximableQuantity<D, S>(exactValue, units), Comparable<Quantity<D, S>> {
	constructor(v: BigDecimal, u: UnitInterface<D, S>): this(ArbitraryPrecisionCalculable(v), u)
	constructor(v: Double, u: UnitInterface<D, S>): this(DecimalCalculable(v), u)
	constructor(v: Long, u: UnitInterface<D, S>): this(IntegerCalculable(v), u)

	companion object {
		fun <D: QuantityDimension<S>, S: UnitSystem> sum(augend: Quantity<D, S>, addend: Quantity<D, S>) =
			Quantity(ArbitraryPrecisionCalculable(augend.exactValue.calcToMaxPrecision()
				.add(addend.exactValue.calcToMaxPrecision())), augend.units)
		fun <D: QuantityDimension<S>, S: UnitSystem> diff(minuend: Quantity<D, S>, subtrahend: Quantity<D, S>) =
			Quantity(ArbitraryPrecisionCalculable(minuend.exactValue.calcToMaxPrecision()
				.subtract(subtrahend.exactValue.calcToMaxPrecision())), minuend.units)
	}

	val floatValue = exactValue.calcTo16bFloat()
	val doubleValue = exactValue.calcTo32bFloat()
	val maxPrecisionValue = exactValue.calcToMaxPrecision()

	operator fun plus(other: Quantity<D, S>) = sum(this, other)
	operator fun minus(other: Quantity<D, S>) = diff(this, other)
	operator fun times(other: Calculable) = Quantity(exactValue * other, units)
	override operator fun times(other: BigDecimal) = times(ArbitraryPrecisionCalculable(other))
	override operator fun times(other: Double) = times(DecimalCalculable(other))
	override operator fun times(other: Long) = times(IntegerCalculable(other))
	operator fun div(other: Calculable) = Quantity(exactValue / other, units)
	override operator fun div(other: BigDecimal) = div(ArbitraryPrecisionCalculable(other))
	override operator fun div(other: Double) = div(DecimalCalculable(other))
	override operator fun div(other: Long) = div(IntegerCalculable(other))

	override fun compareTo(other: Quantity<D, S>) = exactValue.compareTo(other.exactValue)
	override fun toString() = "$doubleValue ${units.unitSymbol}"
}
