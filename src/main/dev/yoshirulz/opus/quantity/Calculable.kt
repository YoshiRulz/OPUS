package dev.yoshirulz.opus.quantity

import dev.yoshirulz.opus.LibraryGlobals
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.PureMaths.trimXToNDecimalPlaces
import dev.yoshirulz.opus.quantity.CalculableProduct.Companion.productOf
import java.math.BigDecimal

/** @see License */
interface Calculable: Approximable, Comparable<Calculable> {
	fun calcTo16bFloat(): Float
	fun calcTo32bFloat(): Double
	fun calcToMaxPrecision(): BigDecimal
	override fun approxTo16bFloat() = calcTo16bFloat()
	override fun approxTo32bFloat() = calcTo32bFloat()
	override fun approxToNDecimalPlaces(n: Int) = trimXToNDecimalPlaces(calcToMaxPrecision(), n)

	operator fun times(other: Calculable): Calculable = productOf(this, other)
	operator fun div(other: Calculable): Calculable = productOf(this) / other

	override fun compareTo(other: Calculable) = calcToMaxPrecision().compareTo(other.calcToMaxPrecision())
}

/** @see License */
open class ArbitraryPrecisionCalculable(protected val value: BigDecimal): Calculable {
	override fun calcTo16bFloat() = value.toFloat()
	override fun calcTo32bFloat() = value.toDouble()
	override fun calcToMaxPrecision() = value
	override operator fun unaryMinus() = ArbitraryPrecisionCalculable(value.negate(LibraryGlobals.mathContext))
	override fun toString() = value.toString()
}
/** @see License */
class DecimalCalculable(private val value: Double): Calculable {
	override fun calcTo16bFloat() = value.toFloat()
	override fun calcTo32bFloat() = value
	override fun calcToMaxPrecision(): BigDecimal = BigDecimal.valueOf(value)
	override operator fun unaryMinus() = DecimalCalculable(-value)
	override fun toString() = value.toString()
}
/** @see License */
class IntegerCalculable(private val value: Long): Calculable {
	companion object {
		val ONE = IntegerCalculable(1)
		val TWO = IntegerCalculable(2)
	}
	override fun calcTo16bFloat() = value.toFloat()
	override fun calcTo32bFloat() = value.toDouble()
	override fun calcToMaxPrecision(): BigDecimal = BigDecimal.valueOf(value)
	override operator fun unaryMinus() = IntegerCalculable(-value)
	override fun toString() = value.toString()
}

/**
 * Value calculated upon construction.
 * @see License
 */
class CalculableProduct(vararg factors: Calculable): ArbitraryPrecisionCalculable(calcProductOf(factors)) {
	companion object {
		private fun calcProductOf(factors: Array<out Calculable>): BigDecimal {
			var p = BigDecimal.ONE
			for (c in factors) p = p.multiply(c.calcToMaxPrecision())
			return p
		}
		fun productOf(vararg factors: Calculable) = CalculableProduct(*factors)
	}
	fun overProductOf(vararg divisors: Approximable) = ApproximableProduct(this).overProductOf(*divisors)
	fun overProductOf(vararg divisors: Calculable) = ArbitraryPrecisionCalculable(value.divide(productOf(*divisors)
		.calcToMaxPrecision(), LibraryGlobals.roundingMode))
	override operator fun div(other: Approximable) = overProductOf(other)
	override operator fun div(other: Calculable) = overProductOf(other)
}
