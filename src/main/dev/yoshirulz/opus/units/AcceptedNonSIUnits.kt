package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.LibraryGlobals.ConstantSymbolName.*
import dev.yoshirulz.opus.LibraryGlobals.i18nGetConstantSymbol
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class MinuteClass: AcceptedSIUnit<ISQTime> {
	Minute, Minutes;
	override val qDimension = ISQTime.QDIM
	override val unitSymbol = "min"
}
/** @see License */
enum class HourClass: AcceptedSIUnit<ISQTime> {
	Hour, Hours;
	override val qDimension = ISQTime.QDIM
	override val unitSymbol = "h"
}
/** @see License */
enum class DayClass: AcceptedSIUnit<ISQTime> {
	Day, Days;
	override val qDimension = ISQTime.QDIM
	override val unitSymbol = "d"
}
/** @see License */
enum class DegreeOfArcClass: AcceptedSIUnit<ISQAngle> {
	Degree, Degrees;
	override val qDimension = ISQAngle.QDIM
	override val unitSymbol = "°"
}
/** @see License */
enum class MinuteOfArcClass: AcceptedSIUnit<ISQAngle> {
	Minute, Minutes;
	override val qDimension = ISQAngle.QDIM
	override val unitSymbol = "′"
}
/** @see License */
enum class SecondOfArcClass: AcceptedSIUnit<ISQAngle> {
	Second, Seconds;
	override val qDimension = ISQAngle.QDIM
	override val unitSymbol = "″"
}
/** @see License */
enum class HectareClass: AcceptedSIUnit<ISQArea> {
	Hectare, Hectares;
	override val qDimension = ISQArea.QDIM
	override val unitSymbol = "ha"
}
/** @see License */
enum class LitreClass: AcceptedSIUnit<ISQVolume> {
	Litre, Litres;
	override val qDimension = ISQVolume.QDIM
	override val unitSymbol = "L"
}
/** @see License */
enum class TonneClass: AcceptedSIUnit<ISQMass> {
	Tonne, Tonnes;
	override val qDimension = ISQMass.QDIM
	override val unitSymbol = "t"
}
/** @see License */
enum class ElectronVoltClass: AcceptedSIUnit<ISQEnergy> {
	ElectronVolt, ElectronVolts;
	override val qDimension = ISQEnergy.QDIM
	override val unitSymbol = "eV"
}
/** @see License */
enum class DaltonClass: AcceptedSIUnit<ISQMass> {
	Dalton, Daltons;
	override val qDimension = ISQMass.QDIM
	override val unitSymbol = "Da"
}
/** @see License */
enum class AstronomicalUnitClass: AcceptedSIUnit<ISQLength> {
	AstronomicalUnit, AstronomicalUnits;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "ua"
}
/** @see License */
enum class NaturalUnitOfSpeedClass: AcceptedSIUnit<ISQSpeed> {
	NaturalUnitOfSpeed, NaturalUnitsOfSpeed;
	override val qDimension = ISQSpeed.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(SpeedOfLightInFreeSpace)
}
/** @see License */
enum class NaturalUnitOfActionClass: AcceptedSIUnit<ISQAction> {
	NaturalUnitOfAction, NaturalUnitsOfAction;
	override val qDimension = ISQAction.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(ReducedPlanckConstant)
}
/** @see License */
enum class NaturalUnitOfMassClass: AcceptedSIUnit<ISQMass> {
	NaturalUnitOfMass, NaturalUnitsOfMass;
	override val qDimension = ISQMass.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(ElectronMass)
}
/** @see License */
enum class NaturalUnitOfTimeClass: AcceptedSIUnit<ISQTime> {
	NaturalUnitOfTime, NaturalUnitsOfTime;
	override val qDimension = ISQTime.QDIM
	override val unitSymbol get() = "${i18nGetConstantSymbol(ReducedPlanckConstant)}/(${i18nGetConstantSymbol(ElectronMass)}${i18nGetConstantSymbol(SpeedOfLightInFreeSpaceSquared)})"
}
/** @see License */
enum class AtomicUnitOfChargeClass: AcceptedSIUnit<ISQElectricCharge> {
	AtomicUnitOfCharge, AtomicUnitsOfCharge;
	override val qDimension = ISQElectricCharge.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(ElectronChargeNegative)
}
/** @see License */
enum class AtomicUnitOfMassClass: AcceptedSIUnit<ISQMass> {
	AtomicUnitOfMass, AtomicUnitsOfMass;
	override val qDimension = ISQMass.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(ElectronMass)
}
/** @see License */
enum class AtomicUnitOfActionClass: AcceptedSIUnit<ISQAction> {
	AtomicUnitOfAction, AtomicUnitsOfAction;
	override val qDimension = ISQAction.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(ReducedPlanckConstant)
}
/** @see License */
enum class AtomicUnitOfLengthClass: AcceptedSIUnit<ISQLength> {
	AtomicUnitOfLength, AtomicUnitsOfLength,
	Bohr, Bohrs;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(BohrRadius)
}
/** @see License */
enum class AtomicUnitOfEnergyClass: AcceptedSIUnit<ISQEnergy> {
	AtomicUnitOfEnergy, AtomicUnitsOfEnergy,
	Hartree, Hartrees;
	override val qDimension = ISQEnergy.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(HartreeEnergy)
}
/** @see License */
enum class AtomicUnitOfTimeClass: AcceptedSIUnit<ISQTime> {
	AtomicUnitOfTime, AtomicUnitsOfTime;
	override val qDimension = ISQTime.QDIM
	override val unitSymbol get() = "${i18nGetConstantSymbol(ReducedPlanckConstant)}/${i18nGetConstantSymbol(HartreeEnergy)}"
}
