package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class AccelerationUnitNameClass: OPUSUnit<Acceleration> {
	AccelerationUnitName, AccelerationUnitNames;
	override val qDimension = Acceleration.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ActionUnitNameClass: OPUSUnit<Action> {
	ActionUnitName, ActionUnitNames;
	override val qDimension = Action.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class AngularAccelerationUnitNameClass: OPUSUnit<AngularAcceleration> {
	AngularAccelerationUnitName, AngularAccelerationUnitNames;
	override val qDimension = AngularAcceleration.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class AngularJerkUnitNameClass: OPUSUnit<AngularJerk> {
	AngularJerkUnitName, AngularJerkUnitNames;
	override val qDimension = AngularJerk.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class AngularMomentumUnitNameClass: OPUSUnit<AngularMomentum> {
	AngularMomentumUnitName, AngularMomentumUnitNames;
	override val qDimension = AngularMomentum.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class AngularSpeedUnitNameClass: OPUSUnit<AngularSpeed> {
	AngularSpeedUnitName, AngularSpeedUnitNames;
	override val qDimension = AngularSpeed.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class AreaDensityUnitNameClass: OPUSUnit<AreaDensity> {
	AreaDensityUnitName, AreaDensityUnitNames;
	override val qDimension = AreaDensity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class CatalyticActivityUnitNameClass: OPUSUnit<CatalyticActivity> {
	CatalyticActivityUnitName, CatalyticActivityUnitNames;
	override val qDimension = CatalyticActivity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ChargeMassRatioUnitNameClass: OPUSUnit<ChargeMassRatio> {
	ChargeMassRatioUnitName, ChargeMassRatioUnitNames;
	override val qDimension = ChargeMassRatio.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class DensityUnitNameClass: OPUSUnit<Density> {
	DensityUnitName, DensityUnitNames;
	override val qDimension = Density.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class DisplacementFieldStrengthUnitNameClass: OPUSUnit<DisplacementFieldStrength> {
	DisplacementFieldStrengthUnitName, DisplacementFieldStrengthUnitNames;
	override val qDimension = DisplacementFieldStrength.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class DynamicViscosityUnitNameClass: OPUSUnit<DynamicViscosity> {
	DynamicViscosityUnitName, DynamicViscosityUnitNames;
	override val qDimension = DynamicViscosity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ElectricalCapacitanceUnitNameClass: OPUSUnit<ElectricalCapacitance> {
	ElectricalCapacitanceUnitName, ElectricalCapacitanceUnitNames;
	override val qDimension = ElectricalCapacitance.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ElectricalConductanceUnitNameClass: OPUSUnit<ElectricalConductance> {
	ElectricalConductanceUnitName, ElectricalConductanceUnitNames;
	override val qDimension = ElectricalConductance.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ElectricalInductanceUnitNameClass: OPUSUnit<ElectricalInductance> {
	ElectricalInductanceUnitName, ElectricalInductanceUnitNames;
	override val qDimension = ElectricalInductance.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ElectricalResistanceUnitNameClass: OPUSUnit<ElectricalResistance> {
	ElectricalResistanceUnitName, ElectricalResistanceUnitNames;
	override val qDimension = ElectricalResistance.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ElectricCurrentUnitNameClass: OPUSUnit<ElectricCurrent> {
	ElectricCurrentUnitName, ElectricCurrentUnitNames;
	override val qDimension = ElectricCurrent.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ElectricFieldStrengthUnitNameClass: OPUSUnit<ElectricFieldStrength> {
	ElectricFieldStrengthUnitName, ElectricFieldStrengthUnitNames;
	override val qDimension = ElectricFieldStrength.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ElectrostaticPotentialUnitNameClass: OPUSUnit<ElectrostaticPotential> {
	ElectrostaticPotentialUnitName, ElectrostaticPotentialUnitNames;
	override val qDimension = ElectrostaticPotential.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class EnergyUnitNameClass: OPUSUnit<Energy> {
	EnergyUnitName, EnergyUnitNames;
	override val qDimension = Energy.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class EnergyDensityUnitNameClass: OPUSUnit<EnergyDensity> {
	EnergyDensityUnitName, EnergyDensityUnitNames;
	override val qDimension = EnergyDensity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class EntropyUnitNameClass: OPUSUnit<Entropy> {
	EntropyUnitName, EntropyUnitNames;
	override val qDimension = Entropy.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ForceUnitNameClass: OPUSUnit<Force> {
	ForceUnitName, ForceUnitNames;
	override val qDimension = Force.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class FrequencyUnitNameClass: OPUSUnit<Frequency> {
	FrequencyUnitName, FrequencyUnitNames;
	override val qDimension = Frequency.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class HeatCapacityUnitNameClass: OPUSUnit<HeatCapacity> {
	HeatCapacityUnitName, HeatCapacityUnitNames;
	override val qDimension = HeatCapacity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class OPUSHydrogenPotentialUnitNameClass: OPUSUnit<HydrogenPotential> {
	HydrogenPotentialUnitName;
	override val qDimension = HydrogenPotential.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class OPUSHydroxidePotentialUnitNameClass: OPUSUnit<HydroxidePotential> {
	HydroxidePotentialUnitName;
	override val qDimension = HydroxidePotential.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class JerkUnitNameClass: OPUSUnit<Jerk> {
	JerkUnitName, JerkUnitNames;
	override val qDimension = Jerk.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class KinematicViscosityUnitNameClass: OPUSUnit<KinematicViscosity> {
	KinematicViscosityUnitName, KinematicViscosityUnitNames;
	override val qDimension = KinematicViscosity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class LongitudinCubedClass: OPUSUnit<Volume> {
	LongitudinCubed, LongitudinsCubed;
	override val qDimension = Volume.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class LongitudinSquaredClass: OPUSUnit<Area> {
	LongitudinSquared, LongitudinsSquared;
	override val qDimension = Area.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MagneticFieldHStrengthUnitNameClass: OPUSUnit<MagneticFieldHStrength> {
	MagneticFieldHStrengthUnitName, MagneticFieldHStrengthUnitNames;
	override val qDimension = MagneticFieldHStrength.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MagneticFluxDensityUnitNameClass: OPUSUnit<MagneticFluxDensity> {
	MagneticFluxDensityUnitName, MagneticFluxDensityUnitNames;
	override val qDimension = MagneticFluxDensity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MagneticFluxUnitNameClass: OPUSUnit<MagneticFlux> {
	MagneticFluxUnitName, MagneticFluxUnitNames;
	override val qDimension = MagneticFlux.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MagneticMomentUnitNameClass: OPUSUnit<MagneticMoment> {
	MagneticMomentUnitName, MagneticMomentUnitNames;
	override val qDimension = MagneticMoment.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MolarConcentrationUnitNameClass: OPUSUnit<MolarConcentration> {
	MolarConcentrationUnitName, MolarConcentrationUnitNames;
	override val qDimension = MolarConcentration.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MolarMassUnitNameClass: OPUSUnit<MolarMass> {
	MolarMassUnitName, MolarMassUnitNames;
	override val qDimension = MolarMass.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MolarEnergyUnitNameClass: OPUSUnit<MolarEnergy> {
	MolarEnergyUnitName, MolarEnergyUnitNames;
	override val qDimension = MolarEnergy.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MolarHeatCapacityUnitNameClass: OPUSUnit<MolarHeatCapacity> {
	MolarHeatCapacityUnitName, MolarHeatCapacityUnitNames;
	override val qDimension = MolarHeatCapacity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MolarVolumeUnitNameClass: OPUSUnit<MolarVolume> {
	MolarVolumeUnitName, MolarVolumeUnitNames;
	override val qDimension = MolarVolume.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MomentumUnitNameClass: OPUSUnit<Momentum> {
	MomentumUnitName, MomentumUnitNames;
	override val qDimension = Momentum.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class PermeabilityUnitNameClass: OPUSUnit<Permeability> {
	PermeabilityUnitName, PermeabilityUnitNames;
	override val qDimension = Permeability.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class PermittivityUnitNameClass: OPUSUnit<Permittivity> {
	PermittivityUnitName, PermittivityUnitNames;
	override val qDimension = Permittivity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class PowerUnitNameClass: OPUSUnit<Power> {
	PowerUnitName, PowerUnitNames;
	override val qDimension = Power.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class PressureUnitNameClass: OPUSUnit<Pressure> {
	PressureUnitName, PressureUnitNames;
	override val qDimension = Pressure.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class RotatumUnitNameClass: OPUSUnit<Rotatum> {
	RotatumUnitName, RotatumUnitNames;
	override val qDimension = Rotatum.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class SpecificEnergyUnitNameClass: OPUSUnit<SpecificEnergy> {
	SpecificEnergyUnitName, SpecificEnergyUnitNames;
	override val qDimension = SpecificEnergy.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class SpecificHeatCapacityUnitNameClass: OPUSUnit<SpecificHeatCapacity> {
	SpecificHeatCapacityUnitName, SpecificHeatCapacityUnitNames;
	override val qDimension = SpecificHeatCapacity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class SpecificVolumeUnitNameClass: OPUSUnit<SpecificVolume> {
	SpecificVolumeUnitName, SpecificVolumeUnitNames;
	override val qDimension = SpecificVolume.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class SpeedUnitNameClass: OPUSUnit<Speed> {
	SpeedUnitName, SpeedUnitNames;
	override val qDimension = Speed.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class SuperrevolutionClass: OPUSUnit<SolidAngle> {
	Superrevolution, Superrevolutions;
	override val qDimension = SolidAngle.QDIM
	override val unitSymbol = "Srev"
}
/** @see License */
enum class SurfaceChargeDensityUnitNameClass: OPUSUnit<SurfaceChargeDensity> {
	SurfaceChargeDensityUnitName, SurfaceChargeDensityUnitNames;
	override val qDimension = SurfaceChargeDensity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class SurfaceTensionUnitNameClass: OPUSUnit<SurfaceTension> {
	SurfaceTensionUnitName, SurfaceTensionUnitNames;
	override val qDimension = SurfaceTension.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ThermalConductivityUnitNameClass: OPUSUnit<ThermalConductivity> {
	ThermalConductivityUnitName, ThermalConductivityUnitNames;
	override val qDimension = ThermalConductivity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class TorqueUnitNameClass: OPUSUnit<Torque> {
	TorqueUnitName, TorqueUnitNames;
	override val qDimension = Torque.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class ChargeDensityUnitNameClass: OPUSUnit<ChargeDensity> {
	ChargeDensityUnitName, ChargeDensityUnitNames;
	override val qDimension = ChargeDensity.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class VolumetricFlowRateUnitNameClass: OPUSUnit<VolumetricFlowRate> {
	VolumetricFlowRateUnitName, VolumetricFlowRateUnitNames;
	override val qDimension = VolumetricFlowRate.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class WavenumberUnitNameClass: OPUSUnit<Wavenumber> {
	WavenumberUnitName, WavenumberUnitNames;
	override val qDimension = Wavenumber.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class YankUnitNameClass: OPUSUnit<Yank> {
	YankUnitName, YankUnitNames;
	override val qDimension = Yank.QDIM
	override val unitSymbol = "TODO"
}
