package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*
import dev.yoshirulz.opus.quantity.*
import dev.yoshirulz.opus.units.UnitSystem.*
import java.math.BigDecimal

/** @see License */
interface UnitSystem {
	/**
	 * Neo-SI, proposed SI replacement.
	 * tl;dr: base units are SI - candela + radian + dollar.
	 *
	 * Proposed deanthropogenization of the SI, also maintains angles as an independent dimension not equal to unity.
	 * Does *not* replace current with charge, making it "backwards-compatible" with the SI (excluding, of course, light
	 * perception). All units are inherited from the SI, but each base unit's dimensions are defined per Neo-ISQ i.e.
	 * (L, M, T, I, R, Θ, N, V).
	 * @see License
	 */
	object NeoSI: UnitSystem
	/**
	 * Opus Potentia Unit System.
	 *
	 * Base units manifest each dimension defined in the OPUS Quantity System (OQuS, /oʊkəs/) i.e.
	 * (T, L, M, Q, R, Θ, N, Η, V).
	 * @see License
	 */
	object OPUS: UnitSystem
	/**
	 * Système International d'Unités, formal codification of the "metric system".
	 *
	 * Base units manifest each dimension defined in the International System of Quantities (ISQ) i.e.
	 * (L, M, T, I, Θ, N, J).
	 * @see License
	 */
	object SI: UnitSystem
}

/** @see License */
interface UnitInterface<D: QuantityDimension<S>, S: UnitSystem> {
	val qDimension: D
	val reciprocal: UnitInterface<*, S>
	val unitSymbol: String

	fun qOf(v: Approximable) = ApproximableQuantity(v, this)
	fun qOf(v: Calculable) = Quantity(v, this)
	fun qOf(v: BigDecimal) = Quantity(v, this)
	fun qOf(v: Double) = Quantity(v, this)
	fun qOf(v: Long) = Quantity(v, this)

	fun by(multiplier: Approximable) = qOf(multiplier)
	fun over(divisor: Approximable) = qOf(IntegerCalculable.ONE) / divisor
}

/** @see License */
interface UnitAmalgam<D: QuantityDimension<S>, S: UnitSystem>: UnitInterface<D, S>

/** @see License */
interface NeoSIUnit<D: NeoISQQuantityDimension>: UnitInterface<D, NeoSI> {
	override val qDimension: D
	override val reciprocal get() = NeoSIUnitReciprocal(this)

	fun by(multiplier: NeoSIUnit<*>) = NeoSIUnitAmalgam(arrayOf(this, multiplier))
}
/** @see License */
class NeoSIUnitReciprocal<D: NeoISQQuantityDimension> internal constructor(u: NeoSIUnit<D>,
			override val qDimension: NeoISQReciprocalDim<D> = NeoISQReciprocalDim(u.qDimension)):
		NeoSIUnit<NeoISQReciprocalDim<D>> {
	override val unitSymbol = "${u.unitSymbol}^-1"
}

/** @see License */
interface OPUSUnit<D: OQuSQuantityDimension>: UnitInterface<D, OPUS> {
	override val qDimension: D
	override val reciprocal get() = OPUSUnitReciprocal(this)
	fun by(multiplier: OPUSUnit<*>): OPUSUnitAmalgam = OPUSUnitAmalgam(arrayOf(this, multiplier))
}
/** @see License */
class OPUSUnitReciprocal<D: OQuSQuantityDimension> internal constructor(u: OPUSUnit<D>,
			override val qDimension: OQuSReciprocalDim<D> = OQuSReciprocalDim(u.qDimension)):
		OPUSUnit<OQuSReciprocalDim<D>> {
	override val unitSymbol = "${u.unitSymbol}^-1"
}

/** @see License */
interface SIUnit<D: ISQQuantityDimension>: UnitInterface<D, SI> {
	val isAcceptedUnit: Boolean
	override val qDimension: D
	override val reciprocal get() = SIUnitReciprocal(this)
	fun by(multiplier: SIUnit<*>) = SIUnitAmalgam(arrayOf(this, multiplier))
}
/** @see License */
interface AcceptedSIUnit<D: ISQQuantityDimension>: SIUnit<D> {
	override val isAcceptedUnit get() = true
}
/** @see License */
interface UnacceptedSIUnit<D: ISQQuantityDimension>: SIUnit<D> {
	override val isAcceptedUnit get() = false
}
/** @see License */
interface USCustomaryUnit<D: ISQQuantityDimension>: UnacceptedSIUnit<D>
/** @see License */
class SIUnitReciprocal<D: ISQQuantityDimension> internal constructor(u: SIUnit<D>,
			override val qDimension: ISQReciprocalDim<D> = ISQReciprocalDim(u.qDimension)):
		UnacceptedSIUnit<ISQReciprocalDim<D>> {
	override val unitSymbol = "${u.unitSymbol}^-1"
}
