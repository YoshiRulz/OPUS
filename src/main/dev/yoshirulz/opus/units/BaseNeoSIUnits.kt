package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class NeoSIUnitlessClass: NeoSIUnit<NeoISQDimensionOne> {
	Unitless;
	override val qDimension = NeoISQDimensionOne.QDIM
	override val unitSymbol = ""
}
/** @see License */
enum class NeoSIMetreClass: NeoSIUnit<NeoISQLength> {
	Metre, Metres;
	override val qDimension = NeoISQLength.QDIM
	override val unitSymbol = "m"
}
/** @see License */
enum class NeoSIKilogramClass: NeoSIUnit<NeoISQMass> {
	Kilogram, Kilograms;
	override val qDimension = NeoISQMass.QDIM
	override val unitSymbol = "kg"
}
/** @see License */
enum class NeoSISecondClass: NeoSIUnit<NeoISQTime> {
	Second, Seconds;
	override val qDimension = NeoISQTime.QDIM
	override val unitSymbol = "s"
}
/** @see License */
enum class NeoSIAmpereClass: NeoSIUnit<NeoISQElectricCurrent> {
	Ampere, Amperes;
	override val qDimension = NeoISQElectricCurrent.QDIM
	override val unitSymbol = "A"
}
/** @see License */
enum class NeoSIRadianClass: NeoSIUnit<NeoISQAngle> {
	Radian, Radians;
	override val qDimension = NeoISQAngle.QDIM
	override val unitSymbol = "rad"
}
/** @see License */
enum class NeoSIKelvinClass: NeoSIUnit<NeoISQAbsoluteTemperature> {
	Kelvin, Kelvins;
	override val qDimension = NeoISQAbsoluteTemperature.QDIM
	override val unitSymbol = "K"
}
/** @see License */
enum class NeoSIMoleClass: NeoSIUnit<NeoISQAmountOfSubstance> {
	Mole, Moles;
	override val qDimension = NeoISQAmountOfSubstance.QDIM
	override val unitSymbol = "mol"
}
/** @see License */
enum class NeoSIUnitOfAccountClass: NeoSIUnit<NeoISQFinancialValue> {
	UnitOfAccount, UnitsOfAccount;
	override val qDimension = NeoISQFinancialValue.QDIM
	override val unitSymbol = "uacc"
}
