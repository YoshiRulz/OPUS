package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.LibraryGlobals.ConstantSymbolName.SpeedOfLightInFreeSpace
import dev.yoshirulz.opus.LibraryGlobals.i18nGetConstantSymbol
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/**
 * Accepted by IUPAC.
 * @see License
 */
enum class MolarClass: UnacceptedSIUnit<ISQMolarConcentration> {
	Molar;
	override val qDimension = ISQMolarConcentration.QDIM
	override val unitSymbol = "M"
}
/**
 * De-facto accepted.
 * @see License
 */
enum class SILightspeedClass: UnacceptedSIUnit<ISQSpeed> {
	Lightspeed;
	override val qDimension = ISQSpeed.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(SpeedOfLightInFreeSpace)
}
/**
 * Accepted by IAU.
 * @see License
 */
enum class SILightYearClass: UnacceptedSIUnit<ISQLength> {
	Lightyear, Lightyears;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "ly"
}
/**
 * Accepted by IAU.
 * @see License
 */
enum class ParsecClass: UnacceptedSIUnit<ISQLength> {
	Parsec, Parsecs;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "pc"
}
/**
 * De-facto accepted.
 * @see License
 */
enum class SIGenericDollarClass: UnacceptedSIUnit<ISQFinancialValue> {
	GenericDollar, GenericDollars;
	override val qDimension = ISQFinancialValue.QDIM
	override val unitSymbol = "$"
}
/** @see License */
enum class BarClass: UnacceptedSIUnit<ISQPressure> {
	Bar, Bars;
	override val qDimension = ISQPressure.QDIM
	override val unitSymbol = "bar"
}
/** @see License */
enum class MillimetreMercuryClass: UnacceptedSIUnit<ISQPressure> {
	MillimetreMercury, MillimetresMercury;
	override val qDimension = ISQPressure.QDIM
	override val unitSymbol = "mmHg"
}
/** @see License */
@Suppress("ClassName", "EnumEntryName")
enum class ÅngströmClass: UnacceptedSIUnit<ISQLength> {
	Ångström, Ångströms;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "Å"
}
/** @see License */
enum class NauticalMileClass: UnacceptedSIUnit<ISQLength> {
	NauticalMile, NauticalMiles;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "M"
}
/** @see License */
enum class BarnClass: UnacceptedSIUnit<ISQArea> {
	Barn, Barns;
	override val qDimension = ISQArea.QDIM
	override val unitSymbol = "b"
}
/** @see License */
enum class KnotClass: UnacceptedSIUnit<ISQSpeed> {
	Knot, Knots;
	override val qDimension = ISQSpeed.QDIM
	override val unitSymbol = "kn"
}
/** @see License */
enum class NeperClass: UnacceptedSIUnit<ISQDimensionOne> {
	Neper, Nepers;
	override val qDimension = ISQDimensionOne.QDIM
	override val unitSymbol = "Np"
}
/** @see License */
enum class BelClass: UnacceptedSIUnit<ISQDimensionOne> {
	Bel, Bels;
	override val qDimension = ISQDimensionOne.QDIM
	override val unitSymbol = "B"
}
