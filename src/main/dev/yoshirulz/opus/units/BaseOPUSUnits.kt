package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class OPUSUnitlessClass: OPUSUnit<DimensionOne> {
	Unitless;
	override val qDimension = DimensionOne.QDIM
	override val unitSymbol = ""
}
/** @see License */
enum class TempunClass: OPUSUnit<Time> {
	Tempun, Tempuns;
	override val qDimension = Time.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class LongitudinClass: OPUSUnit<Length> {
	Longitudin, Longitudins;
	override val qDimension = Length.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class MassUnitNameClass: OPUSUnit<Mass> {
	MassUnitName, MassUnitNames;
	override val qDimension = Mass.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class CharjonClass: OPUSUnit<ElectricCharge> {
	Charjon, Charjons;
	override val qDimension = ElectricCharge.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class RevolutionClass: OPUSUnit<Angle> {
	Revolution, Revolutions;
	override val qDimension = Angle.QDIM
	override val unitSymbol = "Rev"
}
/** @see License */
enum class AbsoluteTemperatureUnitNameClass: OPUSUnit<AbsoluteTemperature> {
	AbsoluteTemperatureUnitName, AbsoluteTemperatureUnitNames;
	override val qDimension = AbsoluteTemperature.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class AmountOfSubstanceUnitNameClass: OPUSUnit<AmountOfSubstance> {
	AmountOfSubstanceUnitName, AmountOfSubstanceUnitNames;
	override val qDimension = AmountOfSubstance.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class SymbolCountUnitNameClass: OPUSUnit<SymbolCount> {
	SymbolCountUnitName, SymbolCountUnitNames;
	override val qDimension = SymbolCount.QDIM
	override val unitSymbol = "TODO"
}
/** @see License */
enum class OPUSUnitOfAccountClass: OPUSUnit<FinancialValue> {
	UnitOfAccount, UnitsOfAccount;
	override val qDimension = FinancialValue.QDIM
	override val unitSymbol = "UoA"
}
