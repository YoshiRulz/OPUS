package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class NeoSIHertzClass: NeoSIUnit<NeoISQFrequency> {
	Hertz;
	override val qDimension = NeoISQFrequency.QDIM
	override val unitSymbol = "Hz"
}
/** @see License */
enum class NeoSISteradianClass: NeoSIUnit<NeoISQSolidAngle> {
	Steradian, Steradians;
	override val qDimension = NeoISQSolidAngle.QDIM
	override val unitSymbol = "sr"
}
/** @see License */
enum class NeoSINewtonClass: NeoSIUnit<NeoISQForce> {
	Newton, Newtons;
	override val qDimension = NeoISQForce.QDIM
	override val unitSymbol = "N"
}
/** @see License */
enum class NeoSIPascalClass: NeoSIUnit<NeoISQPressure> {
	Pascal, Pascals;
	override val qDimension = NeoISQPressure.QDIM
	override val unitSymbol = "Pa"
}
/** @see License */
enum class NeoSIJouleClass: NeoSIUnit<NeoISQEnergy> {
	Joule, Joules;
	override val qDimension = NeoISQEnergy.QDIM
	override val unitSymbol = "J"
}
/** @see License */
enum class NeoSIWattClass: NeoSIUnit<NeoISQPower> {
	Watt, Watts;
	override val qDimension = NeoISQPower.QDIM
	override val unitSymbol = "W"
}
/** @see License */
enum class NeoSICoulombClass: NeoSIUnit<NeoISQElectricCharge> {
	Coulomb, Coulombs;
	override val qDimension = NeoISQElectricCharge.QDIM
	override val unitSymbol = "C"
}
/** @see License */
enum class NeoSIVoltClass: NeoSIUnit<NeoISQElectrostaticPotential> {
	Volt, Volts;
	override val qDimension = NeoISQElectrostaticPotential.QDIM
	override val unitSymbol = "V"
}
/** @see License */
enum class NeoSIFaradClass: NeoSIUnit<NeoISQElectricalCapacitance> {
	Farad, Farads;
	override val qDimension = NeoISQElectricalCapacitance.QDIM
	override val unitSymbol = "F"
}
/** @see License */
enum class NeoSIOhmClass: NeoSIUnit<NeoISQElectricalResistance> {
	Ohm, Ohms;
	override val qDimension = NeoISQElectricalResistance.QDIM
	override val unitSymbol = "Ω"
}
/** @see License */
enum class NeoSISiemensClass: NeoSIUnit<NeoISQElectricalConductance> {
	Siemens;
	override val qDimension = NeoISQElectricalConductance.QDIM
	override val unitSymbol = "S"
}
/** @see License */
enum class NeoSIWeberClass: NeoSIUnit<NeoISQMagneticFlux> {
	Weber, Webers;
	override val qDimension = NeoISQMagneticFlux.QDIM
	override val unitSymbol = "Wb"
}
/** @see License */
enum class NeoSITeslaClass: NeoSIUnit<NeoISQMagneticFluxDensity> {
	Tesla, Teslas;
	override val qDimension = NeoISQMagneticFluxDensity.QDIM
	override val unitSymbol = "T"
}
/** @see License */
enum class NeoSIHenryClass: NeoSIUnit<NeoISQElectricalInductance> {
	Henry, Henries;
	override val qDimension = NeoISQElectricalInductance.QDIM
	override val unitSymbol = "H"
}
/** @see License */
enum class NeoSIKatalClass: NeoSIUnit<NeoISQCatalyticActivity> {
	Katal, Katals;
	override val qDimension = NeoISQCatalyticActivity.QDIM
	override val unitSymbol = "kat"
}
/** @see License */
enum class NeoSIGenericDollarClass: NeoSIUnit<NeoISQFinancialValue> {
	GenericDollar, GenericDollars;
	override val qDimension = NeoISQFinancialValue.QDIM
	override val unitSymbol = "$"
}

/** @see License */
enum class NeoSIAmperePerMetreClass: NeoSIUnit<NeoISQMagneticFieldHStrength> {
	AmperePerMetre, AmperesPerMetre;
	override val qDimension = NeoISQMagneticFieldHStrength.QDIM
	override val unitSymbol = "A/m"
}
/** @see License */
enum class NeoSICoulombPerKilogramClass: NeoSIUnit<NeoISQChargeMassRatio> {
	CoulombPerKilogram, CoulombsPerKilogram;
	override val qDimension = NeoISQChargeMassRatio.QDIM
	override val unitSymbol = "C/kg"
}
/** @see License */
enum class NeoSICoulombPerMetreCubedClass: NeoSIUnit<NeoISQChargeDensity> {
	CoulombPerMetreCubed, CoulombsPerMetreCubed;
	override val qDimension = NeoISQChargeDensity.QDIM
	override val unitSymbol = "C/m^3"
}
/** @see License */
enum class NeoSICoulombPerMetreSquaredClass: NeoSIUnit<NeoISQSurfaceChargeDensity> {
	CoulombPerMetreSquared, CoulombsPerMetreSquared;
	override val qDimension = NeoISQSurfaceChargeDensity.QDIM
	override val unitSymbol = "C/m^2"
}
/** @see License */
enum class NeoSIFaradPerMetreClass: NeoSIUnit<NeoISQPermittivity> {
	FaradPerMetre, FaradsPerMetre;
	override val qDimension = NeoISQPermittivity.QDIM
	override val unitSymbol = "F/m"
}
/** @see License */
enum class NeoSIHenryPerMetreClass: NeoSIUnit<NeoISQPermeability> {
	HenryPerMetre, HenriesPerMetre;
	override val qDimension = NeoISQPermeability.QDIM
	override val unitSymbol = "H/m"
}
/** @see License */
enum class NeoSIJoulePerKelvinEntropyClass: NeoSIUnit<NeoISQEntropy> {
	JoulePerKelvin, JoulesPerKelvin;
	override val qDimension = NeoISQEntropy.QDIM
	override val unitSymbol = "J/K"
}
/** @see License */
enum class NeoSIJoulePerKelvinHeatCapacityClass: NeoSIUnit<NeoISQHeatCapacity> {
	JoulePerKelvin, JoulesPerKelvin;
	override val qDimension = NeoISQHeatCapacity.QDIM
	override val unitSymbol = "J/K"
}
/** @see License */
enum class NeoSIJoulePerKilogramClass: NeoSIUnit<NeoISQSpecificEnergy> {
	JoulePerKilogram, JoulesPerKilogram;
	override val qDimension = NeoISQSpecificEnergy.QDIM
	override val unitSymbol = "J/kg"
}
/** @see License */
enum class NeoSIJoulePerKilogramKelvinClass: NeoSIUnit<NeoISQSpecificHeatCapacity> {
	JoulePerKilogramKelvin, JoulesPerKilogramKelvin;
	override val qDimension = NeoISQSpecificHeatCapacity.QDIM
	override val unitSymbol = "J/(kg·K)"
}
/** @see License */
enum class NeoSIJoulePerMetreCubedClass: NeoSIUnit<NeoISQEnergyDensity> {
	JoulePerMetreCubed, JoulesPerMetreCubed;
	override val qDimension = NeoISQEnergyDensity.QDIM
	override val unitSymbol = "J/K"
}
/** @see License */
enum class NeoSIJoulePerMoleClass: NeoSIUnit<NeoISQMolarEnergy> {
	JoulePerMole, JoulesPerMole;
	override val qDimension = NeoISQMolarEnergy.QDIM
	override val unitSymbol = "J/mol"
}
/** @see License */
enum class NeoSIJoulePerMoleKelvinClass: NeoSIUnit<NeoISQMolarHeatCapacity> {
	JoulePerMoleKelvin, JoulesPerMoleKelvin;
	override val qDimension = NeoISQMolarHeatCapacity.QDIM
	override val unitSymbol = "J/(mol·K)"
}
/** @see License */
enum class NeoSIJoulePerRadianClass: NeoSIUnit<NeoISQTorque> {
	JoulePerRadian, JoulesPerRadian;
	override val qDimension = NeoISQTorque.QDIM
	override val unitSymbol = "J/rad"
}
/** @see License */
enum class NeoSIJoulePerTeslaClass: NeoSIUnit<NeoISQMagneticMoment> {
	JoulePerTesla, JoulesPerTesla;
	override val qDimension = NeoISQMagneticMoment.QDIM
	override val unitSymbol = "J/T"
}
/** @see License */
enum class NeoSIJouleSecondActionClass: NeoSIUnit<NeoISQAction> {
	JouleSecond, JouleSeconds;
	override val qDimension = NeoISQAction.QDIM
	override val unitSymbol = "J·s"
}
/** @see License */
enum class NeoSIKilogramPerMetreCubedClass: NeoSIUnit<NeoISQDensity> {
	KilogramPerMetreCubed, KilogramsPerMetreCubed;
	override val qDimension = NeoISQDensity.QDIM
	override val unitSymbol = "kg/m^3"
}
/** @see License */
enum class NeoSIKilogramPerMetreSquaredClass: NeoSIUnit<NeoISQAreaDensity> {
	KilogramPerMetreSquared, KilogramsPerMetreSquared;
	override val qDimension = NeoISQAreaDensity.QDIM
	override val unitSymbol = "kg/m^2"
}
/** @see License */
enum class NeoSIKilogramPerMoleClass: NeoSIUnit<NeoISQMolarMass> {
	KilogramPerMole, KilogramsPerMole;
	override val qDimension = NeoISQMolarMass.QDIM
	override val unitSymbol = "kg/mol"
}
/** @see License */
enum class NeoSIMetreCubedClass: NeoSIUnit<NeoISQVolume> {
	MetreCubed, MetresCubed;
	override val qDimension = NeoISQVolume.QDIM
	override val unitSymbol = "m^3"
}
/** @see License */
enum class NeoSIMetreCubedPerKilogramClass: NeoSIUnit<NeoISQSpecificVolume> {
	MetreCubedPerKilogram, MetresCubedPerKilogram;
	override val qDimension = NeoISQSpecificVolume.QDIM
	override val unitSymbol = "m^3/kg"
}
/** @see License */
enum class NeoSIMetreCubedPerMoleClass: NeoSIUnit<NeoISQMolarVolume> {
	MetreCubedPerMole, MetresCubedPerMole;
	override val qDimension = NeoISQMolarVolume.QDIM
	override val unitSymbol = "m^3/mol"
}
/** @see License */
enum class NeoSIMetreCubedPerSecondClass: NeoSIUnit<NeoISQVolumetricFlowRate> {
	MetreCubedPerSecond, MetresCubedPerSecond;
	override val qDimension = NeoISQVolumetricFlowRate.QDIM
	override val unitSymbol = "m^3/s"
}
/** @see License */
enum class NeoSIMetrePerSecondClass: NeoSIUnit<NeoISQSpeed> {
	MetrePerSecond, MetresPerSecond;
	override val qDimension = NeoISQSpeed.QDIM
	override val unitSymbol = "m/s"
}
/** @see License */
enum class NeoSIMetrePerSecondCubedClass: NeoSIUnit<NeoISQJerk> {
	MetrePerSecondCubed, MetresPerSecondCubed;
	override val qDimension = NeoISQJerk.QDIM
	override val unitSymbol = "m/s^3"
}
/** @see License */
enum class NeoSIMetrePerSecondSquaredClass: NeoSIUnit<NeoISQAcceleration> {
	MetrePerSecondSquared, MetresPerSecondSquared;
	override val qDimension = NeoISQAcceleration.QDIM
	override val unitSymbol = "m/s^2"
}
/** @see License */
enum class NeoSIMetreSquaredClass: NeoSIUnit<NeoISQArea> {
	MetreSquared, MetresSquared;
	override val qDimension = NeoISQArea.QDIM
	override val unitSymbol = "m^2"
}
/** @see License */
enum class NeoSIMetreSquaredPerSecondClass: NeoSIUnit<NeoISQKinematicViscosity> {
	MetreSquaredPerSecond, MetresSquaredPerSecond;
	override val qDimension = NeoISQKinematicViscosity.QDIM
	override val unitSymbol = "m^2/s"
}
/** @see License */
enum class NeoSIMolePerMetreCubedClass: NeoSIUnit<NeoISQMolarConcentration> {
	MolePerMetreCubed, MolesPerMetreCubed;
	override val qDimension = NeoISQMolarConcentration.QDIM
	override val unitSymbol = "mol/m^3"
}
/** @see License */
enum class NeoSINewtonMetrePerSecondClass: NeoSIUnit<NeoISQRotatum> {
	NewtonMetrePerSecond, NewtonMetresPerSecond;
	override val qDimension = NeoISQRotatum.QDIM
	override val unitSymbol = "N·m/s"
}
/** @see License */
enum class NeoSINewtonPerCoulombClass: NeoSIUnit<NeoISQElectricFieldStrength> {
	NewtonPerCoulomb, NewtonsPerCoulomb;
	override val qDimension = NeoISQElectricFieldStrength.QDIM
	override val unitSymbol = "N/C"
}
/** @see License */
enum class NeoSINewtonPerMetreClass: NeoSIUnit<NeoISQSurfaceTension> {
	NewtonPerMetre, NewtonsPerMetre;
	override val qDimension = NeoISQSurfaceTension.QDIM
	override val unitSymbol = "N/m"
}
/** @see License */
enum class NeoSINewtonPerSecondClass: NeoSIUnit<NeoISQYank> {
	NewtonPerSecond, NewtonsPerSecond;
	override val qDimension = NeoISQYank.QDIM
	override val unitSymbol = "N/s"
}
/** @see License */
enum class NeoSINewtonSecondClass: NeoSIUnit<NeoISQMomentum> {
	NewtonSecond, NewtonSeconds;
	override val qDimension = NeoISQMomentum.QDIM
	override val unitSymbol = "N·s"
}
/** @see License */
enum class NeoSIPascalSecondClass: NeoSIUnit<NeoISQDynamicViscosity> {
	PascalSecond, PascalSeconds;
	override val qDimension = NeoISQDynamicViscosity.QDIM
	override val unitSymbol = "Pa·s"
}
/** @see License */
enum class NeoSIRadianPerSecondClass: NeoSIUnit<NeoISQAngularSpeed> {
	RadianPerSecond, RadiansPerSecond;
	override val qDimension = NeoISQAngularSpeed.QDIM
	override val unitSymbol = "rad/s"
}
/** @see License */
enum class NeoSIRadianPerSecondCubedClass: NeoSIUnit<NeoISQAngularJerk> {
	RadianPerSecondCubed, RadiansPerSecondCubed;
	override val qDimension = NeoISQAngularJerk.QDIM
	override val unitSymbol = "rad/s^3"
}
/** @see License */
enum class NeoSIRadianPerSecondSquaredClass: NeoSIUnit<NeoISQAngularAcceleration> {
	RadianPerSecondSquared, RadiansPerSecondSquared;
	override val qDimension = NeoISQAngularAcceleration.QDIM
	override val unitSymbol = "rad/s^2"
}
/** @see License */
enum class NeoSIReciprocalMetreClass: NeoSIUnit<NeoISQWavenumber> {
	ReciprocalMetre, ReciprocalMetres;
	override val qDimension = NeoISQWavenumber.QDIM
	override val unitSymbol = "m^-1"
}
/** @see License */
enum class NeoSIWattPerMetreKelvinClass: NeoSIUnit<NeoISQThermalConductivity> {
	WattPerMetreKelvin, WattsPerMetreKelvin;
	override val qDimension = NeoISQThermalConductivity.QDIM
	override val unitSymbol = "W/(m·K)"
}
