package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class SIUnitlessClass: AcceptedSIUnit<ISQDimensionOne> {
	Unitless;
	override val qDimension = ISQDimensionOne.QDIM
	override val unitSymbol = ""
}
/** @see License */
enum class SIMetreClass: AcceptedSIUnit<ISQLength> {
	Metre, Metres;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "m"
}
/** @see License */
enum class SIKilogramClass: AcceptedSIUnit<ISQMass> {
	Kilogram, Kilograms;
	override val qDimension = ISQMass.QDIM
	override val unitSymbol = "kg"
}
/** @see License */
enum class SISecondClass: AcceptedSIUnit<ISQTime> {
	Second, Seconds;
	override val qDimension = ISQTime.QDIM
	override val unitSymbol = "s"
}
/** @see License */
enum class SIAmpereClass: AcceptedSIUnit<ISQElectricCurrent> {
	Ampere, Amperes;
	override val qDimension = ISQElectricCurrent.QDIM
	override val unitSymbol = "A"
}
/** @see License */
enum class SIKelvinClass: AcceptedSIUnit<ISQAbsoluteTemperature> {
	Kelvin, Kelvins;
	override val qDimension = ISQAbsoluteTemperature.QDIM
	override val unitSymbol = "K"
}
/** @see License */
enum class SIMoleClass: AcceptedSIUnit<ISQAmountOfSubstance> {
	Mole, Moles;
	override val qDimension = ISQAmountOfSubstance.QDIM
	override val unitSymbol = "mol"
}
/** @see License */
enum class CandelaClass: AcceptedSIUnit<ISQLuminousIntensity> {
	Candela, Candelas;
	override val qDimension = ISQLuminousIntensity.QDIM
	override val unitSymbol = "cd"
}
