package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*
import dev.yoshirulz.opus.qdims.AbelianAbsoluteTemperature.ArbitraryTemperatureDim
import dev.yoshirulz.opus.qdims.AbelianAmountOfSubstance.ArbitraryAmountDim
import dev.yoshirulz.opus.qdims.AbelianAngle.ArbitraryAngleDim
import dev.yoshirulz.opus.qdims.AbelianElectricCharge.ArbitraryChargeDim
import dev.yoshirulz.opus.qdims.AbelianElectricCurrent.ArbitraryCurrentDim
import dev.yoshirulz.opus.qdims.AbelianFinancialValue.ArbitraryFinancialValueDim
import dev.yoshirulz.opus.qdims.AbelianLength.ArbitraryLengthDim
import dev.yoshirulz.opus.qdims.AbelianLuminousIntensity.ArbitraryLuminousIntensityDim
import dev.yoshirulz.opus.qdims.AbelianMass.ArbitraryMassDim
import dev.yoshirulz.opus.qdims.AbelianSymbolCount.ArbitrarySymbolCountDim
import dev.yoshirulz.opus.qdims.AbelianTime.ArbitraryTimeDim
import dev.yoshirulz.opus.units.UnitSystem.*
import java.util.*

/** @see License */
class NeoSIUnitAmalgam(override val unitSymbol: String, exponents: IntArray): NeoSIUnit<NeoISQQuantityDimension>,
		UnitAmalgam<NeoISQQuantityDimension, NeoSI> {
	constructor(symbol: String, factors: Array<out NeoSIUnit<*>>): this(symbol, sumDims(factors))
	constructor(factors: Array<out NeoSIUnit<*>>): this(concatSymbols(factors), factors)

	companion object {
		private fun concatSymbols(factors: Array<out NeoSIUnit<*>>): String {
			val sj = StringJoiner("·")
			for (factor in factors) sj.add(factor.unitSymbol)
			return sj.toString()
		}

		private fun sumDims(f: Array<out NeoSIUnit<*>>): IntArray {
			val e = IntArray(9)
			for (factor in f) {
				val (length, mass, time, current, angle, temperature, amount, financialValue) = factor.qDimension.group
				e[0] += length.exponent
				e[1] += mass.exponent
				e[2] += time.exponent
				e[3] += current.exponent
				e[4] += angle.exponent
				e[5] += temperature.exponent
				e[6] += amount.exponent
				e[7] += financialValue.exponent
			}
			return e
		}
	}

	override val qDimension = NeoISQUnnamedDimension(ArbitraryLengthDim(exponents[1]), ArbitraryMassDim(exponents[2]),
		ArbitraryTimeDim(exponents[0]), ArbitraryCurrentDim(exponents[3]), ArbitraryAngleDim(exponents[4]),
		ArbitraryTemperatureDim(exponents[5]), ArbitraryAmountDim(exponents[6]),
		ArbitraryFinancialValueDim(exponents[8])).tryMapToNamed()
	override fun toString() = unitSymbol
}

/** @see License */
class OPUSUnitAmalgam(override val unitSymbol: String, exponents: IntArray): OPUSUnit<OQuSQuantityDimension>,
		UnitAmalgam<OQuSQuantityDimension, OPUS> {
	constructor(symbol: String, factors: Array<out OPUSUnit<*>>): this(symbol, sumDims(factors))
	constructor(factors: Array<out OPUSUnit<*>>): this(concatSymbols(factors), factors)

	companion object {
		private fun concatSymbols(factors: Array<out OPUSUnit<*>>): String {
			val sj = StringJoiner("·")
			for (factor in factors) sj.add(factor.unitSymbol)
			return sj.toString()
		}

		private fun sumDims(f: Array<out OPUSUnit<*>>): IntArray {
			val e = IntArray(9)
			for (u in f) {
				val (time, length, mass, charge, angle, temperature, amount, symbolCount, financialValue)
					= u.qDimension.group
				e[0] += time.exponent
				e[1] += length.exponent
				e[2] += mass.exponent
				e[3] += charge.exponent
				e[4] += angle.exponent
				e[5] += temperature.exponent
				e[6] += amount.exponent
				e[7] += symbolCount.exponent
				e[8] += financialValue.exponent
			}
			return e
		}
	}

	override val qDimension = OQuSUnnamedDimension(ArbitraryTimeDim(exponents[0]), ArbitraryLengthDim(exponents[1]),
		ArbitraryMassDim(exponents[2]), ArbitraryChargeDim(exponents[3]), ArbitraryAngleDim(exponents[4]),
		ArbitraryTemperatureDim(exponents[5]), ArbitraryAmountDim(exponents[6]), ArbitrarySymbolCountDim(exponents[7]),
		ArbitraryFinancialValueDim(exponents[8])).tryMapToNamed()
	override fun toString() = unitSymbol
}

/** @see License */
class SIUnitAmalgam private constructor(override val unitSymbol: String, e: IntArray,
		private val isOnlyAcceptedUnits: Boolean): UnitAmalgam<ISQQuantityDimension, SI>, SIUnit<ISQQuantityDimension> {
	constructor(symbol: String, exponents: IntArray): this(symbol, exponents, false)
	constructor(symbol: String, factors: Array<out SIUnit<*>>, b: Boolean): this(symbol, sumDims(factors), b)
	constructor(factors: Array<out SIUnit<*>>): this(concatSymbols(factors), factors, isAcceptedCalc(factors))

	companion object {
		private fun concatSymbols(factors: Array<out SIUnit<*>>): String {
			val sj = StringJoiner("·")
			for (factor in factors) sj.add(factor.unitSymbol)
			return sj.toString()
		}

		private fun sumDims(factors: Array<out SIUnit<*>>): IntArray {
			val exponents = IntArray(7)
			for (factor in factors) {
				val (length, mass, time, current, temperature, amount, luminousIntensity) = factor.qDimension.group
				exponents[0] += length.exponent
				exponents[1] += mass.exponent
				exponents[2] += time.exponent
				exponents[3] += current.exponent
				exponents[4] += temperature.exponent
				exponents[5] += amount.exponent
				exponents[6] += luminousIntensity.exponent
			}
			return exponents
		}

		private fun isAcceptedCalc(factors: Array<out SIUnit<*>>): Boolean {
			for (factor in factors) if (!factor.isAcceptedUnit) return false
			return true
		}
	}

	override val isAcceptedUnit get() = isOnlyAcceptedUnits
	override val qDimension = ISQUnnamedDimension(ArbitraryLengthDim(e[0]), ArbitraryMassDim(e[1]),
		ArbitraryTimeDim(e[2]), ArbitraryCurrentDim(e[3]), ArbitraryTemperatureDim(e[4]), ArbitraryAmountDim(e[5]),
		ArbitraryLuminousIntensityDim(e[6])).tryMapToNamed()
	override fun toString() = unitSymbol
}
