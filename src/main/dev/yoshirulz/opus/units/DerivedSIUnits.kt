package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class SIAmperePerMetreClass: AcceptedSIUnit<ISQMagneticFieldHStrength> {
	AmperePerMetre, AmperesPerMetre;
	override val qDimension = ISQMagneticFieldHStrength.QDIM
	override val unitSymbol = "A/m"
}
/** @see License */
enum class SICoulombPerKilogramClass: AcceptedSIUnit<ISQChargeMassRatio> {
	CoulombPerKilogram, CoulombsPerKilogram;
	override val qDimension = ISQChargeMassRatio.QDIM
	override val unitSymbol = "C/kg"
}
/** @see License */
enum class SICoulombPerMetreCubedClass: AcceptedSIUnit<ISQChargeDensity> {
	CoulombPerMetreCubed, CoulombsPerMetreCubed;
	override val qDimension = ISQChargeDensity.QDIM
	override val unitSymbol = "C/m^3"
}
/** @see License */
enum class SICoulombPerMetreSquaredClass: AcceptedSIUnit<ISQSurfaceChargeDensity> {
	CoulombPerMetreSquared, CoulombsPerMetreSquared;
	override val qDimension = ISQSurfaceChargeDensity.QDIM
	override val unitSymbol = "C/m^2"
}
/** @see License */
enum class SIFaradPerMetreClass: AcceptedSIUnit<ISQPermittivity> {
	FaradPerMetre, FaradsPerMetre;
	override val qDimension = ISQPermittivity.QDIM
	override val unitSymbol = "F/m"
}
/** @see License */
enum class SIHenryPerMetreClass: AcceptedSIUnit<ISQPermeability> {
	HenryPerMetre, HenriesPerMetre;
	override val qDimension = ISQPermeability.QDIM
	override val unitSymbol = "H/m"
}
/** @see License */
enum class SIJoulePerKelvinEntropyClass: AcceptedSIUnit<ISQEntropy> {
	JoulePerKelvin, JoulesPerKelvin;
	override val qDimension = ISQEntropy.QDIM
	override val unitSymbol = "J/K"
}
/** @see License */
enum class SIJoulePerKelvinHeatCapacityClass: AcceptedSIUnit<ISQHeatCapacity> {
	JoulePerKelvin, JoulesPerKelvin;
	override val qDimension = ISQHeatCapacity.QDIM
	override val unitSymbol = "J/K"
}
/** @see License */
enum class SIJoulePerKilogramClass: AcceptedSIUnit<ISQSpecificEnergy> {
	JoulePerKilogram, JoulesPerKilogram;
	override val qDimension = ISQSpecificEnergy.QDIM
	override val unitSymbol = "J/kg"
}
/** @see License */
enum class SIJoulePerKilogramKelvinClass: AcceptedSIUnit<ISQSpecificHeatCapacity> {
	JoulePerKilogramKelvin, JoulesPerKilogramKelvin;
	override val qDimension = ISQSpecificHeatCapacity.QDIM
	override val unitSymbol = "J/(kg·K)"
}
/** @see License */
enum class SIJoulePerMetreCubedClass: AcceptedSIUnit<ISQEnergyDensity> {
	JoulePerMetreCubed, JoulesPerMetreCubed;
	override val qDimension = ISQEnergyDensity.QDIM
	override val unitSymbol = "J/K"
}
/** @see License */
enum class SIJoulePerMoleClass: AcceptedSIUnit<ISQMolarEnergy> {
	JoulePerMole, JoulesPerMole;
	override val qDimension = ISQMolarEnergy.QDIM
	override val unitSymbol = "J/mol"
}
/** @see License */
enum class SIJoulePerMoleKelvinClass: AcceptedSIUnit<ISQMolarHeatCapacity> {
	JoulePerMoleKelvin, JoulesPerMoleKelvin;
	override val qDimension = ISQMolarHeatCapacity.QDIM
	override val unitSymbol = "J/(mol·K)"
}
/** @see License */
enum class SIJoulePerRadianClass: AcceptedSIUnit<ISQTorque> {
	JoulePerRadian, JoulesPerRadian;
	override val qDimension = ISQTorque.QDIM
	override val unitSymbol = "J/rad"
}
/** @see License */
enum class SIJoulePerTeslaClass: AcceptedSIUnit<ISQMagneticMoment> {
	JoulePerTesla, JoulesPerTesla;
	override val qDimension = ISQMagneticMoment.QDIM
	override val unitSymbol = "J/T"
}
/** @see License */
enum class SIJouleSecondActionClass: AcceptedSIUnit<ISQAction> {
	JouleSecond, JouleSeconds;
	override val qDimension = ISQAction.QDIM
	override val unitSymbol = "J·s"
}
/** @see License */
enum class SIJouleSecondAngularMomentumClass: AcceptedSIUnit<ISQAngularMomentum> {
	JouleSecond, JouleSeconds;
	override val qDimension = ISQAngularMomentum.QDIM
	override val unitSymbol = "J·s"
}
/** @see License */
enum class SIKilogramPerMetreCubedClass: AcceptedSIUnit<ISQDensity> {
	KilogramPerMetreCubed, KilogramsPerMetreCubed;
	override val qDimension = ISQDensity.QDIM
	override val unitSymbol = "kg/m^3"
}
/** @see License */
enum class SIKilogramPerMetreSquaredClass: AcceptedSIUnit<ISQAreaDensity> {
	KilogramPerMetreSquared, KilogramsPerMetreSquared;
	override val qDimension = ISQAreaDensity.QDIM
	override val unitSymbol = "kg/m^2"
}
/** @see License */
enum class SIKilogramPerMoleClass: AcceptedSIUnit<ISQMolarMass> {
	KilogramPerMole, KilogramsPerMole;
	override val qDimension = ISQMolarMass.QDIM
	override val unitSymbol = "kg/mol"
}
/** @see License */
enum class SIMetreCubedClass: AcceptedSIUnit<ISQVolume> {
	MetreCubed, MetresCubed;
	override val qDimension = ISQVolume.QDIM
	override val unitSymbol = "m^3"
}
/** @see License */
enum class SIMetreCubedPerKilogramClass: AcceptedSIUnit<ISQSpecificVolume> {
	MetreCubedPerKilogram, MetresCubedPerKilogram;
	override val qDimension = ISQSpecificVolume.QDIM
	override val unitSymbol = "m^3/kg"
}
/** @see License */
enum class SIMetreCubedPerMoleClass: AcceptedSIUnit<ISQMolarVolume> {
	MetreCubedPerMole, MetresCubedPerMole;
	override val qDimension = ISQMolarVolume.QDIM
	override val unitSymbol = "m^3/mol"
}
/** @see License */
enum class SIMetreCubedPerSecondClass: AcceptedSIUnit<ISQVolumetricFlowRate> {
	MetreCubedPerSecond, MetresCubedPerSecond;
	override val qDimension = ISQVolumetricFlowRate.QDIM
	override val unitSymbol = "m^3/s"
}
/** @see License */
enum class SIMetrePerSecondClass: AcceptedSIUnit<ISQSpeed> {
	MetrePerSecond, MetresPerSecond;
	override val qDimension = ISQSpeed.QDIM
	override val unitSymbol = "m/s"
}
/** @see License */
enum class SIMetrePerSecondCubedClass: AcceptedSIUnit<ISQJerk> {
	MetrePerSecondCubed, MetresPerSecondCubed;
	override val qDimension = ISQJerk.QDIM
	override val unitSymbol = "m/s^3"
}
/** @see License */
enum class SIMetrePerSecondSquaredClass: AcceptedSIUnit<ISQAcceleration> {
	MetrePerSecondSquared, MetresPerSecondSquared;
	override val qDimension = ISQAcceleration.QDIM
	override val unitSymbol = "m/s^2"
}
/** @see License */
enum class SIMetreSquaredClass: AcceptedSIUnit<ISQArea> {
	MetreSquared, MetresSquared;
	override val qDimension = ISQArea.QDIM
	override val unitSymbol = "m^2"
}
/** @see License */
enum class SIMetreSquaredPerSecondClass: AcceptedSIUnit<ISQKinematicViscosity> {
	MetreSquaredPerSecond, MetresSquaredPerSecond;
	override val qDimension = ISQKinematicViscosity.QDIM
	override val unitSymbol = "m^2/s"
}
/** @see License */
enum class SIMolePerMetreCubedClass: AcceptedSIUnit<ISQMolarConcentration> {
	MolePerMetreCubed, MolesPerMetreCubed;
	override val qDimension = ISQMolarConcentration.QDIM
	override val unitSymbol = "mol/m^3"
}
/** @see License */
enum class SINewtonMetrePerSecondClass: AcceptedSIUnit<ISQRotatum> {
	NewtonMetrePerSecond, NewtonMetresPerSecond;
	override val qDimension = ISQRotatum.QDIM
	override val unitSymbol = "N·m/s"
}
/** @see License */
enum class SINewtonPerCoulombClass: AcceptedSIUnit<ISQElectricFieldStrength> {
	NewtonPerCoulomb, NewtonsPerCoulomb;
	override val qDimension = ISQElectricFieldStrength.QDIM
	override val unitSymbol = "N/C"
}
/** @see License */
enum class SINewtonPerMetreClass: AcceptedSIUnit<ISQSurfaceTension> {
	NewtonPerMetre, NewtonsPerMetre;
	override val qDimension = ISQSurfaceTension.QDIM
	override val unitSymbol = "N/m"
}
/** @see License */
enum class SINewtonPerSecondClass: AcceptedSIUnit<ISQYank> {
	NewtonPerSecond, NewtonsPerSecond;
	override val qDimension = ISQYank.QDIM
	override val unitSymbol = "N/s"
}
/** @see License */
enum class SINewtonSecondClass: AcceptedSIUnit<ISQMomentum> {
	NewtonSecond, NewtonSeconds;
	override val qDimension = ISQMomentum.QDIM
	override val unitSymbol = "N·s"
}
/** @see License */
enum class SIPascalSecondClass: AcceptedSIUnit<ISQDynamicViscosity> {
	PascalSecond, PascalSeconds;
	override val qDimension = ISQDynamicViscosity.QDIM
	override val unitSymbol = "Pa·s"
}
/** @see License */
enum class SIRadianPerSecondClass: AcceptedSIUnit<ISQAngularSpeed> {
	RadianPerSecond, RadiansPerSecond;
	override val qDimension = ISQAngularSpeed.QDIM
	override val unitSymbol = "rad/s"
}
/** @see License */
enum class SIRadianPerSecondCubedClass: AcceptedSIUnit<ISQAngularJerk> {
	RadianPerSecondCubed, RadiansPerSecondCubed;
	override val qDimension = ISQAngularJerk.QDIM
	override val unitSymbol = "rad/s^3"
}
/** @see License */
enum class SIRadianPerSecondSquaredClass: AcceptedSIUnit<ISQAngularAcceleration> {
	RadianPerSecondSquared, RadiansPerSecondSquared;
	override val qDimension = ISQAngularAcceleration.QDIM
	override val unitSymbol = "rad/s^2"
}
/** @see License */
enum class SIReciprocalMetreClass: AcceptedSIUnit<ISQWavenumber> {
	ReciprocalMetre, ReciprocalMetres;
	override val qDimension = ISQWavenumber.QDIM
	override val unitSymbol = "m^-1"
}
/** @see License */
enum class SIWattPerMetreKelvinClass: AcceptedSIUnit<ISQThermalConductivity> {
	WattPerMetreKelvin, WattsPerMetreKelvin;
	override val qDimension = ISQThermalConductivity.QDIM
	override val unitSymbol = "W/(m·K)"
}
