package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.LibraryGlobals.ConstantSymbolName.SpeedOfLightInFreeSpace
import dev.yoshirulz.opus.LibraryGlobals.i18nGetConstantSymbol
import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.NeoISQLength
import dev.yoshirulz.opus.qdims.NeoISQSpeed

/** @see License */
enum class NeoSILightspeedClass: NeoSIUnit<NeoISQSpeed> {
	Lightspeed;
	override val qDimension = NeoISQSpeed.QDIM
	override val unitSymbol get() = i18nGetConstantSymbol(SpeedOfLightInFreeSpace)
}
/** @see License */
enum class NeoSILightYearClass: NeoSIUnit<NeoISQLength> {
	Lightyear, Lightyears;
	override val qDimension = NeoISQLength.QDIM
	override val unitSymbol = "ly"
}
