package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.*

/** @see License */
enum class SIHertzClass: AcceptedSIUnit<ISQFrequency> {
	Hertz;
	override val qDimension = ISQFrequency.QDIM
	override val unitSymbol = "Hz"
}
/** @see License */
enum class SIRadianClass: AcceptedSIUnit<ISQAngle> {
	Radian, Radians;
	override val qDimension = ISQAngle.QDIM
	override val unitSymbol = "rad"
}
/** @see License */
enum class SISteradianClass: AcceptedSIUnit<ISQSolidAngle> {
	Steradian, Steradians;
	override val qDimension = ISQSolidAngle.QDIM
	override val unitSymbol = "sr"
}
/** @see License */
enum class SINewtonClass: AcceptedSIUnit<ISQForce> {
	Newton, Newtons;
	override val qDimension = ISQForce.QDIM
	override val unitSymbol = "N"
}
/** @see License */
enum class SIPascalClass: AcceptedSIUnit<ISQPressure> {
	Pascal, Pascals;
	override val qDimension = ISQPressure.QDIM
	override val unitSymbol = "Pa"
}
/** @see License */
enum class SIJouleClass: AcceptedSIUnit<ISQEnergy> {
	Joule, Joules;
	override val qDimension = ISQEnergy.QDIM
	override val unitSymbol = "J"
}
/** @see License */
enum class SIWattClass: AcceptedSIUnit<ISQPower> {
	Watt, Watts;
	override val qDimension = ISQPower.QDIM
	override val unitSymbol = "W"
}
/** @see License */
enum class SICoulombClass: AcceptedSIUnit<ISQElectricCharge> {
	Coulomb, Coulombs;
	override val qDimension = ISQElectricCharge.QDIM
	override val unitSymbol = "C"
}
/** @see License */
enum class SIVoltClass: AcceptedSIUnit<ISQElectrostaticPotential> {
	Volt, Volts;
	override val qDimension = ISQElectrostaticPotential.QDIM
	override val unitSymbol = "V"
}
/** @see License */
enum class SIFaradClass: AcceptedSIUnit<ISQElectricalCapacitance> {
	Farad, Farads;
	override val qDimension = ISQElectricalCapacitance.QDIM
	override val unitSymbol = "F"
}
/** @see License */
enum class SIOhmClass: AcceptedSIUnit<ISQElectricalResistance> {
	Ohm, Ohms;
	override val qDimension = ISQElectricalResistance.QDIM
	override val unitSymbol = "Ω"
}
/** @see License */
enum class SISiemensClass: AcceptedSIUnit<ISQElectricalConductance> {
	Siemens;
	override val qDimension = ISQElectricalConductance.QDIM
	override val unitSymbol = "S"
}
/** @see License */
enum class SIWeberClass: AcceptedSIUnit<ISQMagneticFlux> {
	Weber, Webers;
	override val qDimension = ISQMagneticFlux.QDIM
	override val unitSymbol = "Wb"
}
/** @see License */
enum class SITeslaClass: AcceptedSIUnit<ISQMagneticFluxDensity> {
	Tesla, Teslas;
	override val qDimension = ISQMagneticFluxDensity.QDIM
	override val unitSymbol = "T"
}
/** @see License */
enum class SIHenryClass: AcceptedSIUnit<ISQElectricalInductance> {
	Henry, Henries;
	override val qDimension = ISQElectricalInductance.QDIM
	override val unitSymbol = "H"
}
/** @see License */
enum class DegreeCelsiusClass: AcceptedSIUnit<ISQAbsoluteTemperature> {
	DegreeCelsius, DegreesCelsius;
	override val qDimension = ISQAbsoluteTemperature.QDIM
	override val unitSymbol = "°C"
}
/** @see License */
enum class LumenClass: AcceptedSIUnit<ISQLuminousFlux> {
	Lumen, Lumens;
	override val qDimension = ISQLuminousFlux.QDIM
	override val unitSymbol = "lm"
}
/** @see License */
enum class LuxClass: AcceptedSIUnit<ISQIlluminance> {
	Lux;
	override val qDimension = ISQIlluminance.QDIM
	override val unitSymbol = "lx"
}
/** @see License */
enum class BecquerelClass: AcceptedSIUnit<ISQRadioactivity> {
	Becquerel, Becquerels;
	override val qDimension = ISQRadioactivity.QDIM
	override val unitSymbol = "Bq"
}
/** @see License */
enum class GrayClass: AcceptedSIUnit<ISQAbsorbedDose> {
	Gray, Grays;
	override val qDimension = ISQAbsorbedDose.QDIM
	override val unitSymbol = "Gy"
}
/** @see License */
enum class SievertClass: AcceptedSIUnit<ISQEquivalentDose> {
	Sievert, Sieverts;
	override val qDimension = ISQEquivalentDose.QDIM
	override val unitSymbol = "Sv"
}
/** @see License */
enum class SIKatalClass: AcceptedSIUnit<ISQCatalyticActivity> {
	Katal, Katals;
	override val qDimension = ISQCatalyticActivity.QDIM
	override val unitSymbol = "kat"
}
