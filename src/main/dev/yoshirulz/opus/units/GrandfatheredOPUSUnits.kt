package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.Angle
import dev.yoshirulz.opus.qdims.FinancialValue
import dev.yoshirulz.opus.qdims.SolidAngle

/** @see License */
enum class OPUSGenericDollarClass: OPUSUnit<FinancialValue> {
	GenericDollar, GenericDollars;
	override val qDimension = FinancialValue.QDIM
	override val unitSymbol = "$"
}
/** @see License */
enum class OPUSRadianClass: OPUSUnit<Angle> {
	Radian, Radians;
	override val qDimension = Angle.QDIM
	override val unitSymbol = "rad"
}
/** @see License */
enum class OPUSSteradianClass: OPUSUnit<SolidAngle> {
	Steradian, Steradians;
	override val qDimension = SolidAngle.QDIM
	override val unitSymbol = "sr"
}
