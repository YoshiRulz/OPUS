package dev.yoshirulz.opus.units

import dev.yoshirulz.opus.License
import dev.yoshirulz.opus.qdims.ISQAbsoluteTemperature
import dev.yoshirulz.opus.qdims.ISQLength

/**
 * 1 pt = 1/12 pc
 * @see License
 */
enum class PointClass: USCustomaryUnit<ISQLength> {
	Point, Points;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "pt"
}
/**
 * 1 pc = 1/6 in
 * @see License
 */
enum class PicaClass: USCustomaryUnit<ISQLength> {
	Pica, Picas;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "pc"
}
/**
 * 1 in = 1/12 ft
 * @see License
 */
enum class InchClass: USCustomaryUnit<ISQLength> {
	Inch, Inches;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "in"
}
/**
 * 1 ft = 1/3 yd
 * @see License
 */
enum class InternationalFootClass: USCustomaryUnit<ISQLength> {
	Foot, Feet;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "ft"
}
/**
 * Using the modern definition: 1 yd = 0.9144 m
 * @see License
 */
enum class YardClass: USCustomaryUnit<ISQLength> {
	Yard, Yards;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "yd"
}
/**
 * 1 mi = 1760 yd = 5280 ft
 * @see License
 */
enum class MileClass: USCustomaryUnit<ISQLength> {
	Mile, Miles;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "mi"
}
/**
 * 1 li = 33/50 ft (survey)
 * @see License
 */
enum class LinkClass: USCustomaryUnit<ISQLength> {
	Link, Links;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "li"
}
/**
 * Using the 1893 definition of the foot: 1 ft = 1200/3937 m
 * @see License
 */
enum class SurveyFootClass: USCustomaryUnit<ISQLength> {
	Foot, Feet;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "ft"
}
/**
 * 1 rd = 25 li = 16.5 ft (survey)
 * @see License
 */
enum class RodClass: USCustomaryUnit<ISQLength> {
	Rod, Rods;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "rd"
}
/**
 * 1 ch = 4 rd = 66 ft (survey)
 * @see License
 */
enum class ChainClass: USCustomaryUnit<ISQLength> {
	Chain, Chains;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "ch"
}
/**
 * 1 fur = 10 ch = 660 ft (survey)
 * @see License
 */
enum class FurlongClass: USCustomaryUnit<ISQLength> {
	Furlong, Furlongs;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "fur"
}
/**
 * 1 mi = 8 fur = 5280 ft (survey)
 * @see License
 */
enum class SurveyMileClass: USCustomaryUnit<ISQLength> {
	Mile, Miles;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "mi"
}
/**
 * 1 lea = 3 mi = 15840 ft (survey)
 * @see License
 */
enum class LeagueClass: USCustomaryUnit<ISQLength> {
	League, Leagues;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "lea"
}
/**
 * 1 ftm = 2 yd = 6 ft
 * @see License
 */
enum class FathomClass: USCustomaryUnit<ISQLength> {
	Fathom, Fathoms;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "ftm"
}
/**
 * 1 cb = 120 ftm = 720 ft
 * @see License
 */
enum class CableClass: USCustomaryUnit<ISQLength> {
	Cable, Cables;
	override val qDimension = ISQLength.QDIM
	override val unitSymbol = "cb"
}
/**
 * -459.67 °F is exactly 0 K or absolute zero, 569.5875 °F is exactly 569.5875 K, a fixed point
 * @see License
 */
enum class DegreeFahrenheitClass: USCustomaryUnit<ISQAbsoluteTemperature> {
	DegreeFahrenheit, DegreesFahrenheit;
	override val qDimension = ISQAbsoluteTemperature.QDIM
	override val unitSymbol = "°F"
}
