package dev.yoshirulz.opus

import dev.yoshirulz.GPLTwo
import dev.yoshirulz.opus.LibraryGlobals.ConstantSymbolName.*
import dev.yoshirulz.opus.LibraryGlobals.DimensionSymbolName.*
import java.math.MathContext
import java.math.RoundingMode

/**
 * OPUS Dimensional Analysis Library — Licensed under GPL2+ (GPL-2.0)
 *
 * Copyright © 2018 YoshiRulz <[OSSYoshiRulz@gmail.com](mailto:OSSYoshiRulz@gmail.com)>
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received [a copy of the GNU General Public License][GPLTwo] along with this program. If not, see
 * [gnu.org/licenses](https://www.gnu.org/licenses).
 * @see GPLTwo
 */
object License

/**
 * It's dangerous to go alone, take this: ×
 * @see License
 */
object LibraryGlobals {
	enum class ConstantSymbolName {
		AntielectronChargeNegative, DownTypeQuarkCharge, ElectronChargeNegative, ElectronChargePositive,
		ElectronChargeSquaredNegative, ElectronChargeSquaredPositive, PositronCharge, UpTypeQuarkCharge,

		ImpedanceOfFreeSpace, PermeabilityOfFreeSpace, PermittivityOfFreeSpace, SpeedOfLightInFreeSpace,
		SpeedOfLightInFreeSpaceSquared,

		AvogadroConstant, BoltzmannConstant, BohrRadius, ElectronMass, FermionSpin, FineStructureConstant,
		GravitationalConstant, HartreeEnergy, IdealGasConstant, PlanckConstant, ReducedPlanckConstant, Revolution
	}
	enum class DimensionSymbolName {
		Amount, Angle, Charge, Current, FinancialValue, Length, LuminousIntensity, Mass, SymbolCount, Time, Temperature
	}

	private val i18nConstantSymbolMapDefault = mapOf(Revolution to "rev")
	private val i18nConstantSymbolMapData = mutableMapOf(
		"OPUS" to mutableMapOf(
			AntielectronChargeNegative to "TODO",
			DownTypeQuarkCharge to "Q_d",
			ElectronChargeNegative to "-e",
			ElectronChargePositive to "e",
			PositronCharge to "TODO",
			UpTypeQuarkCharge to "Q_u",

			ImpedanceOfFreeSpace to "η_0",
			PermeabilityOfFreeSpace to "μ_0",
			PermittivityOfFreeSpace to "ε_0",
			SpeedOfLightInFreeSpace to "c_0",

			AvogadroConstant to "N_A",
			BoltzmannConstant to "k_B",
			BohrRadius to "a_0",
			ElectronMass to "m_e",
			FermionSpin to "S_f",
			FineStructureConstant to "α",
			GravitationalConstant to "G",
			HartreeEnergy to "E_h",
			IdealGasConstant to "R",
			PlanckConstant to "h",
			ReducedPlanckConstant to "ħ",
			Revolution to "rev"
		)
	)

	private val i18nDimensionSymbolMapDefault = mapOf(Time to "T")
	private val i18nDimensionSymbolCanonicalMap = mapOf(
		Amount to "N", Angle to "R", Charge to "Q", Current to "I", FinancialValue to "V", Length to "L",
		LuminousIntensity to "J", Mass to "M", SymbolCount to "Q", Time to "T", Temperature to "Q"
	)
	val i18nDimensionSymbolMaps =
		mutableMapOf("OPUS" to i18nDimensionSymbolCanonicalMap)

	val i18nConstantSymbolMaps: MutableMap<String, Map<ConstantSymbolName, String>>
	init {
		i18nConstantSymbolMaps = mutableMapOf("OPUS" to i18nConstantSymbolMapDefault)
		for ((name, map) in i18nConstantSymbolMapData) {
			map[ElectronChargeSquaredNegative] = "${map[ElectronChargeNegative]}^2"
			map[ElectronChargeSquaredPositive] = "${map[ElectronChargePositive]}^2"
			map[SpeedOfLightInFreeSpaceSquared] = "${map[SpeedOfLightInFreeSpace]}^2"
			i18nConstantSymbolMaps[name] = map.toMap()
		}
	}

	fun i18nGetConstantSymbol(s: ConstantSymbolName) =
		(i18nConstantSymbolMaps[i18nConstantSymbolMapName] ?: i18nConstantSymbolMapDefault)[s]
			?: i18nConstantSymbolDefault
	fun i18nGetDimensionSymbol(s: DimensionSymbolName) =
		(i18nDimensionSymbolMaps[i18nDimensionSymbolMapName] ?: i18nDimensionSymbolMapDefault)[s]
			?: i18nDimensionSymbolDefault
	fun i18nGetCanonicalDimensionSymbol(s: DimensionSymbolName) =
		i18nDimensionSymbolCanonicalMap[s] ?: i18nDimensionSymbolDefault

	var i18nConstantSymbolDefault = "nil"
	var i18nConstantSymbolMapName = "OPUS"
	var i18nDimensionSymbolDefault = "nil"
	var i18nDimensionSymbolMapName = "OPUS"
	var isDevMode = false
	var mathContext: MathContext = MathContext.UNLIMITED
	var roundingMode = RoundingMode.HALF_UP
}
