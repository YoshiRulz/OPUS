package dev.yoshirulz.opus

import dev.yoshirulz.opus.qdims.ChargeMassRatio
import dev.yoshirulz.opus.qdims.ElectricFieldStrength
import dev.yoshirulz.opus.qdims.Length
import dev.yoshirulz.opus.qdims.MagneticFluxDensity
import dev.yoshirulz.opus.quantity.Quantity
import dev.yoshirulz.opus.units.ChargeMassRatioUnitNameClass.ChargeMassRatioUnitName
import dev.yoshirulz.opus.units.UnitSystem.OPUS

/** @see License */
object Electromagnetism {
	fun calcBeamChargeMassRatio(E: Quantity<ElectricFieldStrength, OPUS>, B: Quantity<MagneticFluxDensity, OPUS>,
				r: Quantity<Length, OPUS>):
			Quantity<ChargeMassRatio, OPUS> =
		ChargeMassRatioUnitName.qOf(E.exactValue / (B.exactValue * B.exactValue * r.exactValue))
}
