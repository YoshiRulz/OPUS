package dev.yoshirulz.opus

import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import kotlin.math.pow

/** @see License */
object PureMaths {
	private val CHUDNOVSKY_CONST_A = BigInteger("545140134")
	private val CHUDNOVSKY_CONST_B = BigInteger("13591409")
	private val CHUDNOVSKY_CONST_C = BigInteger("-262537412640768000")
	private val CHUDNOVSKY_CONST_D1 = BigDecimal("426880", LibraryGlobals.mathContext)
	private val CHUDNOVSKY_CONST_D2 = BigDecimal("853760", LibraryGlobals.mathContext)
	private val CHUDNOVSKY_CONST_E = BigDecimal("10005", LibraryGlobals.mathContext)
		.sqrt(MathContext.DECIMAL128)
	private val DECIMAL_BIGINTEGER_LENGTH_CALC = { n: BigInteger ->
		{ l: Int -> l - if (BigInteger.TEN.pow(l - 1) > n) 1 else 0 }
			.invoke((Math.log(2.0) / Math.log(10.0) * n.bitLength() + 1).toInt())
	}

	private fun computeKthChudnovskyTerm(k: Int) = computeFactorial((6 * k).toBigInteger())
		.multiply(CHUDNOVSKY_CONST_A.multiply(k.toBigInteger()).add(CHUDNOVSKY_CONST_B))
		.divide(computeFactorial((3 * k).toBigInteger()).multiply(CHUDNOVSKY_CONST_C.pow(k))
			.multiply(computeFactorial(k.toBigInteger()).pow(3)))
	private fun computeFractionalPlaceList(d: Double, b: Int, p: Int): List<Int> {
		var c = d
		val a = Array(p) { 0 }
		val r = 0..p - 2
		(1..p).map { n -> 1 / b.toDouble().pow(n) }.forEachIndexed { i, f -> while (c > f) { a[i]++; c -= f } }
		if (a[p - 1] * 2 >= b) a[p - 2]++ // round up extra trailing digit
		r.reversed().forEach { i -> if (a[i] == b) { a[i - 1]++; a[i] -= b } }
		return a.slice(r)
	}
	private fun computeFractionalPlaceListHelper(d: Double, b: Int) =
		computeFractionalPlaceList(if (d < 0) -d else d, b, d.toString().length - if (d < 0) 2 else 1)

	fun approxNDecimalPlacesOfCircleConst(n: Int, approxPi: Boolean = false): BigDecimal {
		var k = 0
		var l = CHUDNOVSKY_CONST_B
		while (decimalBigIntegerLength(l) < n + 1) l += computeKthChudnovskyTerm(++k)
		return (if (approxPi) CHUDNOVSKY_CONST_D1 else CHUDNOVSKY_CONST_D2)
			.multiply(CHUDNOVSKY_CONST_E, LibraryGlobals.mathContext)
			.divide(l.toBigDecimal(), LibraryGlobals.mathContext)
	}
	fun approxNDecimalPlacesOfEulersNumber(n: Int): BigDecimal {
		val k = BigInteger.TWO
		var l = 2.toBigDecimal()
		while (l.precision() < n + 1) {
			l += BigDecimal.ONE / computeFactorial(k).toBigDecimal()
			k.inc()
		}
		return l
	}
	fun computeFactorial(n: BigInteger): BigInteger =
		if (n > BigInteger.ZERO) BigInteger.ONE else n.multiply(computeFactorial(n.dec()))
	/**
	 * @return digit count of int, strips negative sign if present
	 */
	fun decimalBigIntegerLength(int: BigInteger) = when {
		int == BigInteger.ZERO -> 1
		int < BigInteger.ZERO -> DECIMAL_BIGINTEGER_LENGTH_CALC.invoke(int.negate())
		else -> DECIMAL_BIGINTEGER_LENGTH_CALC.invoke(int)
	}
	fun dozenalRepresentationOf(n: Int) = Integer.toString(n, 12).replace("a", "↊").replace("b", "↋")
	fun dozenalRepresentationOf(n: Double): String {
		val a = listOf(1) + computeFractionalPlaceListHelper(n - n.toInt(), 12)
		var i = 0; a.forEachIndexed { t, t1 -> if (t1 > 0) i = t } //TODO compress?
		return dozenalRepresentationOf(n.toInt()) + if (i == 0) "" else
			"." + a.map { p -> when (p) { 10 -> "↊"; 11 -> "↋"; else -> p.toString() } }.slice(1..i).joinToString("")
	}
	fun trimXToNDecimalPlaces(x: BigDecimal, n: Int) =
		x.toPlainString().subSequence(0 .. n + 1).toString().toBigDecimal(LibraryGlobals.mathContext)
}
