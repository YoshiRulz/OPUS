package dev.yoshirulz.opus

import dev.yoshirulz.opus.qdims.*
import dev.yoshirulz.opus.quantity.OPUSCalculableConstant
import dev.yoshirulz.opus.quantity.Quantity
import dev.yoshirulz.opus.units.MolarVolumeUnitNameClass.MolarVolumeUnitName
import dev.yoshirulz.opus.units.UnitSystem.OPUS

/** @see License */
object IdealGas {
	fun getMolarVolume_Vn(V: Quantity<Volume, OPUS>, n: Quantity<AmountOfSubstance, OPUS>):
			Quantity<MolarVolume, OPUS> =
		MolarVolumeUnitName.qOf(V.exactValue / n.exactValue)
	fun getMolarVolume_TP(T: Quantity<AbsoluteTemperature, OPUS>, P: Quantity<Pressure, OPUS>):
			Quantity<MolarVolume, OPUS> =
		MolarVolumeUnitName.qOf(OPUSCalculableConstant.IDEAL_GAS_CONSTANT.exactValue * T.exactValue / P.exactValue)
}
